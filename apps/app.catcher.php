<?php
/**
 * @package use
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/


/**
 * app_catcher
 * Приложение catcher
 * 
 * @package use   
 * @author Charlie <charlie@chrl.ru> 
 * @access public
 */
class catcher
{

    public function is_email($string) {
      return preg_match('/^[^0-9][a-zA-Z0-9_.]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/',$string);
    }
    
    public function is_url($uri) {
        
        $uri = str_replace('-','',$uri);
        // Check if it has unicode chars.
        $l = mb_strlen ($uri,'UTF-8');
        $s = $uri;
        if ($l !== strlen ($uri)) {
            // Replace wide chars by “X”.
            $s = str_repeat(' ', $l);
            for ($i = 0; $i < $l; ++$i) {
                $ch = mb_substr($uri, $i, 1,'UTF-8');
                $s [$i] = strlen($ch) > 1 ? 'X' : $ch;
            }
        }
        
        return filter_var ($s, FILTER_VALIDATE_URL);        
    }
    

    public function catchProjectaddform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if(!trim($fields['project_name']))
        {
            $return['msg']='Не заполнено поле "Название проекта"';
            $return['mark']='project_name';
        }
        
        if (!isset($return['msg']))
        {
            $id = BS::DAO('project')->insertItem(array(
                'name'=>$fields['project_name'],
                'owner'=>$_SESSION['user']['id'],
                'date'=>time(),
            ));
            
            BS::DAO('access')->insertItem(array(
                'user'=>$_SESSION['user']['id'],
                'project'=>$id,
                'level'=>10,
            ));
            
            BS::Log(array(
                   'action'=>'project_create',
                   'subject'=>$id,
                   'params'=>array('project_id'=>$id),
            ));                
            
            $return['result']='ok';
            $return['function']='gotoProject';
	   $return['arg']=$id;		
        }        
        
        return $return;                
    }   
    
    public function catchEdittextdocumentform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['editor']) && !trim($fields['editor'])) {
                $return['editor']='Введите текст документа!';
                $return['result'] = 'fail';			
        }		
		
        $oldversion = BS::DAO('document')->getById($fields['page_id']);
        
       

        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('document')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'author'=>$_SESSION['user']['id'],
				'text'=>$fields['editor'],
				'date'=>time(),
                                'type'=>'text',
                                'version'=>BS::DAO('document')->getLastVersion($fields['page_alias'])+1,
			));

            $return['result']='ok';
            $return['uri']='/admin/documents/';
        }
		
        
        return $return;                
    }

    public function catchEditmediadocumentform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название документа не может быть пустым';
                $return['result'] = 'fail';			
        }
        
        // parse files
        
        $files = array();
        foreach ($fields as $key=>$value) {
            if (substr($key,0,5)=='file_') {
                $fname = str_replace('file_','',$key);
                $fname = str_replace('_name','',$fname);
                
                $files[] = array(
                    'name'=>$fname,
                    'desc'=>trim($value) ? trim($value):'',
                );
            }
        }
        
        if (!count($files)) {
                $return['files']='Добавьте хотя бы один медиафайл!';
                $return['result'] = 'fail';			
        }        

        $oldversion = BS::DAO('document')->getById($fields['page_id']);
        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('document')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'author'=>$_SESSION['user']['id'],
				'text'=>  json_encode($files),
				'date'=>time(),
                                'type'=>'media',
                                'version'=>BS::DAO('document')->getLastVersion($fields['page_alias'])+1,
                                'visual'=>$fields['page_visual'],
			));

            $return['result']='ok';
            $return['uri']='/admin/documents/';
        }
		
        
        return $return;                
    }
    
    
    public function catchEditmarkdowndocumentform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['markdowntextedited']) && !trim($fields['markdowntextedited'])) {
                $return['epiceditor']='Введите текст документа!';
                $return['result'] = 'fail';			
        }		
		
        $oldversion = BS::DAO('document')->getById($fields['page_id']);
        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('document')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'author'=>$_SESSION['user']['id'],
				'text'=>$fields['markdowntextedited'],
				'date'=>time(),
                'type'=>'markdown',
                'version'=>BS::DAO('document')->getLastVersion($fields['page_alias'])+1,
			));

            $return['result']='ok';
            $return['uri']='/admin/documents/';
        }
		
        
        return $return;                
    }
    
    public function catchNewmediadocumentform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (BS::DAO('document')->getByFields(array('alias'=>$fields['page_alias']),null,true))
        {
            $return['page_alias']='Документ с таким алиасом уже существует';
            $return['result'] = 'fail';
        }
        
        // parse files
        
        $files = array();
        foreach ($fields as $key=>$value) {
            if (substr($key,0,5)=='file_') {
                $fname = str_replace('file_','',$key);
                $fname = str_replace('_name','',$fname);
                
                $files[] = array(
                    'name'=>$fname,
                    'desc'=>trim($value) ? trim($value):'',
                );
            }
        }
        
        if (!count($files)) {
                $return['files']='Добавьте хотя бы один медиафайл!';
                $return['result'] = 'fail';			
        }
        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('document')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'author'=>$_SESSION['user']['id'],
				'text'=>  json_encode($files),
				'date'=>time(),
                                'type'=>'media',
                                'version'=>1,
                                'visual'=>$fields['page_visual'],
			));

            $return['result']='ok';
            $return['uri']='/admin/documents/';
        }
		
        
        return $return;                
    }
    
    public function catchNewtextdocumentform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название документа не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['editor']) && !trim($fields['editor'])) {
                $return['editor']='Введите текст документа!';
                $return['result'] = 'fail';			
        }		
		
        if (BS::DAO('document')->getByFields(array('alias'=>$fields['page_alias']),null,true))
        {
            $return['page_alias']='Документ с таким алиасом уже существует';
            $return['result'] = 'fail';
        }
       

        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('document')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'author'=>$_SESSION['user']['id'],
				'text'=>$fields['editor'],
				'date'=>time(),
                                'type'=>'text',
                                'version'=>1,
			));

            $return['result']='ok';
            $return['uri']='/admin/documents/';
        }
		
        
        return $return;                
    }
    
    public function catchEditpageform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас страницы не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название страницы не может быть пустым';
                $return['result'] = 'fail';			
        }

        
		
        $availdocs = array();
        if (isset($fields['availabledocs'])) {
            $avail = json_decode($fields['availabledocs'],true);
            foreach($avail as $doc) {
                $availdocs[]=BS::DAO('document')->getById($doc['id']);
            }
        }
        
        
        if ($return['result']!='fail')
        {
            BS::DAO('resource')->updateItem($fields['page_id'],array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'desc'=>$fields['page_desc'],				
				'date'=>time(),
			));
            BS::DAO('res_doc')->clearAll($fields['page_id']);
            foreach ($availdocs as $key=>$doc) {
                BS::DAO('res_doc')->insertItem(array(
                   'resource'=>$fields['page_id'],
                    'document'=>$doc['alias'],
                    'level'=>$key,
                ));
            }

            $return['result']='ok';
            $return['uri']='/admin/pages/';
        }
		
        
        return $return;                
    }    
    

    public function catchNewpageform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id) && isset($item->value)) {
                    $fields [$item->id]= $item->value;
            }
        }
        
        if (isset($fields['page_alias']) && !trim($fields['page_alias'])) {
                $return['page_alias']='Алиас страницы не может быть пустым';
                $return['result'] = 'fail';			
        }

        if (isset($fields['page_name']) && !trim($fields['page_name'])) {
                $return['page_name']='Название страницы не может быть пустым';
                $return['result'] = 'fail';			
        }

        
		
        if (BS::DAO('resource')->getByFields(array('alias'=>$fields['page_alias']),null,true))
        {
            $return['page_alias']='Страница с таким алиасом уже существует';
            $return['result'] = 'fail';
        }
       $availdocs = array();
        if (isset($fields['availabledocs'])) {
            $avail = json_decode($fields['availabledocs'],true);
            foreach($avail as $doc) {
                $availdocs[]=BS::DAO('document')->getById($doc['id']);
            }
        }
        
        
        if ($return['result']!='fail')
        {
            $id = BS::DAO('resource')->insertItem(array(
				'alias'=>$fields['page_alias'],
				'name'=>$fields['page_name'],
				'desc'=>$fields['page_desc'],
				'author'=>$_SESSION['user']['id'],
                                'active'=>1,
				'date'=>time(),
			));
            foreach ($availdocs as $key=>$doc) {
                BS::DAO('res_doc')->insertItem(array(
                   'resource'=>$id,
                    'document'=>$doc['alias'],
                    'level'=>$key,
                ));
            }

            $return['result']='ok';
            $return['uri']='/admin/pages/';
        }
		
        
        return $return;                
    }    
    
    public function catchProjecteditform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if(!trim($fields['project_name']))
        {
            $return['msg']='Не заполнено поле "Название проекта"';
            $return['mark']='project_name';
        }
        
        if (!isset($return['msg']))
        {
            BS::DAO('project')->updateItem($fields['form_id'],array(
                'name'=>$fields['project_name'],
            ));
            
            BS::Log(array(
                   'action'=>'project_rename',
                   'subject'=>$fields['form_id'],
                   'params'=>array('project_name'=>$fields['project_name']),
            ));                
            
            $return['result']='ok';
            $return['function']='gotoProject';
	        $return['arg']=$fields['form_id'];		
        }        
        
        return $return;                
    }



    
    public function catchTaskaddform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        
        if(!trim($fields['task_name']))
        {
            $return['msg']='Не заполнено поле "Название задачи"';
            $return['mark']='task_name';
        }
        
        if ($fields['task_project']==74) {
    	    $return['msg'] = 'Project is closed, adding tasks prohibited';
    	    $return['mark'] = 'task_name';
        
        }
        
        $expiration = time()+3600*24*3;
        
        if (isset($fields['task_mailstone']) && $fields['task_mailstone']) {
            
            $ms = BS::DAO('mstone')->getById($fields['task_mailstone']);
            if ($ms) {
                $expiration = $ms['duedate'];
            } else $fields['task_mailstone'] = 0;
            
            
        } else $fields['task_mailstone'] = 0;
        
        if (isset($fields['task_expire'])&&(trim($fields['task_expire'])!='')) {
            $expiration = strtotime($fields['task_expire']);
        }
        
        // parse and insert tags here

        $tags = explode(',',$fields['task_tags']);
        
        foreach ($tags as $k=>$tag) {
            $tags[$k]=trim($tag);
            
            if ($tags[$k]!='')
            
            BS::DAO('tag')->insertItem(array(
                'name'=>$tags[$k],
                'user_id'=>$_SESSION['user']['id'],
            ));
            
        }        
        
        
        if (!isset($return['msg']))
        {
            $id = BS::DAO('task')->insertItem(array(
                'name'=>$fields['task_name'],
                'desc'=>$fields['task_description'],
        		'priority'=>$fields['task_priority'],
        		'added'=>time(),
        		'status'=>'new',
        		'repeatable'=>0,
        		'project_id'=>$fields['task_project'],
        		'expiration'=>$expiration,
                'milestone_id'=>$fields['task_mailstone'],	
                'author'=> $_SESSION['user']['id'],	
            ));
            
            $project = BS::DAO('project')->getById($fields['task_project']);
            
            if ($project) {
                $owner = $project['owner'];
                
                $owner = BS::DAO('user')->getById($owner);
                if ($owner['phone']) {
                    BS::Sms('Создана задача "'.$fields['task_name'].'" в проекте "'.$project['name'].'"')->send(array(
                        $owner['phone'],
                    ));
                }
            }
            
            
            foreach ($tags as $k=>$tag) {
                $tag = BS::DAO('tag')->getByFields(array('name'=>$tag,'user_id'=>$_SESSION['user']['id']),null,true);
                
                if ($tag)
                BS::DAO('task_tag')->insertItem(array(
                    'task_id'=>$id,
                    'tag_id'=>$tag['id'],
                ));
            }
            
            BS::DAO('file')->activateFiles($fields['tmp_id'],$id);
            
            $return['result']='ok';
            $return['function']='refreshProject';
        }        
        
        return $return;                
    }
    
    public function catchTaskeditform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            if(isset($item->id)) {
                $fields [$item->id]= $item->value;
            }
        }
        
        
        if(!trim($fields['task_name']))
        {
            $return['msg']='Не заполнено поле "Название задачи"';
            $return['mark']='task_name';
        }
        
        $expiration = time()+3600*24*3;
        
        if (isset($fields['task_mailstone']) && $fields['task_mailstone']) {
            
            $ms = BS::DAO('mstone')->getById($fields['task_mailstone']);
            if ($ms) {
                $expiration = $ms['duedate'];
            } else $fields['task_mailstone'] = 0;
            
            
        } else $fields['task_mailstone'] = 0;
        
        if (isset($fields['task_expire2'])&&(trim($fields['task_expire2'])!='')) {
            $expiration = strtotime($fields['task_expire2']);
        }
        
        // parse and insert tags here

        $tags = explode(',',$fields['task_tags2']);
        
        foreach ($tags as $k=>$tag) {
            $tags[$k]=trim($tag);
            
            if ($tags[$k]!='')
            
            BS::DAO('tag')->insertItem(array(
                'name'=>$tags[$k],
                'user_id'=>$_SESSION['user']['id'],
            ));
            
        }        
        
        
        if (!isset($return['msg']))
        {
            BS::DAO('task')->updateItem($fields['form_id'],array(
                'name'=>$fields['task_name'],
                'desc'=>$fields['task_description'],
        		'priority'=>$fields['task_priority'],
        		'expiration'=>$expiration,
                'milestone_id'=>$fields['task_mailstone'],		
            ));
            
            BS::DAO('tag')->removeTaskTags($fields['form_id']);
            
            foreach ($tags as $k=>$tag) {
                $tag = BS::DAO('tag')->getByFields(array('name'=>$tag,'user_id'=>$_SESSION['user']['id']),null,true);
                
                if ($tag)
                BS::DAO('task_tag')->insertItem(array(
                    'task_id'=>$fields['form_id'],
                    'tag_id'=>$tag['id'],
                ));
                
                
                
            }
            
            $return['result']='ok';
            $return['function']='refreshProject';
        }        
        
        return $return;                
    }
    
    
    public function catchNewcategoryform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if(!trim($fields['form_name']))
        {
            $return['msg']='Не заполнено поле "Название категории"';
            $return['mark']='form_name';
        }
        
        if (!isset($return['msg']))
        {
            BS::DAO('category')->insertItem(array(
                'name'=>$fields['form_name'],
                'testplan_id'=>$fields['form_testplan'],
            ));
            
            $return['result']='ok';
            $return['uri']='/testplans/'.$fields['form_testplan'].'/';
        }        
        
        return $return;                
    }    

    public function catchRegisterform($data)
    {
        $return = array('result'=>'ok');
        $fields = array();

        
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        //checks
        
        // check if user exists
        
        if ($u = BS::DAO('user')->getByFields(array('mail'=>$fields['reg_mail']),null,true))
        {

            if ($u['role']==0) {
                $return['reg_mail']='Вы почти зарегистрированы, дождитесь письма со ссылкой подтверждения';
                $return['result']='fail';
            } else {
                $return['reg_mail']='<!--{t:_user_exists}-->';
                $return['result']='fail';
            }
        }        

        if (mb_strlen($fields['reg_pass'],'UTF-8')<4)
        {
            $return['reg_pass']='<!--{t:_pass_too_short}-->';
            $return['result']='fail';
        }
        if ($fields['reg_pass']!=$fields['reg_pass2'])
        {
            $return['reg_pass2']='Введенные пароли не совпадают';
            $return['result']='fail';
        }
        
        if (!$this->is_email($fields['reg_mail']))
        {
            $return['reg_mail']='<!--{t:_mail_not_valid}-->';
            $return['result']='fail';
        }
             
        if ($return['result']!='fail')
        {
            

            $res = BS::DAO('user')->insertItem(array(
                'mail'=>$fields['reg_mail'],
                'pass'=>$fields['reg_pass'],
                'phone'=>'',
                'role'=>0,
                'translation'=>'russian',
            )); 
            
            if ($res)
            {
                $link = md5(rand(1,1000).'__'.microtime().'check'.$res);
                
                BS::DAO('settings')->insertItem(array(
                    'user'=>$res,
                    'entity'=>'link',
                    'value'=>$link,
                ));
                
                BS::Log(array(
                    'action'=>'user_register',
                    'subject'=>$res,
                ));                    
                
                $letter = BS::Visual()->preloadtemplate('letter');
                $letter = BS::Visual()->pagifyText(array(
                    'name'=>$fields['reg_mail'],
                    'sitename'=>BS::getConfig('site.name'),
                    'domain'=>BS::getConfig('site.domain'),
                    'mail'=>$fields['reg_mail'],
                    'pass'=>$fields['reg_pass'],
                    'verifyLink'=>'http://'.BS::getConfig('site.domain').'/verify/'.$link.'/',
                    ),$letter);
                    
                    
                BS::Mqueue()->send($fields['reg_mail'],'Регистрация',$letter);
                
                $return['result']='ok';
                $return['msg']='Благодарим Вас за регистрацию! Пожалуйста, дождитесь письма на указанный Вами e-mail ('.$fields['reg_mail'].'). Мы пришлем Вам ссылку для подтверждения регистрации.<br /><br />';
            }
            else
            {
                $return['result']='check';
                $return['msg']='Failed to register user';
            }
            
        }
        
        //return
        
        return $return;
    }
	
  public function catchFsignupform($data)
    {
        $return = array('result'=>'ok');
        $fields = array();

        
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        //checks
        
        // check if user exists
        
        if ($u = BS::DAO('user')->getByFields(array('mail'=>$fields['fmail']),null,true))
        {

            if ($u['role']==0) {
                $return['fmail']='Вы почти зарегистрированы, дождитесь письма со ссылкой подтверждения';
                $return['result']='fail';
            } else {
                $return['fmail']='<!--{t:_user_exists}-->';
                $return['result']='fail';
            }
        }        

        if (mb_strlen($fields['fpass1'],'UTF-8')<4)
        {
            $return['fpass1']='<!--{t:_pass_too_short}-->';
            $return['result']='fail';
        }
        if ($fields['fpass1']!=$fields['fpass2'])
        {
            $return['fpass2']='Введенные пароли не совпадают';
            $return['result']='fail';
        }
        
        if (!$this->is_email($fields['fmail']))
        {
            $return['fmail']='<!--{t:_mail_not_valid}-->';
            $return['result']='fail';
        }
             
        if ($return['result']!='fail')
        {
            

            $res = BS::DAO('user')->insertItem(array(
                'mail'=>$fields['fmail'],
                'pass'=>$fields['fpass1'],
                'phone'=>'',
                'role'=>0,
                'translation'=>'russian',
            )); 
            
            if ($res)
            {
                $link = md5(rand(1,1000).'__'.microtime().'check'.$res);
                
                BS::DAO('settings')->insertItem(array(
                    'user'=>$res,
                    'entity'=>'link',
                    'value'=>$link,
                ));
                
                BS::Log(array(
                    'action'=>'user_register',
                    'subject'=>$res,
                ));                    
                
                $letter = BS::Visual()->preloadtemplate('letter');
                $letter = BS::Visual()->pagifyText(array(
                    'name'=>$fields['fmail'],
                    'sitename'=>BS::getConfig('site.name'),
                    'domain'=>BS::getConfig('site.domain'),
                    'mail'=>$fields['fmail'],
                    'pass'=>$fields['fpass1'],
                    'verifyLink'=>'http://'.BS::getConfig('site.domain').'/verify/'.$link.'/',
                    ),$letter);
                    
                    
                BS::Mqueue()->send($fields['fmail'],'Регистрация',$letter);
                
                $return['result']='ok';
                $return['msg']='Благодарим Вас за регистрацию! Пожалуйста, дождитесь письма на указанный Вами e-mail ('.$fields['fmail'].'). Мы пришлем Вам ссылку для подтверждения регистрации.<br /><br />';
            }
            else
            {
                $return['result']='check';
                $return['msg']='Failed to register user';
            }
            
        }
        
        //return
        
        return $return;
    }	

    public function catchFloginform($data)
    {
        $return = array('result'=>'ok');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if ($u = BS::DAO('user')->getByFields(array('mail'=>$fields['flogin'],'pass'=>$fields['fpass']),null,true))
        {
            if ($u['role']==0)
            {
                $return['flogin']='<!--{t:_verify_sent}-->';
                $return['result']='fail';
            }
            
        }
        else
        {        
            $return['flogin']='<!--{t:_login_not_correct}-->';
            $return['result'] = 'fail';
        }


        
        if ($return['result']!='fail')
        {
            
            $_SESSION['user'] = array('id'=>$u['id'],'mail'=>$u['mail'],'role'=>$u['role']);
            session_write_close();
            
            $return['result']='ok';
            $return['uri']='/main/';
        }
        
        //return
        
        return $return;
    }    
    
    public function catchLoginform($data)
    {
        $return = array('result'=>'ok');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if ($u = BS::DAO('user')->getByFields(array('mail'=>$fields['login'],'pass'=>$fields['pass']),null,true))
        {
            if ($u['role']==0)
            {
                $return['login']='<!--{t:_verify_sent}-->';
                $return['result']='fail';
            }
            
        }
        else
        {        
            $return['login']='<!--{t:_login_not_correct}-->';
            $return['result'] = 'fail';
        }


        
        if ($return['result']!='fail')
        {
            
            $_SESSION['user'] = array('id'=>$u['id'],'mail'=>$u['mail'],'role'=>$u['role']);
            session_write_close();
            
            $return['result']='ok';
            $return['uri']='/main/';
        }
        
        //return
        
        return $return;
    }
    
    public function catchWelcomeform($data)
    {
        $return = array('result'=>'check');
        $fields = array();
        
        foreach ($data as $item)
        {
            $fields [$item->id]= $item->value;
        }
        
        if (mb_strlen($fields['login_name'],'UTF-8')<2)
        {
            $return['msg']='Имя не может быть таким коротким.';
            $return['mark']='login_name';
        }


        
        if (!isset($return['msg']))
        {
            
            BS::DAO('user')->updateItem($_SESSION['user']['id'],array(
                'name'=>$fields['login_name'],
                'phone'=>$fields['login_phone'],
            ));
            
            $_SESSION['user']['phone'] = $fields['login_phone'];
            $_SESSION['user']['name'] = $fields['login_name'];
            
            $return['result']='ok';
            $return['uri']='/';
        }
        
        //return
        
        return $return;
    }    
    
    
}

class preparator
{
    public function prepareProjecteditform($id) {
        $project = BS::DAO('project')->getById($id);
        if (!$project) return false;
        
        return array('project_name'=>$project['name']);
        
    }
    
    public function prepareMstoneeditform($id) {
        $mstone = BS::DAO('mstone')->getById($id);
        if (!$mstone) return false;
        
        return array('mstone_name'=>$mstone['name'],'mstone_date2'=>date('Y-m-d',$mstone['duedate']));
        
    }
    
    public function prepareTaskeditform($id) {
        $task = BS::DAO('task')->getById($id);
        if (!$task) return false;
        
        $tags = BS::DAO('tag')->getTaskTags($id);
        $tags[] = ' ';
        
        $tags = implode(', ',$tags);
        
        return array(
            'task_name'=>$task['name'],
            'task_description'=>$task['desc'],
            'task_mailstone'=>$task['milestone_id'],
            'task_expire2'=>date('Y-m-d',$task['expiration']),
            'task_tags2'=>$tags,
            'task_priority'=>$task['priority']>0?'+'.$task['priority']:$task['priority'],
        );
        
    }            
}


class app_catcher
{
    var $version='App_Catcher v.0.1';
    var $depends=array();
    public function init()
    {
        
    }
    
    public function run()
    {
		BS::Visual()->template['name']='ajax.tpl';
        BS::Visual()->text = '';
        
        $this->catcher = new catcher();
        $this->preparator = new preparator();
        
        if (isset($_POST['method'])&&($_POST['method']=='catch'))
        {
            $method = 'catch'.ucfirst($_POST['entity']);
            if (method_exists($this->catcher,$method))
            {
                $result = $this->catcher->$method(json_decode($_POST['data']));
                BS::Visual()->text = json_encode($result);   
            }
            else BS::Visual()->text = json_encode(array('result'=>'fail','method'=>$method));
        }
        
        if (isset($_POST['method'])&&($_POST['method']=='prepareForm'))
        {
            $method = 'prepare'.ucfirst($_POST['form']);
            if (method_exists($this->preparator,$method))
            {
                $result = $this->preparator->$method($_POST['id']);
                BS::Visual()->text = json_encode($result);   
            }
            else BS::Visual()->text = json_encode(array('result'=>'fail','method'=>$method));
        }
        
    }
}
?>