<?php
class app_quest
{
	public $version='App_404 v.0.1';
	public $depends=array();

	public function init()
	{

	}

	public function ucfirst($string, $e = 'utf-8')
	{
		if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
			$string = mb_strtolower($string, $e);
			$upper = mb_strtoupper($string, $e);
			preg_match('#(.)#us', $upper, $matches);
			$string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
		} else {
			$string = ucfirst($string);
		}
		return $string;
	}

	public function run()
	{
		die;
		echo 'Quest started'."\n";

		$xp = file('questions.txt');

		foreach ($xp as $line) {
			$line = trim($line);
			$line = iconv('cp1251', 'UTF-8', $line);

			list($q,$a) = explode('|', $line);

			if ($q && $a) {

			$question = array(
				'text'=>$q,
				'a1'=> $this->ucfirst($a),
				'a2'=> '-',
				'a3'=> '-',
				'a4'=> '-',
				'guid'=> md5(rand(1, 1000000).  microtime(true)),
			);

			BS::DAO('question')->insertItem($question);
			//die;
			}

		}

		echo "\n";
		die;
	}
}
