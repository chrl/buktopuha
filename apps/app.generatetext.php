<?php

class app_generatetext
{
	public $version='App_Enter v.0.1';
	public $depends=array('pq','generator','namelookup');


	public function init()
	{

	}

	public $props = array(
		'original'=>'Оригинал',
		'lastname'=>'Фамилия',
		'name'=>'Имя',
		'patronim'=>'Отчество',
		'gender'=>'Пол',
		'city'=>'Город',
		'address'=>'Адрес',
		'aprecision'=>'Точность',
		'phone'=>'Телефон',
		'operator'=>'Оператор',
		'region'=>'Регион',
		'tzoffset'=>'Часовой пояс',
				'email'=>'E-Mail',
		'birthdate'=>'Дата',
	);

	public function run()
	{

		BS::Visual()->title = 'Datarich.ru - Results';
		$name = $_SESSION['dbase'];

		$persons = file_get_contents(CACHEDIR.'/file.'.$name.'.persons');
		$persons = json_decode($persons, true);

		$person = $persons[array_rand($persons)];

		BS::Visual()->subjectdata = '';
		$data = 0;
		foreach ($person as $key => $value) {
			BS::Visual()->subjectdata.='<a href="#" class="delement list-group-item"><span class="ditem">'.$value.'</span><span class="badge">'.(isset($this->props[$key])?$this->props[$key]:'Данные '.++$data).'</span></a>';
		}

		BS::Visual()->template['name'] = 'generatetext.tpl';

	}
}
