<?php
class app_categ
{
	public $version='App_404 v.0.1';
	public $depends=array();

	public function init()
	{

	}
	public function run()
	{
		BS::Visual()->template['name'] = 'dashcat.tpl';

		$cats = array(
			'science'=>'Наука и техника',
			'medic'=>'Медицина',
			'religion'=>'Религия',
			'none'=>'Без категории',
			'music'=>'Музыка',
			'math'=>'Математика',
			'physics'=>'Физика',
			'history'=>'История',
			'comps'=>'Компьютеры',
			'russia'=>'Россия',
			'politics'=>'Политика',
			'tvshow'=>'Телевидение',
			'geography'=>'География',
			'persons'=>'Личности',
			'sports'=>'Спорт',
			'zoo'=>'Флора и фауна',
			'cinema'=>'Кино и мультики',
			'literature'=>'Литература',
			'hobby'=>'Хобби',
			'tech'=>'Технологии',

		);

		BS::Visual()->cats = json_encode($cats);

	}
}
