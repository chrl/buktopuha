<?php

class app_pushserver
{
	public $depends = array('apns');

	public function run()
	{

		exec('ps ax | grep pushserver', $output);
		if (count($output)>4) {
			//var_dump($output);
			die();
		}
		echo 'Started'."\n";

		date_default_timezone_set('Europe/Moscow');

		// Report all PHP errors
		error_reporting(-1);

		// Instanciate a new ApnsPHP_Push object
		$server = new ApnsPHP_Push_Server(
   ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
   'apns-pro.pem'
		);

		// Set the number of concurrent processes
		$server->setProcesses(2);

		// Starts the server forking the new processes
		$server->start();

		// Main loop...
		$i = 1;
		while ($server->run()) {

				// Check the error queue
				$aErrorQueue = $server->getErrors();
				if (!empty($aErrorQueue)) {
						// Do somethings with this error messages...
						var_dump($aErrorQueue);
				}

				$notifications = BS::DAO('notification')->getNew();

				if ($notifications) {
					foreach ($notifications as $notification) {
						// Instantiate a new Message with a single recipient
						$message = new ApnsPHP_Message($notification['apnstoken']);
						$message->setCustomIdentifier("message-".md5($notification['id'].rand(1, 19999999).microtime(true)));
						$message->setBadge((int)$notification['badge']);
						$message->setText($notification['message']);
						$message->setSound();
						$message->setCustomProperty('messageid', $notification['id']);
						$message->setCustomProperty('messageguid', md5($notification['id']));
						$message->setExpiry(30);
						$server->add($message);

						BS::DAO('notification')->updateItem($notification['id'], array(
							'sent'=>time(),
							'status'=>1,
						));
					}
				}

				// Sleep a little...
				sleep(2);
		}

	}
}
