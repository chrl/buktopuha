<?php

class app_addimage
{


	public function run()
	{
		$data = json_decode(file_get_contents('php://input'), true);

		BS::DAO('instupload')->insertItem(array(
			'url'=>$data['url'],
			'imagefile'=>'',
			'date_changed'=>time(),
			'status'=>0,
		));

		return array('result'=>'ok');

	}
}
