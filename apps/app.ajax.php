<?php
/**
 * @package use
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/


class ajax // коллекция методов обработки
{

	public function axMakealias($data)
	{
		$alias = $data['alias'];

		$alias = $this->getAlias($alias);

		return array('result'=>'ok','alias'=>$alias);
	}

	public function axPostCallback($data)
	{
		BS::Sms('Callback: '.mb_substr($data['phone'], 0, 50, 'UTF-8'))->send(array('79672977000','79265501768'));
		return array('result'=>'ok');
	}


	public function axMakeusage($data)
	{
		$usage = array('objects'=>array(),'usage'=>0);
		$name = $_SESSION['dbase'];

		foreach ($data['usage'] as $item) {
			$usage['objects'][] = array($item);
		}

		file_put_contents(CACHEDIR.'/file.'.$name.'.usage', json_encode($usage));

		return array('result'=>'ok');
	}

	public $props = array(
		'original'=>'Оригинал',
		'lastname'=>'Фамилия',
		'name'=>'Имя',
		'patronim'=>'Отчество',
		'gender'=>'Пол',
		'city'=>'Город',
		'address'=>'Адрес',
		'aprecision'=>'Точность',
		'phone'=>'Телефон',
		'operator'=>'Оператор',
		'region'=>'Регион',
		'tzoffset'=>'Часовой пояс',
		'email'=>'E-Mail',
		'birthdate'=>'Дата',
	);

	public function axGetuser($data)
	{
		$name = $_SESSION['dbase'];

		$persons = file_get_contents(CACHEDIR.'/file.'.$name.'.persons');
		$persons = json_decode($persons, true);

		$person = $persons[array_rand($persons)];

		$subjectdata = '';
		$data = 0;
		foreach ($person as $key => $value) {
			$subjectdata.='<a href="#" class="delement list-group-item"><span class="ditem">'.$value.'</span><span class="badge">'.(isset($this->props[$key])?$this->props[$key]:'Данные '.++$data).'</span></a>';
		}

		echo $subjectdata;
die;

	}

}

/**
 * app_ajax
 * Приложение - коллекция, обработка ajax-запросов
 *
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @access public
 */

class app_ajax
{
	var $version='App_Ajax v.0.1';
	var $depends=array('ytranslate','sms');
	public function init()
	{

	}

	public function run()
	{
		BS::Visual()->template['name']='ajax.tpl';
		BS::Visual()->text = '';

		$this->worker = new ajax();

		if (isset($_POST['method'])) {
		$method = 'ax'.ucfirst($_POST['method']);
			if (method_exists($this->worker, $method)) {
			$result = $this->worker->$method($_POST);
				BS::Visual()->text = isset($_POST['encode'])&&($_POST['encode']=='false')?$result:json_encode($result);
			} else {
BS::Visual()->text = json_encode(array('result'=>'fail','method'=>$method));
	  }
		}
	}
}
