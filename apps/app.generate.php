<?php
class gperson
{

	public $props = array(
		'original'=>'Оригинал',
		'lastname'=>'Фамилия',
		'name'=>'Имя',
		'patronim'=>'Отчество',
		'gender'=>'Пол',
		'city'=>'Город',
		'address'=>'Адрес',
		'aprecision'=>'Точность',
		'phone'=>'Телефон',
		'operator'=>'Оператор',
		'region'=>'Регион',
		'tzoffset'=>'Часовой пояс',
		'email'=>'E-Mail',
		'birthdate'=>'Дата',
	);

		public $corrections = array(

		);

	public $values = array(

	);

	public function __construct()
	{

	}

	public function setData($column, $data)
	{
		$this->values['data_'.$column] = trim(trim($data), '"');
	}

	protected function phone_number($sPhone)
	{
		$sArea = substr($sPhone, 2, 3);
		$sPrefix = substr($sPhone, 5, 3);
		$sNumber = substr($sPhone, 8, 4);
		$sPhone = "<nobr />+7 (".$sArea.") ".$sPrefix."-".$sNumber.'</nobr>';
		return($sPhone);
	}

	public function parseAddress($address)
	{

		$res=file_get_contents("http://geocode-maps.yandex.ru/1.x/?geocode=".urlencode($address)."&format=json");
		$res = json_decode($res, true);

		$num = $res['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'];

		if ($num>0) {
			$rp = $res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData'];

			$this->values['address']=$rp['text'];
			$this->values['aprecision']=$rp['precision'];
			$this->values['city']=$rp['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
		}

	}

	public function findOperator($phone)
	{
		$res = BS::DAO('defcode')->getOperator($phone);

		return $res;
	}

	public function parsePhone($phone)
	{
		$this->values['phone'] = preg_replace('/[^\d]/', '', $phone);

		if (strlen($this->values['phone'])==11) {
			if (substr($this->values['phone'], 0, 1)=='8') {
				$this->values['phone'] = '+7'.substr($this->values['phone'], 1);
								$this->corrections['phone'][]='fix_country';
			}
			if (substr($this->values['phone'], 0, 1)=='7') {
				$this->values['phone'] = '+'.$this->values['phone'];
								$this->corrections['phone'][]='add_leading_plus';
			}
		}

		if (strlen($this->values['phone'])==10) {
			$this->values['phone'] = '+7'.$this->values['phone'];
						$this->corrections['phone'][]='fix_country';
		}
		if (strlen($this->values['phone'])<10) {

			if (isset($this->values['city'])) {

				$code = BS::DAO('citycode')->getByFields(array('city'=>$this->values['city']), null, true);

				$code = $code['code'];
				if (false!==strpos($code, ',')) {
					list($code) = explode(',', $code);
				}
				$code = trim($code);

				$this->values['phone'] = '+7'.$code.$this->values['phone'];
								$this->corrections['phone'][]='add_city_code';
			}

		}

		$operator = $this->findOperator($this->values['phone']);

		if ($operator) {
			$this->values['operator'] = $operator['operator'];
			$this->values['region'] = $operator['region'];
			$this->values['tzoffset'] = substr($operator['tzoffset'], 0, 1) == '-' ? 'MSK'.$operator['tzoffset']:'MSK+'.$operator['tzoffset'];
						$this->corrections['phone'][]='find_operator';
		} else {
			$this->values['operator'] = '&mdash;';
			$this->values['region'] = '&mdash;';
			$this->values['tzoffset'] = '&mdash;';
		}

		$this->values['phone'] = $this->phone_number($this->values['phone']);

	}

	public function recheckName($name)
	{
		$check = array(
			'Ната'=>'Наталья',
			'Наташа'=>'Наталья',
			'Коля'=>'Николай',
		);

		if (isset($check[$name])) {

					$this->corrections['fio'][] = 'fix_name';
					return $check[$name];
		}

		return $name;
	}

	public function parseEmail($item)
	{
			$item = preg_replace('/\s/', '', $item);

			if (filter_var($item, FILTER_VALIDATE_EMAIL)) {
				$this->values['email'] = $item;
			} else {
				$this->values['email'] = '&mdash;';
			}
	}


	public function parseFio($fio)
	{
		$r = BS::Namelookup()->getNameArrayFormat($fio);

		foreach (array('name','lastname','patronim') as $item) {
			$this->values[$item] = isset($r[$item])?
				mb_strtoupper(mb_substr($r[$item], 0, 1, 'UTF-8'), 'UTF-8')
				.mb_strtolower(mb_substr($r[$item], 1, mb_strlen($r[$item], 'UTF-8'), 'UTF-8'), 'UTF-8')
				:'&mdash;';
						if (isset($r[$item])) {
							$this->corrections['fio'][]='find_'.$item;
						}
		}

		if ($this->values['name']!='&mdash;') {
			$this->values['name'] = $this->recheckName($this->values['name']);
		}

		$this->values['original'] = $fio;

		$this->values['gender'] = BS::Namelookup()->genderDetect($fio)==1?'Мужской':'Женский';
				$this->corrections['gender'][]='find_gender';
	}

	public function parseBirthdate($item)
	{

		$item = str_replace('/', '.', $item);
		$cnv = strtotime($item);
		if (!$cnv) {
			$item='01.'.$item;
			$cnv = strtotime($item);
		}

		$this->values['birthdate'] = $cnv ? date('Y.m.d H:i:s', $cnv):'&mdash;';

	}

}

class app_generate
{
	public $version='App_Enter v.0.1';
	public $depends=array('pq','generator','namelookup');

	public $delimeter = ';';


	public function init()
	{

	}

	public function generatePerson($string, $usage)
	{

		$person = new gperson();
		$string = explode($this->delimeter, $string);

		foreach ($usage['objects'] as $key => $item) {
			if (count($item)!=0) {
				$method = $item[0];

				if (method_exists($person, 'parse'.  ucfirst($method))) {
					$person->{'parse'.ucfirst($method)}(trim(trim($string[$key]), '"'));
				}

								if ($method == 'leave') {
									$person->setData($key, $string[$key]);
								}

			} else {
				$person->setData($key, $string[$key]);
			}
		}

		return $person;
	}

	public function generateCorrections(gperson $person)
	{
		$text= array();

		$translations = array(
			'add_leading_plus'=>'Добавили + в телефон',
			'fix_name'=>'Исправили опечатки в имени',
			'find_operator'=>'Определили оператора',
			'find_name'=>'Определили имя',
			'find_lastname'=>'Определили фамилию',
			'find_gender'=>'Определили пол',
		);

		$labels = array(
			'phone'=>'success',
			'fio'=>'primary'
		);

		foreach ($person->corrections as $citem => $cvalue) {
			foreach ($cvalue as $ci) {
				$text[]='<span class="label label-'.(isset($labels[$citem])?$labels[$citem]:'default').'">'.(isset($translations[$ci])?$translations[$ci]:$ci).'</span>';
			}
		}

		return implode('&nbsp;', $text);
	}

	public function run()
	{
		BS::Visual()->title = 'Datarich.ru - Results';
		$name = $_SESSION['dbase'];

		BS::Visual()->template['name'] = 'generate.tpl';

		$x = file(CACHEDIR.'/file.'.$name.'.save');
		$usage = json_decode(file_get_contents(CACHEDIR.'/file.'.$name.'.usage'), true);

		$samplePerson = new gperson();

		require_once('apps/app.validate.php');
		$validate = new app_validate();

		$persons = array();

				$enc =$validate->detect_encoding($x[0]);
				if ($enc!='utf-8') {
					foreach ($x as $k => $item) {
						$x[$k] = iconv($enc, 'UTF-8', $x[$k]);
					}
				}

				$this->delimeter = $validate->findDelimiter($x);

		for ($xt=0; $xt< count($x); $xt++) {

			if ($xt==0) {
				$rp = $validate->supposeString($x[$xt]);
				$cont = true;
				foreach ($rp as $rpt) {
					if (isset($rpt[0]) && (($rpt[0]=='fio') || ($rpt[0]=='phone'))) {
						$cont = false;
					}
				}

				if ($cont) {
continue;
	   }
			}

			$person = $this->generatePerson($x[$xt], $usage);

			$persons[] = $person;

		}

		// fix bug with pseudo-patronims finishing on 'vich'

		$mcontents = array();

		foreach ($persons as $person) {
			$sap = array();
			foreach ($person->values as $k => $v) {
				if ($v!='&mdash;') {
					$sap[] = $k;
				}
			}

			$sap2 = md5(json_encode($sap));

			if (!isset($mcontents[$sap2])) {
				$mcontents[$sap2]=array(
					'usage'=>0,
					'sap'=>$sap,
					'sap2'=>$sap2,
				);
			}

			$mcontents[$sap2]['usage']++;
		}

		usort($mcontents, array($validate,"sortByUsage"));
		unset($mcontents[0]);

		// output

		// now findout which columns we need to include

		$outheaders = array();

		foreach ($samplePerson->props as $id => $item) {

			foreach ($persons as $person) {
				if (isset($person->values[$id]) && (
					$person->values[$id]!='&mdash'
				)) {
					$outheaders[$id] = $item;
					break;
				}
			}
		}

		foreach ($persons[0]->values as $k => $item) {
			if (false!==strpos($k, 'data_')) {
				$outheaders[$k] = 'Данные';
			}
		}

		BS::Visual()->text='<table id="genresult" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" style="background-color:white;"><thead><tr><th></th>';

		foreach ($outheaders as $id => $item) {

			BS::Visual()->text.='<th>'.$item.'</th>';
		}
		BS::Visual()->text.='<th>Исправления</th>';
		BS::Visual()->text.='</tr></thead><tbody>';

		$j=0;

		$savepers = array();

		foreach ($persons as $id => $person) {

			$j++;

			$sap = array();
			foreach ($person->values as $k => $v) {
				if ($v!='&mdash;') {
					$sap[] = $k;
				}
			}

			$sap2 = md5(json_encode($sap));
			$standard = true;
			foreach ($mcontents as $k => $v) {
				if ($v['sap2'] == $sap2) {
$standard = false;
	   }
			}

			if (!$standard) {
				// 1st case -- have patronim, finished on wich, have no name
				if (isset($person->values['patronim']) && isset($person->values['lastname']) && ($person->values['lastname']=='&mdash;')) {
					if (mb_substr($person->values['patronim'], -3, 3, 'UTF-8')=='вич') {
						$person->values['lastname'] = $person->values['patronim'];
						$person->values['patronim'] = '&mdash;';
						$person->values['gender'] = BS::Namelookup()->genderDetect($person->values['name'])==1?'Мужской':'Женский';

						$standard = true;
					}
				}

				// 2nd case -- have name, have no lastname or patronim, original consists of two words

				if (isset($person->values['name']) && isset($person->values['lastname']) && ($person->values['lastname']=='&mdash;') && ($person->values['name']!='&mdash;') && ($person->values['patronim']=='&mdash;')) {

					if (count(explode(' ', trim($person->values['original'])))==2) {
					//haha found it!

						$person->values['lastname'] = $person->values['name'];
						$person->values['name'] = trim(str_replace($person->values['lastname'], '', $person->values['original']));

						$person->values['gender'] = BS::Namelookup()->genderDetect($person->values['name'])==1?'Мужской':'Женский';

					}
				}

			}

			BS::Visual()->text.=$standard ? '<tr>':'<tr class="danger">';

			BS::Visual()->text.='<td>'.$j.'</td>';

			foreach ($outheaders as $id => $item) {
				BS::Visual()->text.='<td><nobr>'.(isset($person->values[$id])?$person->values[$id]:'&mdash;').'</nobr></td>';
			}

			$savepers[] = $person->values;

			BS::Visual()->text.='<td>'.$this->generateCorrections($person).'</td>';
			BS::Visual()->text.='</tr>';
		}

		file_put_contents(CACHEDIR.'/file.'.$name.'.persons', json_encode($savepers));

		BS::Visual()->text.='</tbody></table>';

	}
}
