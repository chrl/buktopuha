<?php
class app_instatest2
{
	public $version='App_Enter v.0.1';
	public $depends=array('pq');

	public $cli = true;

	function my_mb_ucfirst($str)
	{
		$fc = mb_strtoupper(mb_substr($str, 0, 1, 'UTF-8'), 'UTF-8');
		return $fc.mb_substr($str, 1, null, 'UTF-8');
	}

	public function run()
	{

		echo "Getting questions...\n";
		$questions = BS::DAO('question')->getNotNone();
		$cnt = count($questions);
		echo 'Got questions: '.$cnt."\n";

		$time = time();
		$done = 0;
		$lastdone = 0;

		foreach ($questions as $id => $item) {
			//$item['text'] = 'Радиус ядра';

			$answer = BS::DAO('answer')->getByFields(array('question_id'=>$item['id']), null, true);

			$toput = array($answer['text']);

			$moreanswers = BS::DAO('answer')->getByFields(
	array(
						'category'=>$item['category'],
						'rod'=>$answer['rod'],
						'type'=>$answer['type'],
						'alive'=>$answer['alive'],
					), null, false, array('RAND()',''), 20
			);

			foreach ($moreanswers as $answeritem) {
				if (count($toput)==4) {
					break;
				}

				if (!in_array($answeritem['text'], $toput)) {
					$toput[]=$answeritem['text'];
				}
			}

			if (count($toput) < 4) {
				echo 'No answers for q: '.$done."\n";
				$moreanswers = BS::DAO('answer')->getByFields(
	 array(
							'category'=>$item['category'],

							'type'=>$answer['type'],

						), null, false, array('RAND()',''), 20
				);

				foreach ($moreanswers as $answeritem) {
					if (count($toput)==4) {
						break;
					}

					if (!in_array($answeritem['text'], $toput)) {
						$toput[]=$answeritem['text'];
					}
				}

			}

			if (count($toput) == 4) {

				foreach ($toput as $k => $i) {
					$toput[$k] = $this->my_mb_ucfirst($i);
				}

				//var_dump($toput);die;

				BS::DAO('question')->updateItem($item['id'], array(
					'a1'=>$toput[0],
					'a2'=>$toput[1],
					'a3'=>$toput[2],
					'a4'=>$toput[3],
				));
			} else {
				echo 'Giving up with question '.$item['id']."\n";
			}

			$done++;

			if ($time+10<time()) {
				$time = time();

				echo 'Done '.$done.' of '.$cnt.' ('.(round(1000*$done/$cnt)/10)."%), ".(round(10*($done-$lastdone)/10)/10)." w/sec\n";

				$lastdone = $done;
			}
			//die;
		}

	}
}
