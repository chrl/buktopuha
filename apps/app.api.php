<?php
/**
 * @package datarich
 * @author Charlie <charlie@chrl.ru>
*/


class apiworker // коллекция методов обработки
{

	
	public $groups = array(
		'common'=>'Общие методы',
                'user'=>'Пользователи',
                'question'=>'Вопросы'

	);
	
        protected $cats = array(
                'science'=>'Наука и техника',
                'medic'=>'Медицина',
                'religion'=>'Религия',
                'none'=>'Без категории',
                'music'=>'Музыка',
                'math'=>'Математика',
                'physics'=>'Физика',
                'history'=>'История',
                'comps'=>'Компьютеры',
                'russia'=>'Россия',
                'politics'=>'Политика',
                'tvshow'=>'Телевидение',
                'geography'=>'География',
                'persons'=>'Личности',
                'sports'=>'Спорт',
                'zoo'=>'Флора и фауна',
                'cinema'=>'Кино и мультики',
                'literature'=>'Литература',
                'hobby'=>'Хобби',
                'tech'=>'Технологии',


            );
	
	public $methods = array(
				
		
		'ping'=>array(
			'group'=>'common',
			'name'=>'Пинг апи',
			'parameters'=>array(
				
				'method'=>'ping',
			),
			'description'=>'Проверяет доступность API и успешность авторизации пользователя.',
			'return'=>array(
				'response'=>'Ответный "pong"',
				'time'=>'Текущее время сервера',
			)
		),
                
                'getuserbyname'=>array(
                        'group'=>'user',
                        'name'=>'Получить пользователя по имени',
                        'parameters'=>array(
                            'method'=>'getuserbyname',
                            'name'=>'Имя пользователя',
                        ),
                        'description'=>'Получает информацию о пользователе по имени',
                        'return'=>array(
                                'user'=>'Объект с информацией о пользователе',
                        )
                ),
                'getuserbyguid'=>array(
                        'group'=>'user',
                        'name'=>'Получить пользователя по GUID',
                        'parameters'=>array(
                            'method'=>'getuserbyguid',
                            'guid'=>'GUID Пользователя',
                        ),
                        'description'=>'Получает информацию о пользователе по Guid',
                        'return'=>array(
                                'user'=>'Объект с информацией о пользователе',
                        )
                ),
                'settoken'=>array(
                        'group'=>'user',
                        'name'=>'Получить пользователя по GUID',
                        'parameters'=>array(
                            'method'=>'settoken',
                            'guid'=>'GUID Пользователя',
                            'apnstoken'=>'Token'
                        ),
                        'description'=>'Получает информацию о пользователе по Guid',
                        'return'=>array(
                                'user'=>'Объект с информацией о пользователе',
                        )
                ),             
                'createuser'=>array(
                        'group'=>'user',
                        'name'=>'Создать пользователя',
                        'parameters'=>array(
                            'method'=>'createuser',
                            'name'=>'Имя пользователя',
                        ),
                        'description'=>'Создает пользователя с именем name',
                        'return'=>array(
                                'user'=>'Объект с информацией о пользователе',
                        )
                ),
                'changename'=>array(
                        'group'=>'user',
                        'name'=>'Создать пользователя',
                        'parameters'=>array(
                            'method'=>'changename',
                            'name'=>'Имя пользователя',
                        ),
                        'description'=>'Создает пользователя с именем name',
                        'return'=>array(
                                'user'=>'Объект с информацией о пользователе',
                        )
                ),
                'searchquestions'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'searchquestions',
                            'text'=>'Текст для поиска',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                                'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),
                'sendanswers'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'searchquestions',
                            'guid'=>'Текст для поиска',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                                'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),
                'initgame'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'initgame',
                            'challenger'=>'ID of challenger',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                                'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),
                'startgame'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'startgame',
                            'gameid'=>'ID of game',
                            'bets'=>'Bets amount',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                                'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),
                'getchallengers'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'searchquestions',
                            'guid'=>'Текст для поиска',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                             'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),
                'getpreviousgames'=>array(
                        'group'=>'question',
                        'name'=>'Поиск по вопросам',
                        'parameters'=>array(
                            'method'=>'searchquestions',
                            'guid'=>'Текст для поиска',
                        ),
                        'description'=>'Производит поиск по вхождению подстроки text в текст вопроса',
                        'return'=>array(
                             'result'=>'Список вопросов, соответствующих критерию',
                        )
                ),            
                'loadquestion'=>array(
                        'group'=>'question',
                        'name'=>'Загрузить следующий случайный вопрос',
                        'parameters'=>array(
                            'method'=>'loadquestion',
                        ),
                        'description'=>'Выбирает случайный вопрос, выводит его вместе с вариантами ответов',
                        'return'=>array(
                                'question'=>'Объект с информацией о вопросе',
                        )
                ),            
                'answerquestion'=>array(
                        'group'=>'question',
                        'name'=>'Ответить на вопрос',
                        'parameters'=>array(
                            'method'=>'answerquestion',
                            'guid'=>'Guid вопроса',
                            'userguid'=>'Guid пользователя',
                            'ans'=>'Ответ на вопрос',
                        ),
                        'description'=>'Фиксирует ответ на вопрос',
                        'return'=>array(
                            'result'=>'Результат операции',
                            'balance'=>'Текущий счет пользователя'
                        )
                ),
        );

	public function apiGetuserbyname($data) {
            
            $name = isset($data['name'])?$data['name']:'';
            
            $user=BS::DAO('user')->getByFields(array('login'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false);
            }
            
        
            return array('result'=>'ok','user'=>$user);
        }

	public function apiGetuserbyguid($data) {
            
            $name = isset($data['guid'])?$data['guid']:'';
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false,'guid'=>$name);
            }
            
            if (isset($data['apnstoken'])) {
                BS::DAO('user')->updateItem($user['id'],array(
                    'apnstoken'=>$data['apnstoken']
                ));
            }
            
            return array('result'=>'ok','user'=>$user);
        }      
        
        public function apiSettoken($data) {
            
            $name = isset($data['guid'])?$data['guid']:'';
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false,'guid'=>$name);
            }
            
            if (isset($data['apnstoken'])) {
                $token = $data['apnstoken'];
                $token = str_replace('Optional(\"', '',$token);
                $token = str_replace('\")','',$token);    
                BS::DAO('user')->updateItem($user['id'],array(
                    'apnstoken'=>$token
                ));
            }
            
        
            
            return array('result'=>'ok','token'=>$data['apnstoken']);
        }      

        public function apiSendanswers($data) {
            
            $name = isset($data['guid'])?$data['guid']:'';
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false,'guid'=>$name);
            }
            
            
            $game=BS::DAO('game')->getByFields(array('guid'=>$data['gameid']),null,true);
        
            if (!$game) {
                return array('result'=>'fail','game'=>false,'guid'=>$data['gameid']);
            }
            
            BS::DAO('user')->updateItem($user['id'],
                    array('balance'=>$data['balance'])
            );
            
            BS::DAO('game')->updateItem($game['id'],
                    array('status'=>6,'result_balance'=>$data['result'])
            );
            
            
            return array('result'=>'ok');
        }      
        
        
	public function apiGetchallengers($data) {
            
            $name = isset($data['guid'])?$data['guid']:'';
            
            if (!$name) return array('result'=>false);
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false,'guid'=>$name);
            }
            
            $challengers = BS::DAO('user')->getChallengers($user['id']);
            
            
            return array('result'=>'ok','user'=>$challengers);
        }
        
        public function apiGetpreviousgames($data) {
            
            $name = isset($data['guid'])?$data['guid']:'';
            
            if (!$name) return array('result'=>false);
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false,'guid'=>$name);
            }
            
            $games = BS::DAO('game')->getPreviousGames($user['id']);
            
            foreach ($games as $i=>$game) {
                $games[$i]['desc'] = 'Результат: '.($game['result_balance']>0?'+'.$game['result_balance']:$game['result_balance']);
            }
            
            return array('result'=>'ok','games'=>$games);
        }
        
        public function apiSearchquestions($data) 
        {
            $quests = BS::DAO('question')->search($data['text']);
            $stats = BS::DAO('question')->getStats();
            return array('result'=>'ok','questions'=>$quests, 'stats'=>$stats, 'count'=>count($quests));
        }
        
        public function apiInitgame($data) 
        {
            //$quests = BS::DAO('question')->search($data['text']);
            $stats = BS::DAO('question')->getStats();
            $name = isset($data['guid'])?$data['guid']:'';
            
            if (!$name) return array('result'=>'fail');
            $user=BS::DAO('user')->getByFields(array('guid'=>$name),null,true);
            if (!$user) return array('result'=>'fail');
            
            $chg = isset($data['challenger'])?$data['challenger']:'';
            if (!$chg) return array('result'=>'fail');
            $chg=BS::DAO('user')->getById($chg);
            if (!$chg) return array('result'=>'fail');

            $gameguid = md5($chg['id'].'-'.$user['id'].'-'.rand(0,10000000).microtime(true));
            
            $categories = array();
            unset($stats['none']);
            
            
            for ($x=0;$x<6;$x++) {
                $category = array_rand($stats);
                unset($stats[$category]);
                $categories[]=$category;
            }
            
            
            $gameid = BS::DAO('game')->insertItem(
                    array(
                        'guid'=>$gameguid,
                        'author'=>$user['id'],
                        'challenger'=>$chg['id'],
                        'status'=>'1',
                        'started'=>time(),
                        'lastupdate'=>time(),
                        'categories'=>  json_encode($categories)
                    )
            );
            
            $game = BS::DAO('game')->getById($gameid);
            
            $game['categories'] = json_decode($game['categories']);
            $categories = array();
            foreach($game['categories'] as $item) {
                $categories[] = $this->cats[$item];
            }
            
            $game['categories'] = $categories;
            return array('result'=>'ok','game'=>$game);
        }
        
        public function apiStartgame($data) {
            
            $gameguid = $data['gameid'];
            
            $game = BS::DAO('game')->getByFields(array('guid'=>$gameguid),null,true);
            
            $categories = array();
            
            $result = array();
            $questions = array();
            
            $game['categories'] = json_decode($game['categories'],true);
            
            foreach($game['categories'] as $item) {
                $question = BS::DAO('question')->getRandomCat($item);
                
                $question['answers'] = array(
                    $question['a1'],
                    $question['a2'],
                    $question['a3'],
                    $question['a4'],
                );

                shuffle($question['answers']);
                $question['correct'] = 0;
                foreach ($question['answers'] as $k=>$v) {
                    if ($question['a1']==$v) $question['correct'] = $k+1;
                }

                unset($question['a1'],$question['a2'],$question['a3'],$question['a4']);
                
                $questions[] = array(
                    'text'=>$question['text'],
                    'guid'=>$question['guid'],
                    'correct'=>(string)$question['correct'],
                    'answers'=>$question['answers'],
                    'category'=> $this->cats[$item],
                    
                );
            }
            
            $result = array('questions'=>$questions);
                    
            
            return array('result'=>'ok','game'=>$result);
        }
        public function apiAnswerquestion($data) {
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$data['userguid']),null,true);
            $question=BS::DAO('question')->getByFields(array('guid'=>$data['guid']),null,true);
            if ($question) {
                $correct = $data['ans']==$question['a1'];
                BS::DAO('question')->markAnswer($question['id'],$correct);
                BS::DAO('useranswer')->markAnswer($user['id'],$question['id'],$question['cat'],$correct);
            }
            return array('result'=>'ok','balance'=>$user['balance']);
        }
        
        public function apiLoadquestion($data) {
            $question = BS::DAO('question')->getRandom();
            
            $question['bet']= '+0';
            $question['answers'] = array(
                $question['a1'],
                $question['a2'],
                $question['a3'],
                $question['a4'],
            );
            
            shuffle($question['answers']);
            $question['correct'] = 0;
            foreach ($question['answers'] as $k=>$v) {
                if ($question['a1']==$v) $question['correct'] = $k+1;
            }
            
            unset($question['a1'],$question['a2'],$question['a3'],$question['a4']);
            
            $cats = array(
                'science'=>'Наука и техника',
                'medic'=>'Медицина',
                'religion'=>'Религия',
                'none'=>'Без категории',
                'music'=>'Музыка',
                'math'=>'Математика',
                'physics'=>'Физика',
                'history'=>'История',
                'comps'=>'Компьютеры',
                'russia'=>'Россия',
                'politics'=>'Политика',
                'tvshow'=>'Телевидение',
                'geography'=>'География',
                'persons'=>'Личности',
                'sports'=>'Спорт',
                'zoo'=>'Флора и фауна',
                'cinema'=>'Кино и мультики',
                'literature'=>'Литература',
                'hobby'=>'Хобби',
                'tech'=>'Технологии',


            );
            
            $question['category'] = $cats[$question['category']];
            
            
            return array('result'=>'ok','question'=>$question);
        }
        
        public function apiMarkquests($data) 
        {
            BS::DAO('question')->markquests($data['quest'],$data['cat']);
            return array('result'=>'ok');
        }
  
	public function apiCreateuser($data) {
            
            $name = isset($data['name'])?$data['name']:'';
            
            $guid = isset($data['guid'])?$data['guid']:md5(rand(1,10000000).microtime(true));
            
            $geo = file_get_contents('http://api.sypexgeo.net/json/'.$_SERVER['REMOTE_ADDR']);
            $geo = json_decode($geo,true);
            
            $user = array(
                'login'=>$name,
                'guid'=>$guid,
                'apikey'=>md5(rand(1,1000000).  microtime(true)),
                'translation'=>'russian',
                'role'=>2,
                'balance'=>100,
                'lat'=>$geo['city']['lat'],
                'lon'=>$geo['city']['lon'],
                'level'=>0,
                'textlevel'=>'Student',
                
            );
            
            BS::DAO('user')->insertItem($user);
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$guid),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false);
            }
            
            return array('result'=>'ok','user'=>$user);
        }     
        public function apiChangename($data) {
            
            $name = isset($data['name'])?$data['name']:'';
            $guid = isset($data['guid'])?$data['guid']:'';
            
            $user = BS::DAO('user')->getByFields(array('guid'=>$guid),null,true);
            
            BS::DAO('user')->updateItem($user['id'],array(
                'login'=>$name
            ));
            
            $user=BS::DAO('user')->getByFields(array('guid'=>$guid),null,true);
            
            if (!$user) {
                return array('result'=>'fail','user'=>false);
            }
            
            return array('result'=>'ok','user'=>$user);
        }     
        
	public function apiPing($data) {
            
            $geo = file_get_contents('http://api.sypexgeo.net/json/'.$_SERVER['REMOTE_ADDR']);
            $geo = json_decode($geo,true);
            
            return array('result'=>'ok','response'=>'pong','time'=>time(),'geo'=>array('lat'=>$geo['city']['lat'],'lon'=>$geo['city']['lon']));
	}
        
 
}

/**
 * app_api
 * Приложение - коллекция, обработка ap-запросов
 * 
 * @package mushspider
 * @author Charlie <charlie@chrl.ru> 
 * @access public
 */
 
class app_api
{
    var $version='App_Api v.0.1';
    var $depends=array('antigate','pq');
    public function init()
    {
        
    }
    
    public function run()
    {
	BS::Visual()->template['name']='ajax.tpl';
         BS::Visual()->text = '';
        
        $this->worker = new apiworker();

	$post = file_get_contents('php://input');
        $post = json_decode($post,true);
        
        if ($post) {
            if (isset($post['OSType'])){
                $_GET['method'] = $post['method'];
            }
            
        } else {
            $_GET['method']= isset(BS::getUrl()[1]) ? BS::getUrl()[1]: false;
            $post = array(
                'vars'=>array(
                    'method'=>$_GET['method'],
                )
            );
            $post['vars']+=$_GET;
        }
		
        //$_GET['method']= isset(BS::getUrl()[1]) ? BS::getUrl()[1]: false;
        
        if (isset($_GET['method']) && $_GET['method'])
        {
            $method = 'api'.ucfirst($_GET['method']);
            if (method_exists($this->worker,$method))
            {
		
		
		foreach($this->worker->methods[strtolower($_GET['method'])]['parameters'] as $param=>$desc) {
			if (!isset($post['vars'][$param])) {
				BS::Visual()->text = json_encode(array('result'=>'fail','error'=>'Method "'.$_GET['method'].'" demands parameter "'.$param.'" to be set, but this param doesn\'t found in request'));
				return true;
			}
		}
		
                  $result = $this->worker->$method($post['vars']);
		unset($_GET['apikey']);
		
		BS::Log(array(
			'action'=>'api_call',
			'params'=>$post['vars'],
		));	
				
		BS::Visual()->text = isset($_GET['encode'])&&($_GET['encode']=='false')?$result:json_encode($result);   
            }
            else BS::Visual()->text = json_encode(array('result'=>'fail','error'=>'Method '.$_GET['method'].' not found'));
			
        } else {
            
	    BS::Visual()->text = json_encode(array('result'=>'fail','error'=>'No method found','POST'=>$post));
	}
    }
}
?>