<?php
class app_apidoc
{
	public $version='App_404 v.0.1';
	public $depends=array();

	public function init()
	{

	}
	public function run()
	{

		$apimenu = '';

		require_once('apps/app.api.php');
		$apiWorker = new apiworker();
		foreach ($apiWorker->groups as $key => $group) {
			$apimenu.='<h4>'.$group.'</h4>
                        <ul class="nav nav-list primary">';

								foreach ($apiWorker->methods as $kp => $method) {
									if ($method['group']==$key) {
										$apimenu.='<li'.(isset($_GET['method']) && ($_GET['method']==$kp) ? ' style="background-color:#0088cc !important"':'').'><a '.(isset($_GET['method']) && ($_GET['method']==$kp) ? ' style="color:white;':'').' href="/apidoc/?method='.$kp.'" data-toggle="tooltip" rel="tooltip" data-placement="right" data-container="body" title="'.$method['name'].'">'.$kp.'</a></li>';
									}
								}
								$apimenu.='</ul>';
		}

		BS::Visual()->apileftmenu = $apimenu;

		if (!isset($_GET['method'])) {
			BS::Visual()->page_name = 'Документация API';
			BS::Visual()->template['name']='apidoc_overview.tpl';
		} else {

			$method = $_GET['method'];

			if (!isset($apiWorker->methods[$method])) {
				header('Location: /apidoc/');
				die;
			}

			$method = $apiWorker->methods[$method];
			   BS::Visual()->page_name = $method['name'];

			BS::Visual()->methodname = $_GET['method'];
			BS::Visual()->methodrus = $method['name'];
			BS::Visual()->methoddesc = $method['description'];

		$text='<br /><h4>Параметры метода</h4>';

		foreach ($method['parameters'] as $p => $d) {
			$text.='<b>'.$p.'</b> &mdash; '.$d.'<br />';
		}

		$text.='<br /><h4>Переменные ответа</h4>';

		foreach ($method['return'] as $p => $d) {
			$text.='<b>'.$p.'</b> &mdash; '.$d.'<br />';
		}

		$example = 'http://v.25hr.ru/api/'.$_GET['method'].'/';
		$jj=0;
		foreach ($method['parameters'] as $p => $d) {
			if (($p != 'apikey')&&($p!='method')) {
							$jj++;
				$example.=($jj>1?'&':'?').$p.'=xxx';
			}
		}

				$example = '<a href="'.$example.'" target="_blank">'.$example.'</a>';

		$text.='<br /><h4>Пример запроса</h4><code>'.$example.'</code>';

		BS::Visual()->methodexpl = $text;

			BS::Visual()->template['name']='apidoc.tpl';
		}

	}
}
