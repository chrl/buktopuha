<?php

/**
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/


/**
 * app_verify
 * Приложение подтверждения ссылки пользователя
 *
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @access public
 */

class app_verify
{
	public $version='App_Verify v.0.1';
	public $depends=array();

	public function init()
	{

	}
	public function run()
	{
		BS::Visual()->template['name']='verify.tpl';
		BS::Visual()->title = BS::getConfig('site.name');
		BS::Visual()->page_name = 'Подтверждение регистрации';

		$url = BS::getUrl();

		if (isset($url[1])) {
		$res = BS::DAO('settings')->getByFields(array('value'=>$url[1],'entity'=>'link'), array('id','user'), true);
			if ($res) {
			BS::DAO('user')->updateItem($res['user'], array('role'=>3));
				BS::DAO('settings')->deleteItem($res['id']);

				$u = BS::DAO('user')->getById($res['id']);
				$_SESSION['user'] = array('id'=>$u['id'],'mail'=>$u['mail'],'role'=>$u['role']);

				BS::Visual()->text = '<!--{T:verify_success}--><script>setTimeout(function(){top.location="/";},5000);</script>';
			} else {
BS::Visual()->text = 'Link not found, sorry';
	  }
		} else {
BS::Visual()->text = 'Link not found, sorry';
	 }

	}
}
