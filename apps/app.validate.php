<?php
class app_validate
{
        public $version='App_Enter v.0.1';
        public $depends=array('pq','namelookup');

        protected $headers = array(
                    'fio'=>'Ф.И.О.',
                    'phone'=>'Телефон',
                    'address'=>'Адрес',
                    'birthdate'=>'Дата рождения',
                    'email'=>'E-Mail',
            );

        public $delimeter = ';';

        public function init()
        {

        }
	
        function detect_encoding($string) {  
            static $list = array('utf-8', 'windows-1251');

            foreach ($list as $item) {
              $sample = @iconv($item, $item, $string);
              if (md5($sample) == md5($string))
                return $item;
            }
            return null;
        }
    
	public function supposeItemfio($item) {
		
		$i = BS::Namelookup()->stem($item);
		
		if (count($i)>0) {
			foreach ($i as $word) {
					
					
					
					list($origin,$stream) = explode('{',$word);
					$stream = substr($stream, 0,-1);
					
					if (false!==strpos($stream,'=S,имя,')) return true;
					if (false!==strpos($stream,'=S,фам,')) return true;					
			}
		}
		
		return false;
	}
	
	public function supposeItemPhone($item) {
		return preg_match('/^((8|0|((\+|00)\d{1,2}))[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/',$item);
	}
	
	public function supposeItemEmail($item) {
            
                $item = preg_replace('/\s/', '', $item);
		return preg_match('/.+@.+/i',$item);
	}
        
        public function supposeItemAddress($item) {
		
		if (mb_strlen($item,'UTF-8')<3) return false;
		
		$res=file_get_contents("http://geocode-maps.yandex.ru/1.x/?geocode=".urlencode($item)."&format=json");
		
		if (!$res) var_dump($item);
		$res = json_decode($res,true);
		
		$num = $res['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'];
		
		if (($num >0)&&($num<3)) return true;
		
		return false;
	}
	
	public function supposeString($string) {
		$res = array();	
		
		$string = explode($this->delimeter,$string);
		foreach ($string as $key=>$item) {
			$item = trim($item);
			$item = trim($item,'"');
			
			$res[$key] = array();
			
			foreach(array('fio','phone','address','email') as $method) {
				if (method_exists($this, 'supposeItem'.$method)) {
					
					if ($method == 'address') {
						
						if (!in_array('fio',$res[$key]) && !in_array('phone',$res[$key])) {
							if ($this->{'supposeItem'.$method}($item)) {
								$res[$key][] = $method; 
							}							
						}
					} else {
							if ($this->{'supposeItem'.$method}($item)) {
								$res[$key][] = $method; 
							}							
					}
					
					
				}
			}

		}
		
		return $res;
	}
    
	public function formatString($string) {

		$res = array();	
		
		$string = explode($this->delimeter,$string);
		foreach ($string as $item) {
			$item = trim($item);

			$i = BS::Namelookup()->stem($item);
			
			if (count($i)==0) {
				$res[]=array(
					'origin'=>$item,
					'type'=>'unknown',
				);
			} else {
				foreach ($i as $word) {
					
					
					
					list($origin,$stream) = explode('{',$word);
					$stream = substr($stream, 0,-1);
					
					$stream = explode('|',$stream);
					
					
					$types = array();
	
					foreach ($stream as $part) {
						
						if (false!==strpos($part, 'гео')) {
							if (!isset($types['geographic'])) $types['geographic']=0;
							$types['geographic']++;
						}
						if (false!==strpos($part, 'фам')) {
							if (!isset($types['lastname'])) $types['lastname']=0;
							$types['lastname']++;
						}
						if (false!==strpos($part, 'имя')) {
							if (!isset($types['name'])) $types['name']=0;
							$types['name']++;
						}						
					}
					
					$bs = array_keys($types);
					
					$res[] = array(
						'origin'=>$origin,
						'type'=> count($types)>1 ? $bs:(count($types)==0?'unknown':$bs[0]),
					);
				}
				
				
			}

		}
		
		return $res;
		
	}
	
	public function sortByUsage($a,$b)
	{
		  if ($a['usage'] == $b['usage']) {
				return 0;
		  }
		  return ($a['usage'] > $b['usage']) ? -1 : 1;
	}
	
        public function findDelimiter($handle) {
                $delimiters = array('tab'       => "\t",
                                    'comma'     => ",",
                                    'semicolon' => ";"
                                );
                $line = array();            # Stores the count of delimiters in each row
                $valid_delimiter = array(); # Stores Valid Delimiters
                
                for ( $i = 1; $i < (count($handle)>6?6:count($handle)); $i++ ){
                    foreach ($delimiters as $key => $value ){
                        $line[$key][$i] = count( explode( $value, $handle[$i] ) ) - 1;
                    }
                }

                foreach ( $line as $delimiter => $count ){
                    if ( $count[1] > 0 and $count[2] > 0 ){
                        $match = true;
                        $prev_value = '';
                        foreach ( $count as $value ){
                            if ( $prev_value != '' )
                                $match = ( $prev_value == $value and $match == true ) ? true : false;
                            $prev_value = $value;
                        }
                    } else { 
                        $match = false;
                    }
                    if ( $match == true )    $valid_delimiter[] = $delimiter;
                }//foreach
                $delimiter = ( $valid_delimiter[0] != '' ) ? $valid_delimiter[0] : "comma";
                
                return $delimiters[$delimiter];             
        }
        
	public function run() {
		
		BS::Visual()->template['name'] = 'validate.tpl';
                BS::Visual()->title = 'Datarich.ru - Validate';
		
		if (isset($_FILES['cfile']) && ($_FILES['cfile']['error']==0)) {
		
			$x = file($_FILES['cfile']['tmp_name']);
			
			BS::Visual()->text = 'В файле обнаружено <b>'.count($x).'</b> строк: <br /><br />';
			
			BS::Visual()->text.= '<table class="table">';
                        

			
                        //detect encoding here
                        
                        $enc =$this->detect_encoding($x[0]);
                        if ($enc!='utf-8') {
                            foreach ($x as $k=>$item) {
                                $x[$k] = iconv($enc,'UTF-8',$x[$k]);
                            }
                        }
                        
                        $this->delimeter = $this->findDelimiter($x);

			$rund = array();
			
			for ($xp = 0;$xp<5;$xp++) {
				$string = $x[rand(0,count($x)-1)];
				$objects = $this->supposeString($string);
				if (!isset($rund[md5(json_encode($objects))])) {

					$rund[md5(json_encode($objects))] = array(
						'objects' => $objects,
						'usage'=>1,
					);				
				} else {
					$rund[md5(json_encode($objects))]['usage']++;
				}
			}
			
			usort($rund, array($this,"sortByUsage"));
			
			$usage = $rund[0];
			
			// now save file in data and in session
			
			$name = md5(microtime(true).rand(1,100000000).'salt1');
			
			$_SESSION['dbase'] = $name;
			move_uploaded_file($_FILES['cfile']['tmp_name'], CACHEDIR.'/file.'.$name.'.save');
			file_put_contents(CACHEDIR.'/file.'.$name.'.usage',  json_encode($usage));		
                        
			BS::Visual()->text.='<tr>';
			
			foreach ($usage['objects'] as $id=>$item) {
                            BS::Visual()->text.='<th><div class="btn-group formatselector">
                                                    <button type="button" data-alias="'.(count($item)==0?'leave':$item[0]).'" class="selectbutton btn btn-default">'.(count($item)==0?'Оставить как есть':$this->headers[$item[0]]).'</button>
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                      <span class="caret"></span>
                                                      <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">';
                                                    foreach($this->headers as $him=>$im) {
                                                        BS::Visual()->text.='<li><a href="#" data-alias="'.$him.'">'.$im.'</a></li>';
                                                    } 
                                                    BS::Visual()->text.='
                                                      <li class="divider"></li>
                                                      <li><a href="#" data-alias="leave">Оставить как есть</a></li>
                                                      <li><a href="#" data-alias="remove">Исключить из таблицы</a></li>
                                                    </ul>
                                                  </div></th>';
                            
                            
                            
                            //BS::Visual()->text.='<th>'.(count($item)==0?'Неизвестные данные':$this->headers[$item[0]]).'</th>';
			}
			
			BS::Visual()->text.='</tr>';
			
			for ($xt=0;$xt< (count($x)>10?10:count($x));$xt++) {
				
				if ($xt==0) {
					$rp = $this->supposeString($x[$xt]);
					$cont = true;
					foreach ($rp as $rpt) {
						if (isset($rpt[0]) && (($rpt[0]=='fio') || ($rpt[0]=='phone'))) {
							$cont = false;
						}
					} 
					
					if ($cont)	continue;
				}
				
				
				BS::Visual()->text.='<tr>';
				
				$line=explode($this->delimeter,$x[$xt]);
				foreach ($line as $lp) {
					$lp = trim($lp);
					$lp = trim($lp,'"');
					
					BS::Visual()->text.='<td>'.$lp.'</td>';
				}
				
				BS::Visual()->text.='</tr>';
				
			}
			
			BS::Visual()->text.= '</table>';
			
			
		} else {
			header('Location: /import/');
			die;
		}
		
    }
}