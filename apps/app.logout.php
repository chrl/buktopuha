<?php

/**
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/


/**
 * app_logout
 * ����� ������������ � �����
 *
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @access public
 */

class app_logout
{
	public $version='App_Logout v.0.1';
	public $depends=array();

	public function init()
	{

	}
	public function run()
	{
		unset($_SESSION['user']);
		session_write_close();
		header('Location: /');
		die();
	}
}
