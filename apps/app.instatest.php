<?php
class app_instatest
{
	public $version='App_Enter v.0.1';
	public $depends=array('pq');

	public $cli = true;


	public function run()
	{

		echo "Getting answers...\n";
		$answers = BS::DAO('answer')->getByFields(array('alive'=>''));
		$cnt = count($answers);
		echo 'Got answers: '.$cnt."\n";

		$time = time();
		$done = 0;
		$lastdone = 0;

		foreach ($answers as $id => $item) {
			//$item['text'] = 'Радиус ядра';
			$output = array();
			exec('echo \''.$item['text'].'\' | ./mystem -i -n -g --format=json', $output);
			$str = json_decode($output[0], true);
			$str = ($str['analysis'][0]['gr']);

			list($type,$gr) = explode('=', $str);
			$gr = trim($gr, '()');

			$gr = explode('|', $gr);

			//var_dump($gr);

			$alive = 'U';

			if ($type == 'A') {

				if (strpos($gr[0], 'муж')!==false) {
					$rod = 'M';
				} elseif (strpos($gr[0], 'жен')!==false) {
					$rod = 'F';
				} elseif (strpos($gr[0], 'сред')!==false) {
					$rod = 'N';
				} else {
					$rod = 'U';
					echo $item['text'].', '.$str." -> \n";
					var_dump($gr);
					//die;
				}
			} else {

				//echo $item['text']." -> ".$str."\n";

				$type = explode(',', $type);
				$rod = 'U';
				if ($type[0]=='S') {
				   $rod = $type[1];
				   if ($rod=='сред') {
$rod = 'N';
	      } elseif ($rod=='муж') {
$rod = 'M';
	      } elseif ($rod=='жен') {
$rod = 'F';
	      } else {
$rod = 'U';
	      }

				   $alive = $type[2] == 'од'?'Y':'N';
				}
				$type = $type[0];
			}

			$update = array(
				'text'=>  mb_strtolower($item['text'], 'UTF-8'),
				'type'=>$type,
				'rod'=>$rod,
				'alive'=>$alive,
			);

			BS::DAO('answer')->updateItem($item['id'], $update);
			$done++;

			if ($time+10<time()) {
				$time = time();

				echo 'Done '.$done.' of '.$cnt.' ('.(round(1000*$done/$cnt)/10)."%), ".(round(10*($done-$lastdone)/10)/10)." w/sec\n";

				$lastdone = $done;
			}
			//die;
		}

	}
}
