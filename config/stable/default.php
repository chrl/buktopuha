<?php

    return array(

        'notify'=>array(
            'mail'=>array(
                'charlie@chrl.ru'
            ),
            'sms'=>array(
                '79672977000',
            )
        ),
        'datasource'=>array(
            'host'=>'localhost',
            'db'=>'cdb2',
            'user'=>'root',
            'pass'=>'12917',
        ),
    
        'sms'=> array(
            'id'=>'1048',
            'key'=>'B98DE284E5AB2220',
            'sender'=>'Take2.ru',
        ),    
        'constants'=>array(
            'BULLSHARKDIR' => '../lib/bullshark',
            'APPDIR'=>'apps',
            'BLOCKDIR'=>'blocks',
            'CACHEDIR'=>'data',
            'DICTDIR'=>'translations',
            'TEMPLATEDIR'=>'templates',
            'CACHETIME'=>10,
            'SQLCACHETIME'=>10,
            'DEBUG'=>false,
            'SID'=>'rsid',
        ),
        'site'=>array(
            'name'=>'Client Base',
            'domain'=>'api.02p.ru',
            'defaultroute'=>'enter',
            'encoding'=>'UTF-8',
            'defaulttranslation'=>'russian',
        ),	
        'preload'=>array('log','visual','cache','database','dao','mqueue','mailer','access','antihack','acl','sms'),
        'postexecute'=>array('visual','database'),
    );