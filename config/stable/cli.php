<?php

    return array(
        'pid'=> '/tmp/rte.pid',
        'init'=>array(
            'daemons'=>5,
        ),
        'roles'=> array(
            'Logger'=>'FileLogger',
            'Pcntl'=>'SimplePcntl',
            'Redis'=>'RedisExtAdapter',
            'Db'=>'MysqlDatasource',
        ),
        'log'=>array(
            'default'=>'../../logs/mushspider.rte.log',
        ),
        'datasource'=>array(
            'host'=>'localhost',
            'db'=>'take2',
            'user'=>'take2',
            'pass'=>'take2',
        ),
        'sms'=> array(
            'id'=>'1048',
            'key'=>'15E78135FC7274FC',
            'sender'=>'MushSpider',
        ),
        'notify'=>array(
            'mail'=>array(
                'charlie@chrl.ru'
            ),
            'sms'=>array(
                '79672977000',
            )
        ),
        'constants'=>array(
            'BULLSHARKDIR' => '../bullshark',
            'APPDIR'=>'apps',
            'BLOCKDIR'=>'blocks',
            'CACHEDIR'=>'data',
            'DICTDIR'=>'translations',
            'TEMPLATEDIR'=>'templates',
            'CACHETIME'=>10,
            'SQLCACHETIME'=>10,
            'DEBUG'=>false,
            'SID'=>'rsid',
        ),
        'site'=>array(
            'name'=>'MushSpider',
            'domain'=>'mushspider.ru',
            'defaultroute'=>'help',
            'encoding'=>'UTF-8',
            'defaulttranslation'=>'russian',
            'antigate'=>'815d89f0c0498ad1fb31f557b75e8b87',
            'cli'=>true,
        ),
        'preload'=>array('cache','database','dao','mqueue','mailer'),
        'postexecute'=>array('database')
    );