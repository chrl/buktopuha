<?php

    return array(

        'notify'=>array(
            'mail'=>array(
                'charlie@chrl.ru'
            ),
            'sms'=>array(
                '79672977000',
            )
        ),
        'datasource'=>array(
            'host'=>'localhost',
            'db'=>'cdb2',
            'user'=>'root',
            'pass'=>'12917',
        ),
    
        'sms'=> array(
            'id'=>'1048',
            'key'=>'B98DE284E5AB2220',
            'sender'=>'Take2.ru',
        ),    
        'menuparams'=>array(
            'main'=>array(
                'name'=>'Главная',
                'uri'=>'/^$/',
                'bread'=>array(),
            ),
            'enter'=>array(
                'name'=>'Авторизация на сервисе',
                'bread'=>array('main'),
                'uri'=>'/^enter$/',
            ),
            'apidoc'=>array(
                'name'=>'Документация API',
                'bread'=>array('main'),
                'uri'=>'/^apidoc/',
            ),            
            'usecase'=>array(
                'name'=>'Примеры использования',
                'bread'=>array('main'),
                'uri'=>'/^usecase$/'
            ),
            'prices'=>array(
                            'name'=>'Тарифные планы',
                            'bread'=>array('main'),
                            'uri'=>'/^prices$/'
            )            
        ),
        'menutree'=>array(
            'main'=>array(
                'uri'=>'/',
                'template'=>'none',
            ),
            'usecase'=>array(
                'uri'=>'/usecase/',
                'template'=>'none',
            ),
            'prices'=>array(
                'uri'=>'/prices/',
                'template'=>'none',
            ),
			'apidoc'=>array(
                'uri'=>'/apidoc/',
                'template'=>'apidoc_menu',
            )
            
        ),
        
        'constants'=>array(
            'BULLSHARKDIR' => '../lib/bullshark',
            'APPDIR'=>'apps',
            'BLOCKDIR'=>'blocks',
            'CACHEDIR'=>'data',
            'DICTDIR'=>'translations',
            'TEMPLATEDIR'=>'templates',
            'CACHETIME'=>10,
            'SQLCACHETIME'=>10,
            'DEBUG'=>false,
            'SID'=>'rsid',
        ),
        'site'=>array(
            'name'=>'Client Base',
            'domain'=>'v.25hr.ru',
            'defaultroute'=>'dashboard',
            'encoding'=>'UTF-8',
            'defaulttranslation'=>'russian',
        ),	
        'preload'=>array('log','visual','cache','database','dao','mqueue','mailer','access','antihack','acl','sms'),
        'postexecute'=>array('visual','database'),
    );