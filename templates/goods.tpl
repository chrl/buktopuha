<!--{P:header}-->
<!--{P:menu}-->
<style>
	.mGroup {
        line-height: 30px;
	}
    
    .gTitle {
        padding-left:20px;
    }
    
    .gTitle div {
         float:right;
         padding-right:20px;
         display:none;
    }
    
    .gTitle div img {
        margin-top: 10px;
        cursor:pointer;
    }
    
    .gTitle:hover div {

         display:block;
    }
    
    
    .gTitle:hover {
        background-color:#F5F5F5;
    } 
    
    .highlighter {
        border-left: 4px solid #DD4B39;
        left: 0;
        position: absolute;
    }
    
    .highlighter2 {
        border-left: 4px solid #DD4B39;
        left: 0;
        position: absolute;
    }
    
    .mItem {
        padding-left:30px;
    }
    
	
	.desc {
		background-color:#CCCCCC;
		padding:20px;
		border:1px dotted #222;
	}    
</style>
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
		<td width="220" valign="top">
			<div class="mGroup" id="test">
				<span class="highlighter">&nbsp;</span>
				<div class="gTitle">
                    <a href="#">По категории</a>
                    <div>
                        <img src="/plu.png" />
                    </div>
                
                </div>
                <!--{L:tovcat}-->
			</div>
			<div class="mGroup" id="test_add" style="margin-top:30px;">
				<span class="highlighter" style="border-left: 4px solid #0099FF;">&nbsp;</span>
				<div class="gTitle">
                    <a href="#">Регион:</a>
                </div>
                        <!--{L:tovreg}-->
                                
			</div>                                     
		</td>
		<td valign="top" style="padding:6px 10px 10px;" id="container"></td>
    </tr>
</table>
<script>

    function loadCat(catid) {
        $('div.mItem a[catid='+catid+']').click();
    }
    
	$(document).ready(function(){
                
                window.regionId = -1;
            
		$('div.gTitle a').click(function(){
			$(this).parent().parent().find('div.mItem').toggle();
			return false;
		});
		$('div.mItem a').click(function(){
                    
                        if ($(this).hasClass('catitem')) {
                        
                            catid = $(this).attr('catid');

                            window.currentCat = catid;
                            
                            $('.highlighter').remove();

                            $(this).parent().prepend('<span class="highlighter" style="border-left: 4px solid #0099FF;">&nbsp;</span>');
                            $(this).parent().parent().prepend('<span class="highlighter">&nbsp;</span>');

                            $.post('/ajax/',{method:'getCategoryDescription',subject: catid,region:window.regionId},function(data){
                                    data = $.parseJSON(data);
                                    if (data.result=='ok') {
                                            $('#container').html(data.text);
                                             drawQuantChart(data.q_cats,data.q_data);
                                    }

                            });
                            
                            

                            return false;
                    } else {
                        
                        catid = $(this).attr('regid');
                        $('.highlighter2').remove();
                        $(this).parent().prepend('<span class="highlighter2" style="border-left: 4px solid #00FF99;">&nbsp;</span>');
                        window.regionId = catid;    
                        
                        loadCat(window.currentCat);
                        
                        return false;
                        
                    }
		});
        
		$('a.goodItem').live('click',function(){
			catid = $(this).attr('goodid');
			
			$.post('/ajax/',{method:'getItemDescription',subject: catid,region:window.regionId},function(data){
				data = $.parseJSON(data);
				if (data.result=='ok') {
					$('#container').html(data.text);
                    drawQuantChart(data.q_cats,data.q_data);
				}
				
			});
			
			return false;
		});	        		
		
	});
</script>

<script src="/highcharts.js"></script>
<script>
   var chart;
   
   function drawQuantChart(cats,q_data) {
    
    window.wcount = 0;
   
    chart2 = new Highcharts.Chart({
      chart: {
         renderTo: 'datequant_container',
         defaultSeriesType: 'line',
         margin: [ 50, 50, 100, 80]
      },
      title: {
         text: ''
      },
      xAxis: {
         categories: cats,
         labels: {
            rotation: -90,
            align: 'right',
            style: {
                font: 'normal 12px Verdana, sans-serif'
            }
         }
      },
      yAxis: {
         title: {
            text: 'Частота'
         }
      },
      legend: {
         enabled: false
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.x +'</b><br/>'+
                this.y;
         }
      },
      series: [{
         name: 'Population',
         data: q_data,
         dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#BBBBBB',
            align: 'left',
            x: 2,
            y: -10,
            formatter: function() {
               window.wcount++;
               if (window.wcount>24) {
                return '<b style="color:red;">'+this.y+'</b>';      
               }
               return this.y;
               
            },
            style: {
               font: 'normal 10px Verdana, sans-serif'
            }
         }
      }]
   });   
   
}
</script>


<!--{P:bottom}-->