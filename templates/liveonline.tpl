<div class="container">
					<div class="row">
						<div class="footer-ribon">
							<span>Оставайтесь на связи!</span>
						</div>
						<div class="col-md-6">
							<h4>Последние твиты</h4>
							<div id="tweet" class="twitter" data-account-id="datarich_ru">
								<p>Подождите...</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="contact-details">
								<h4>Связь с нами</h4>
								<ul class="contact">
									<li><p><i class="icon icon-map-marker"></i> <strong>Адрес:</strong> г. Москва, ул. Дубининская, 11/17</p></li>
									<li><p><i class="icon icon-phone"></i> <strong>Телефон:</strong> +7 (967) 29-77-000</p></li>
									<li><p><i class="icon icon-envelope"></i> <strong>E-Mail:</strong> <a href="mailto:info@datarich.ru">info@datarich.ru</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<h4>Соцсети</h4>
							<div class="social-icons">
								<ul class="social-icons">
									<li class="facebook"><a href="http://www.facebook.com/25hour" target="_blank" data-placement="bottom" rel="tooltip" title="Facebook">Facebook</a></li>
									<li class="twitter"><a href="http://www.twitter.com/datarich_ru" target="_blank" data-placement="bottom" rel="tooltip" title="Twitter">Twitter</a></li>
									<li class="vk"><a href="http://www.vk.com/25hourday" target="_blank" data-placement="bottom" rel="tooltip" title="Vkontakte">Vkontakte</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>