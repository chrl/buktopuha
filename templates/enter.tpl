<!--{P:header}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" width="50%" style="border-right:1px solid #E5E5E5;background-color:#F1F1F1;">
            <img src="/mushspider2.png"/>
            <br />
            <br />
            <table style="height:200px;width:80%;"><tr>
            <td style="width:80%;text-align:left;" id="aboutPage">
                <center>MushSpider: spidering your mushrooms.</center><br /><br />
            </td>
            </tr>
            </table>
            <br />
            <a style="margin-right: 30px;" href="#" onclick="showInner('about');return false;"><!--{T:about}--></a> <a style="margin-right: 30px;" href="#" onclick="showInner('privacy');return false;"><!--{T:terms_of_use}--></a> <a href="#" onclick="showInner('contacts');return false;"><!--{T:contacts}--></a>
        </td>
        <td align="center">
            <table id="loginform">
                <tr><td align="center" style="color: rgb(0, 153, 255); font-size: 22px;"><br /><br />
                    <table cellpadding="5" style="width:100px;">
                        <tr>
                            <td></td>
                            <td><div style="margin-bottom:-13px;font-size:10px;color:gray;"><!--{T:login_or_mail}--></div></td>
                        </tr>                    
                        <tr>
                            <td><img src="/public/user.jpg" /></td>
                            <td><input type="text" id="login_mail" style="color: rgb(0, 153, 255); font-size: 22px; width: 300px;" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div style="margin-bottom:-13px;font-size:10px;color:gray;"><!--{T:password}--></div></td>
                        </tr>                        
                        <tr>
                            <td><img src="/public/pass.jpg" /></td>
                            <td><input id="login_pass" type="password" style="color: rgb(0, 153, 255); font-size: 22px; width: 300px;" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a id="login_button" class="handsomeButton" href="#" onclick="catcher('loginform');return false;"><!--{T:_enter}--></a>&nbsp;&nbsp;&nbsp;<!--{T:_or}-->&nbsp;&nbsp;&nbsp;<a href="/register/"><!--{T:_register_msg}--></a></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center"></td>
                        </tr>
                    </table>
                </td></tr>
                <tr><td>
                    <div id="msg">
                    </div>
                </td></tr>
            </table>
        </td>
    </tr>
</table>
<script>
$(document).ready(function(){
    $('#login_pass').keypress(function(event){
        if (event.which == '13') {
            $('#login_button').click();
        }
    });
});
    

</script>
<!--{P:footer}-->