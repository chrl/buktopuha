<!--{P:2header}-->
<div role="main" class="main">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Импорт базы контактов</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Импорт контактов</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <section class="page-not-found">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
			<!--{L:text}-->
                        </div>
                        <a href="/generate/" class="btn btn-lg btn-primary">Столбцы расставлены верно</a>
                    </div>
                </div>
            </section>
        </div>
</div>
<script>

function saveUsage()
{
    usage = new Array();
    $("button.selectbutton").each(function(k){
        usage.push($(this).attr('data-alias'));
    });
    $.post('/ajax/',{method:'makeUsage',usage:usage},function(data){
        console.log(data);
    });
}

$(document).ready(function(){
    $('div.formatselector ul a').click(function(){
        $(this).parent().parent().parent().find('button.selectbutton').html($(this).html()).attr('data-alias',$(this).data('alias'));
        $(this).parent().parent().dropdown('toggle');

        saveUsage();

        return false;
    });
});
</script>

<!--{P:2footer}-->
