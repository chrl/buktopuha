<!--{P:2header}-->
<div role="main" class="main">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Импорт базы контактов</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Результаты обработки</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <section class="page-not-found">
                <div class="row">
                    <div class="col-md-12">
			<div class="panel-body buttonshere"></div>
			<!--{L:text}-->
			<a href="/generatetext/" class="btn btn-lg btn-primary">Создать базу для рассылки</a> 
			<a href="/import/" class="btn btn-lg btn-success"><i class="glyphicon glyphicon-repeat"></i> Еще один файл</a>
                    </div>
                </div>
            </section>
        </div>
</div>
<!--{P:2footer}-->
<script type="text/javascript" src="/js/datatables.js"></script>	
