<html>
    <head>
        <title>Отчет по тестированию сайта "<!--{L:sitename}-->"</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
        body
        {
            font-family:Arial;
            margin: 30px;
            color: #444;
            font-size: 14px;
        }
        h1
        {
            color: #0099FF;
            font-size:18px;
            font-weight: normal;
        }
        h2
        {
            color: #0099FF;
            font-size:16px;
            font-weight: normal;
        }

        </style>
    </head>
    <body>
    <h1>Отчет по тестированию сайта &laquo;<!--{L:sitename}-->&raquo;</h1>
    <ul>
        <li>Тест создан: <!--{L:testdate}--></li>
        <li>Тест-план: <!--{L:testplan}--></li>
    </ul>
    <!--{L:text}-->  
    </body>
</html>