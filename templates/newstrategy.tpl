<!--{P:header}-->
<!--{P:menu}-->
<div id="actionBox" style="display:none;position:absolute;left:100px;top:300px;width:200px;background-color:white;-moz-box-shadow: 0 0 10px 0;border:1px solid gray;">
    <div class="coverable" style="text-align:center;background-color:#DDD;color:black;">
    Заготовки
    </div>
    <div id="action-templates">
        <!--{L:actionList}-->
    </div>
    <div class="coverable" style="text-align:center;background-color:#DDD;color:black;">
    На странице
    </div>
    <div id="on-page">
    </div>
    <div class="coverable light" id="closeButton" style="text-align:right;padding-top:20px;">Закрыть</div>
</div>
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <table style="margin-top:20px;width:800px;" cellspacing="10" id="newstrategyform">
            <tr><td>Название стратегии</td><td><input id="form_name" class="middlefield" type="text" /></td></tr>
            <tr><td colspan="2" style="border-top:1px gray dashed;border-bottom:1px gray dashed;padding:10px;">
                <table id="workflow" cellspacing="0" cellpadding="15" class="strategyTable">
                    <tr>
                        <td style="background-color:#FF9999;border-top:3px solid red;border:3px solid red;border-right:0px;border-radius:15px 0px 0px 15px;">
                            &nbsp;
                        </td>
                        <td wflid="0" actid="0" valign="top" style="background-color:#FF9999;border-top:3px solid red;border-bottom:3px solid red;">Старт</td>
                        <td style="border-top:3px solid red;border-left:3px solid red;">&rarr;</td>
                        <td style="border-top:3px solid red;">OK</td>
                        <td style="border-top:3px solid red;">&rarr;</td>
                        <td style="border-top:3px solid red;border-right:3px solid red;border-radius:0px 15px 0px 0px;"><a href="#" class="popup">Скачать страницу</a></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" rowspan="3" colspan="2">
                            <a href="#" style="border-bottom:1px dotted;text-decoration: none;">&uarr; Переименовать</a><br />
                            <a href="#" style="border-bottom:1px dotted;text-decoration: none;">[o] Параметры</a><br />
                            <a href="#" style="border-bottom:1px dotted;text-decoration: none;"><span style="color:red;">X</span> Удалить</a>
                        </td>
                        <td style="border-left:3px solid red;">&rarr;</td>
                        <td>FAIL</td>
                        <td>&rarr;</td>
                        <td style="border-right:3px solid red;"><a href="#" class="popup">Выбрать действие</a></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:3px solid red;border-left:3px solid red;border-radius:0px 0px 0px 15px;">&rarr;</td>
                        <td style="border-bottom:3px solid red;">BUSY</td>
                        <td style="border-bottom:3px solid red;">&rarr;</td>
                        <td style="border-bottom:3px solid red;border-right:3px solid red;border-radius:0px 0px 15px 0px;"><a href="#" class="popup">Выбрать действие</a></td>
                    </tr>
                </table>
            </td></tr>
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:20px;padding-bottom:20px;">Стратегия сбора данных &mdash; способ, с помощью которого будет осуществляться сбор данных для источника данных.</td>
            </tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="font-size:16px;color:red;padding:20px;border:1px solid red;display:none;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" id="testform_submit" onclick="catcher('newstrategyform');return false;">Сохранить &rarr;</a></td>
            </tr>
        </table>

        </td>
    </tr>
</table>
<script>
window.lastWFid = 0;
function defineActionPopups()
{
    $('a.popup').unbind('click').click(function(event){
        window.callee = $(this);
        $('#actionBox').animate({left: event.clientX, top: event.clientY-100},100).fadeIn('fast');
        return false;
    });

    $('#closeButton').unbind('click').click(function(){
        $('#actionBox').fadeOut('fast');
    });

    $('.light:not(#closeButton)').unbind('click').click(function(){
        if($(this).attr('type')=='new-block') {

            exitPoint = window.callee.parent().parent().find('td:eq(2)').text();
            
            startAction = window.callee.parent().parent().find('td:eq(0)').attr('wflid');
            console.log(window.callee.parent().parent().find('td:eq(0)'));
            alert(startAction);

            /*
                while(startAction == '→') {
                    startAction = window.callee.parent().parent().parent().find('td:eq(0)').attr('wflid');
                }
            */


            exitPoint = exitPoint == '→'? window.callee.parent().parent().find('td:eq(1)').text():exitPoint;

            getNewActionBlock($(this).attr('action_id'),exitPoint,startAction);
        }
        window.callee.html($(this).html());
        $('#actionBox').fadeOut('fast');
    });
}

defineActionPopups();

</script>
<!--{P:bottom}-->