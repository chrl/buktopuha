<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
<tr>
    <td class="leftMenu" align="right" valign="top" style="width:219px;">
        <!--{D:leftmenu}-->
    </td>
    <td style="padding-left: 20px;" valign="top">
        <!--{L:text}-->
    </td>
</tr>
</table>
<script>
function upTasks()
{
    $.post('/ajax/',{method: 'upTasks',project: <!--{L:project_id}-->},function(data){
        result = $.parseJSON(data);
        
        for (k in result) {
            $('span#status_'+k).html(result[k]);
        }
        
        setTimeout(upTasks,1000);
         
    });
}

$(document).ready(function(){
   $('.leftMenu ul li[linked]').click(function(){
        top.location = '/projects/<!--{L:project_id}-->/'+$(this).attr('linked')+'/';
   });
   
   $('.hourclock').click(function(){
	$(this).toggleClass('active');
	$.post('/ajax/',{method:'setRepeatable','task':$(this).attr('task_id')});
   });
   
   setTimeout(upTasks,1000);
    
});


</script>
<!--{P:bottom}-->