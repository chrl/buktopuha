<!--{P:4header}-->
<div role="main" class="main">
        <!--{P:breadcrumbs}-->
        <div class="container">
            <section class="page-not-found">
                <div class="row">
                    <div class="col-md-3">
                        <!--{L:apileftmenu}-->
                    </div>
                    <div class="col-md-9">
                        <h1>Метод &laquo;<!--{L:methodname}-->&raquo;</h1>
                        <h3><!--{L:methodrus}--></h3>
                        <div class="well">
                                <!--{L:methoddesc}-->
                        </div>
                        <!--{L:methodexpl}-->
                    </div>
                </div>
            </section>
        </div>
</div>
<!--{P:4footer}-->