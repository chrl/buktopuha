<!--{P:header}-->
<!--{P:menu}-->
<table id="centerTable" width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" valign="middle" id="workfield">
            <img src="/ajax-loader.gif" />    
        </td>
    </tr>
</table>
<script>
function doAnalyze()
{
    $.post('/ajax/',{method: 'getSegmentClusters',segment: <!--{L:segment_id}-->},function(data){
        $('#workfield').fadeOut('slow',function(){
            $('#workfield').css('text-align','left');
            $('#workfield').html('');
            
            result = $.parseJSON(data);
            if(result['result']=='ok')
            {
                txt = '';
                i=1;
                for(k in result['groups']) {
                    txt+='<h1>Группа №'+ i++ +'</h1><ul>';
                    
                    for(j in result['groups'][k]) {
                        txt+='<li>'+result['groups'][k][j]+'</li>';   
                    }
                    txt+='</ul><br />';
                }
                
                $('#workfield').html(txt).fadeIn('fast');
                
            } else {
                $('#workfield').html('В ходе анализа возникла ошибка. Ошибки записаны в лог, обратитесь к администратору.').fadeIn('fast');
            }            
             
        });
    });
}
doAnalyze();
</script>
<!--{P:bottom}-->