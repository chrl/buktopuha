<!--{P:header}-->
<div class="row">
        <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-heading">Составление текста СМС
            </div>
            <div class="panel-body">
              <label class="form-label">Предполагаемый текст СМС:</label>
              <div class="well" id="tareacontrol" contenteditable="true">Привет, <span class="badge" contenteditable="false">Имя</span>!</div>
              <hr>
              <label class="form-label">Что получится в итоге:</label>
              <div class="well resultText">
                 Привет!
              </div>
            </div>
          </div>
		  <a class="btn btn-success getbase btn-lg"><span class="glyphicon glyphicon-download"></span> Все ок, скачать базу</a>	
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">Доступные элементы
            </div>
            <div class="text-center panel-body">
              <a class="btn btn-primary reloadperson"><span class="glyphicon glyphicon-refresh"></span> Подставить другого клиента</a>
            </div>
			<div class="list-group" id="availableElems">
			  <!--{L:subjectdata}-->
            </div>
            <div class="panel-footer text-center">
              <a class="btn btn-primary reloadperson"><span class="glyphicon glyphicon-refresh"></span> Подставить другого клиента</a>
            </div>
          </div>
        </div>
</div>
        <script>
        $(document).ready(function(){
            assignClicks();
            $('#tareacontrol').keyup(updateText).click(updateText).change(updateText);
            updateText();
			
			$('a.reloadperson').click(function(){
			     $.post('/ajax/',{method:'getuser'},function(data){
					  $('div#availableElems').html(data);
					  assignClicks();
					  updateText();
					  
				 });
			});
        });
        
		function assignClicks()
		{
			$('a.delement').unbind('click').click(function(){
                var text = $('#tareacontrol').html();
                var insert = ' <span class="badge" contenteditable="false">'+$(this).find('.badge').text()+'</span> ';
                insert = text + insert;
                $('#tareacontrol').html(insert).focus();
                updateText();
                return false;
            });

		}
		
        function getCaretCharacterOffsetWithin(element) {
            var caretOffset = 0;
            if (typeof window.getSelection != "undefined") {
                var range = window.getSelection().getRangeAt(0);
                var preCaretRange = range.cloneRange();
                preCaretRange.selectNodeContents(element);
                preCaretRange.setEnd(range.endContainer, range.endOffset);
                caretOffset = preCaretRange.toString().length;
            } else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
                var textRange = document.selection.createRange();
                var preCaretTextRange = document.body.createTextRange();
                preCaretTextRange.moveToElementText(element);
                preCaretTextRange.setEndPoint("EndToEnd", textRange);
                caretOffset = preCaretTextRange.text.length;
            }
            return caretOffset;
        }        
        
        function updateText()
        {
            var replacements = new Array();
            var text = $('#tareacontrol').html();
            
            $('div#availableElems a').each(function(){
                text = text.replace(new RegExp('<span class="badge" contenteditable="false">'+$(this).find('.badge').text()+'</span>',"g"),$(this).find('.ditem').text());
            });
            
            $('.resultText').html(text);
			
			$('#tareacontrol .badge').click('dropdown');
        }
        
        function saveUsage()
        {
            usage = new Array();
            $("button.selectbutton").each(function(k){
                usage.push($(this).attr('data-alias'));
            });
            $.post('/ajax/',{method:'makeUsage',usage:usage},function(data){
               
				
            });
        }
            
        </script>
<!--{P:footer}-->