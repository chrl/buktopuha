<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 900px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
        <table style="margin-top:20px;" cellspacing="10" id="newconctaskform">
            <input type="hidden" id="form_project" value="<!--{L:project_id}-->" />
            <tr><td>Поле для анализа</td><td><input name="form_anal_type" id="form_anal_type_1" value="all" type="radio" checked /> Все сегменты</td></tr>
            <tr><td></td><td><input name="form_anal_type" id="form_anal_type_2" value="one" type="radio" /> Сегмент:<br /><select id="form_one_segment" class="middlefield" name="form_one_segment" style="margin-top:15px;"><!--{L:segments_list}--></select></td></tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr><td>Регион анализа</td><td><input name="form_anal_region" id="form_anal_region_1" value="all" type="radio" checked /> Москва и область</td></tr>
            <tr><td></td><td><input name="form_anal_region" id="form_anal_region_2" value="one" type="radio" /> Один регион: <span id="region_id"></span><br /><input type="hidden" id="form_one_region_id" value="1" /><input id="form_one_region" class="middlefield" name="form_one_region" style="margin-top:15px;" /><br /><span id="regionList"><span style="color:gray;">Начните вводить название региона</span></span></td></tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr><td>Поисковая система</td><td><input name="form_anal_engine_ya" id="form_anal_engine_ya" type="checkbox" checked /> Яндекс</td></tr>
            <tr><td></td><td><input name="form_anal_engine_go" id="form_anal_engine_go" type="checkbox" disabled /> Google</td></tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr><td>По завершении анализа</td><td><input id="form_anal_notify_mail" type="checkbox" /> Отправить отчет на почту <!--{L:send_mail}--></td></tr>
            <tr><td></td><td><input id="form_anal_notify_sms" type="checkbox" /> Уведомить по SMS <!--{L:send_phone}--></td></tr>
            <tr><td colspan="2"><br /></td></tr>            
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:20px;padding-bottom:20px;">
                    После создания задания оно запустится автоматически. Прогресс выполнения будет виден на экране.<br /><br />
                    Внимательно отнеситесь к настройкам задания: изменить их после будет невозможно. Учтите, что нотификации будут приходить только при корректно указанных номере телефона и E-Mail. На всякий случай, проверяйте папку &laquo;Спам&raquo; Вашего почтового ящика &mdash; вдруг наше письмо по ошибке попало туда.
                </td>
            </tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="margin-bottom:20px;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" class="handsomeButton" id="testform_submit" onclick="catcher('newconctaskform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>
        </div>
        </td>
    </tr>
</table>
<script>
$(document).ready(function(){
   $('#form_one_segment').change(function(){
        $('input#form_anal_type_2').click();
   });
   $('#form_one_region').keypress(function(){
        clearTimeout(window.tmo);
        window.tmo = setTimeout(function(){
            $.post('/ajax/',{method: 'getRegionList',part:$('#form_one_region').val()},function(data){
                result = $.parseJSON(data);
                if(result.text) {
                    $('#regionList').html(result.text);
                    $('a.region').unbind('click').click(function(){
                        $('#form_one_region_id').val($(this).attr('region_id'));
                        $('span#region_id').html('ID='+$(this).attr('region_id'));
                        $('#form_one_region').val($(this).text());
                        $('input#form_anal_region_2').click();
                        return false; 
                    });
                }
            });
        },300);
   });

});

</script>
<!--{P:bottom}-->