<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 900px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
        <table style="margin-top:0px;" cellspacing="10" id="settingsform">
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:0px;padding-bottom:20px;">
                    <!--{L:invites}-->
                </td>
            </tr>
            <tr><td colspan="2" style="padding-top:20px;"><h1><!--{T:passwords}--></h1></td></tr>
            <tr><td><!--{T:old_pass}--></td><td><input id="form_oldpass" type="password" class="middlefield" /></td></tr>
            <tr><td><!--{T:new_pass}--></td><td><input id="form_newpass" type="password" class="middlefield" /></td></tr>
            <tr><td><!--{T:_register_form_pass2}--></td><td><input id="form_newpass2" type="password" class="middlefield" /></td></tr>
            <tr><td colspan="2"><br /></td></tr>            
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:20px;padding-bottom:20px;">
                    <!--{T:auth_reason}-->
                </td>
            </tr>
	   <tr><td colspan="2" style="padding-top:20px;"><h1>API</h1></td></tr>
	   <tr><td colspan="2">Ваш ключ API: <b><!--{L:apikey}--></b>. Пожалуйста, прочитайте <a href="/apihelp/">документацию по API</a> перед его использованием.</td></tr>
            <tr><td colspan="2" style="padding-top:20px;"><h1><!--{T:site_settings}--></h1></td></tr>
            <tr><td><!--{T:site_language}--></td><td><select class="middlefield" id="translation"><!--{L:translations}--></select></td></tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="margin-bottom:20px;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" class="handsomeButton" id="testform_submit" onclick="catcher('settingsform');return false;"><!--{T:save}--></a></td>
            </tr>
        </table>
        </div>
        </td>
    </tr>
</table>
<!--{P:bottom}-->