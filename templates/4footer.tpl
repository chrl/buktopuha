			<footer id="footer" style="padding:0px;">
				<div class="footer-copyright" style="margin-top:0px;">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
                                                            <p>Работает на платформе &laquo;<a href="http://25hr.ru/">Двадцать пятый час</a>&raquo;</p>
							</div>
							<div class="col-md-4">
								<nav id="sub-menu">
							
								</nav>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Libs -->
		<script src="/js/plugins.js"></script>
		<script src="/vendor/jquery.easing.js"></script>
		<script src="/vendor/jquery.appear.js"></script>
		<script src="/vendor/jquery.cookie.js"></script>
		
		<script src="/vendor/bootstrap.js"></script>
		<script src="/vendor/twitterjs/twitter.js"></script>
		<script src="/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
		<script src="/vendor/rs-plugin/js/jquery.themepunch.revolution.js"></script>
		<script src="/vendor/owl-carousel/owl.carousel.js"></script>
		<script src="/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="/vendor/jquery.validate.js"></script>

		<!-- Current Page Scripts -->
		<script src="/js/views/view.home.js"></script>

		<!-- Theme Initializer -->
		<script src="/js/theme.js"></script>
                <script src="/js/jquery.json.js"></script>
                <script src="/js/bootbox.min.js"></script>
                <script src="/js/catcher.js"></script>

		<!-- Custom JS -->
		<script src="/js/custom.js"></script>
                <script type="text/javascript" src="/js/bootstrap.file-input.js"></script>
                <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>	
                <script type="text/javascript" src="/js/dataTables.fixedColumns.min.js"></script>
                <script type="text/javascript" src="/js/dataTables.colReorder.min.js"></script>
                <script type="text/javascript" src="/js/dataTables.tableTools.min.js"></script>
	

	</body>
</html>
