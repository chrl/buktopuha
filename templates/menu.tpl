<script>
    function reportBug()
    {
        $.post('/ajax/',{method: 'reportBug',link: location.href, text: $('#bugText').val()},function(data){
            $('#bugText').val('');
            $('#bugAddBox').fadeOut('fast');
            $('#foundBug').html('<!--{T:goodguy}-->!');
            setTimeout(function(){$('#foundBug').html('<!--{T:bug_found}-->!');},2000);
        });
        
        
    }
        
    
</script>


<div id="uinfo" style="position:absolute;width:300px;height:200px;right:0px;top:0px;border:1px solid #E5E5E5;border-radius:0 0 0 5px;padding:0px;box-shadow:0 2px 4px rgba(0, 0, 0, 0.2);display:none;background: url('/mushspider_watermark.png') no-repeat scroll right center #F8F8F8;">
<div style="text-align:right;padding-right:19px;padding-top:22px;">
        <!--{D:userinfo:login}-->
</div>
<div style="padding:0px 20px;">
<!--{D:userinfo}-->
</div>
</div>
<div id="bugAddBox" style="display:none;position:absolute;width:600px;height:420px;left:50%;top:50px;margin-left:-300px;border:1px solid #E5E5E5;background-color:white;box-shadow: 0px 0px 15px 0px black;padding:10px;">
    
    <!--{T:buglove_text}-->
    
    <textarea id="bugText" style="width:100%;height:50%"></textarea>
    <div style="float:right;margin-top:13px;" align="right">
        <a class="handsomeButton" href="#" onclick="reportBug();return false;"><!--{T:report_bug}--></a>
        <a class="handsomeButton" href="#" onclick="$('#bugAddBox').fadeOut('fast');return false;"><!--{T:changed_mind}--></a>
    </div>
</div>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
<tr bgcolor="#F1F1F1">
    <td style="height:70px;width:300px;">
            <img src="/mushspider_tiny.png" style="cursor:pointer;" border="0" onclick="top.location='/';"/>
    </td>
    <td style="width:20px;">
        <span class="menu">&rarr;</span>
    </td>    
    <td>
        <!--{D:usermenu:current}-->
    </td>
    <td align="right" style="padding-right:20px;"> 
        <a id="foundBug" class="menu" href="#" onclick="$('#bugAddBox').fadeIn('fast');$('#bugText').val('').focus();return false;"><!--{T:bug_found}-->!</a> <span class="divider">&nbsp;</span> <span id="uinfo_login"><!--{D:userinfo:login}--></span>
    </td>
</tr>
<tr>
<td colspan="4" style="height:40px;padding:20px;border-top:1px solid #E5D5D5;" valign="top">
    <!--{D:usermenu:extend}-->
</td>
</tr>
<tr>
<td colspan="4" style="padding:20px;border-top:1px solid #E5D5D5;" valign="top">