<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 800px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
            В Вашем проекте &laquo;<!--{L:project_name}-->&raquo; пока еще нет ни одного сегмента.<br /><br />
            Сегменты, на которые делится проект, соответствуют сегментам рынка. Сегменты рынка определяются его целевой аудиторией, семантическим полем сегмента, и мотивацией ЦА. Если Вы пока еще не можете сегментировать рынок правильно &mdash; создайте один сегмент, позже Вы сможете с помощью кластеризации правильно разбить рынок на сегменты.
            <br /><br /><br />

            <a class="handsomeButton" href="/projects/<!--{L:project_id}-->/segments/create/">Создать первый сегмент &rarr;</a>

        </div>

        </td>
    </tr>
</table>