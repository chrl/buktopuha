<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
<tr>
    <td class="leftMenu" align="right" valign="top" style="width:219px;">
        <!--{D:leftmenu}-->
    </td>
    <td style="padding-left: 20px;" valign="top">
        <table id="centerTable" width="100%" height="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" id="workfield">
                    <img src="/ajax-loader.gif" />    
                </td>
            </tr>
        </table>
        <script>
        function doAnalyze()
        {
            $.post('/ajax/',{method: 'getSegmentClusters',project: <!--{L:project_id}-->},function(data){
                $('#workfield').fadeOut('slow',function(){
                    $('#workfield').css('text-align','left');
                    $('#workfield').html('');
                    
                    result = $.parseJSON(data);
                    if(result['result']=='ok')
                    {
                        txt = '';
                        i=1;
                        for(k in result['groups']) {
                            txt+='<h1><!--{T:group}--> №'+ i++ +' &mdash; '+result['names'][k]+'</h1><ul>';
                            
                            for(j in result['groups'][k]) {
                                txt+='<li>'+result['groups'][k][j]+'</li>';   
                            }
                            txt+='</ul><br />';
                        }
                        
                        $('#workfield').html(txt).fadeIn('fast');
                        
                    } else {
                        $('#workfield').html('В ходе анализа возникла ошибка. Ошибки записаны в лог, обратитесь к администратору.').fadeIn('fast');
                    }            
                     
                });
            });
        }
        doAnalyze();
        </script>
    </td>
</tr>
</table>
<script>
$(document).ready(function(){
   $('.leftMenu ul li[linked]').click(function(){
        top.location = '/projects/<!--{L:project_id}-->/'+$(this).attr('linked')+'/';
   }); 
});
</script>
<!--{P:bottom}-->