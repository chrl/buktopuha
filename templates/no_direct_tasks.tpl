<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 800px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
            Вы пока еще не создали ни одного задания на мониторинг контекстной рекламы для проекта &laquo;<!--{L:project_name}-->&raquo;.<br /><br />
            Создав задание, Вы сможете выбрать область мониторинга, регион, поисковую систему для мониторинга.
            <br /><br /><br />

            <a class="handsomeButton" href="/projects/<!--{L:project_id}-->/direct/create/">Создать задание на мониторинг &rarr;</a>

        </div>

        </td>
    </tr>
</table>