<!--{P:2header}-->
			<div role="main" class="main">
				<div id="content" class="content full">

					<div class="slider-container light">
						<div class="slider" id="revolutionSlider">
							<ul>
								<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >

									<img src="img/slides/slide-bg-light.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

									<div class="tp-caption sft stb visible-lg"
										 data-x="-50"
										 data-y="180"
										 data-speed="300"
										 data-start="1000"
										 data-easing="easeOutExpo"><img src="img/slides/slide-title-border-light.png" alt=""></div>

									<div class="tp-caption top-label lfl stl"
										 data-x="0"
										 data-y="180"
										 data-speed="300"
										 data-start="500"
										 data-easing="easeOutExpo">УСТАЛИ ВРУЧНУЮ ИСПРАВЛЯТЬ</div>

									<div class="tp-caption sft stb visible-lg"
										 data-x="375"
										 data-y="180"
										 data-speed="300"
										 data-start="1000"
										 data-easing="easeOutExpo"><img src="img/slides/slide-title-border-light.png" alt=""></div>

									<div class="tp-caption main-label sft stb"
										 data-x="-50"
										 data-y="210"
										 data-speed="300"
										 data-start="1500"
										 data-easing="easeOutExpo">КЛИЕНТСКУЮ</div>
									<div class="tp-caption main-label sft stb"
										 data-x="75"
										 data-y="280"
										 data-speed="300"
										 data-start="1500"
										 data-easing="easeOutExpo">БАЗУ?</div>
									<div class="tp-caption bottom-label sft stb"
										 data-x="30"
										 data-y="380"
										 data-speed="500"
										 data-start="2000"
										 data-easing="easeOutExpo">Наш сервис &mdash; ваше спасение</div>

									<div class="tp-caption sfb"
										 data-x="460"
										 data-y="70"
										 data-speed="500"
										 data-start="2500"
										 data-easing="easeOutBack"><img src="img/slides/slide-light-1.jpg" alt=""></div>

								</li>
							</ul>
						</div>
					</div>

					<div class="home-intro light">
						<div class="container">

							<div class="row">
								<div class="col-md-8">
									<p>
										Самый быстрый способ стандартизировать и обогатить клиентскую базу.
										<span>Проверьте, как это можно использовать для вашего бизнеса.</span>
									</p>
								</div>
								<div class="col-md-4">
									<div class="get-started">
										<a href="/try/" class="btn btn-lg btn-primary">Попробуйте сейчас</a>
										<div class="learn-more">или <a href="/usecase/">узнайте ещё.</a></div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="container">
						<div class="row center">
							<div class="col-md-12">
								<h2 class="short word-rotator-title">
									ДатаРич &mdash; универсальное средство для грамотного ведения клиентской базы</h2>
								<p class="featured lead">
									Мы можем привести вашу базу в порядок буквально за несколько кликов. Вы можете как загружать файлы в произвольном формате, так и пользоваться нашим API &mdash; набором программных интерфейсов для интеграции с используемыми в вашей	компании CRM.  	
								</p>
							</div>
						</div>
					</div>
										<div class="home-concept">
						<div class="container">

							<div class="row center">
								<span class="sun"></span>
								<span class="cloud"></span>
								<div class="col-md-2 col-md-offset-1">
									<div class="process-image" data-appear-animation="bounceIn">
										<img src="img/home-concept-item-1.png" alt="" />
										<strong>Формат</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
										<img src="img/home-concept-item-2.png" alt="" />
										<strong>Очистка</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
										<img src="img/home-concept-item-3.png" alt="" />
										<strong>Обогащение</strong>
									</div>
								</div>
								<div class="col-md-4 col-md-offset-1">
									<div class="project-image">
										<div id="fcSlideshow" class="fc-slideshow">
											<ul class="fc-slides">
												<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/projects/project-home-1.jpg" /></a></li>
												<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/projects/project-home-2.jpg" /></a></li>
												<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/projects/project-home-3.jpg" /></a></li>
											</ul>
										</div>
										<strong class="our-work">Генерация базы</strong>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="container">
						<div class="row push-top">
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="icon icon-pencil-square-o"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Исправление опечаток</h4>
										<p class="tall">Вы больше не будете терять клиентов, которые обиделись на опечатку в имени или фамилии &mdash; мы исправим очевидные вещи автоматически, или предложим уточнить варианты исправления.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="icon icon-th-list"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Приведение к единому формату</h4>
										<p class="tall">Мы автоматически приведем все варианты написания телефона или имени к единому, по всей базе.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="icon icon-google-plus"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Обогащение данных</h4>
										<p class="tall">Засчет использования внешних источников данных, мы обогащаем вашу базу, добавляя такие поля, как регион, часовой пояс, или даже знак зодиака.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="image-icon small user"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Удаление дубликатов</h4>
										<p class="tall">Дедубликация &mdash; важнейший этап создания базы. Вы ведь не хотите, чтобы Пётр Семенович получил письмо дважды?</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="icon icon-desktop"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Синтез текста</h4>
										<p class="tall">Полученную &laquo;чистую&raquo; базу данных удобно использовать в различных рассылках &mdash; мы автоматически составим текст рассылки, подставим имена, фамилии и любые данные клиента.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box">
									<div class="feature-box-icon">
										<i class="icon icon-bars"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="shorter">Оценка качества</h4>
										<p class="tall">Мы оценим качество и полноту полученных данных &mdash; подходят ли они для почтовой или SMS-рассылки, предложим заполнить пробелы, уточнив у клиента.</p>
									</div>
								</div>
							</div>							
						</div>

					</div>

					<section class="featured">
						<div class="container">

							<div class="row">

								<div class="col-md-6">
									<h2 class="push-top">Премиальные <strong>услуги</strong></h2>
									<p class="lead">
										Вы можете бесплатно обработать до 1000 клиентов в сутки. Все что сверху &mdash; вам придется оплатить по достаточно <a href="/prices/">низким тарифам</a>.
									</p>
									<p class="tall">
										Помимо этого, за деньги мы можем помочь вам интегрировать наши интерфейсы именно в вашу систему управления клиентскими данными, даже если система самописная и неизвестная.
									</p>
								</div>
								<div class="col-md-6 push-top">
									<img class="img-responsive" src="img/promo.jpg" data-appear-animation="fadeInRight">
								</div>
							</div>
						</div>
					</section>

					<div class="container">

						<div class="row featured-boxes">
							<div class="col-md-3">
								<div class="featured-box featured-box-primary">
									<div class="box-content">
										<i class="icon-featured icon icon-user"></i>
										<h4>Индивидуальный подход</h4>
										<p>Мы разработаем интеграционное решение, которое подойдет именно вашему бизнесу.<a href="/usecase/" class="learn-more">Смотреть кейсы <i class="icon icon-angle-right"></i></a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="featured-box featured-box-secundary">
									<div class="box-content">
										<i class="icon-featured icon icon-book"></i>
										<h4>Отличная документация</h4>
										<p>Наши программные интерфейсы полностью документированы и покрыты тестами.<a href="/apidoc/" class="learn-more">Читать документацию <i class="icon icon-angle-right"></i></a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="featured-box featured-box-tertiary">
									<div class="box-content">
										<i class="icon-featured icon icon-trophy"></i>
										<h4>Высокая скорость</h4>
										<p>Мы можем обрабатывать сотни запросов в секунду.<a href="/try/" class="learn-more">Результаты тестов <i class="icon icon-angle-right"></i></a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="featured-box featured-box-quaternary">
									<div class="box-content">
										<i class="icon-featured icon icon-cogs"></i>
										<h4>Готовые решения</h4>
										<p>У нас есть готовые решения по работе с нашим API на нескольких популярных языках. <a href="/try/" class="learn-more">Смотреть список <i class="icon icon-angle-right"></i></a></p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
<!--{P:2footer}-->