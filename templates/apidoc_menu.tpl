<li class="dropdown mega-menu-item mega-menu-fullwidth<!--{L:apimenuactive}-->">
    <a class="dropdown-toggle" href="/apidoc/">
        Документация API
        <i class="icon icon-angle-down"></i>
    </a>
    <ul class="dropdown-menu">
        <li>
            <div class="mega-menu-content">
                <div class="row">
					<!--{L:apimenu}-->
                </div>
            </div>
        </li>
    </ul>
</li>