<script src="/highcharts.js"></script>
<script>

function bindItems()
{
    $('.pItem').unbind('click').click(function(){
        id = $(this).attr('id');
        window.val = $(this).text();
        par = $(this).parent();
        par.html('<input id="edit_'+id+'"/> <a href="#" onclick="removeCount(\''+id+'\');return false;">удалить</a>');
        $('input#edit_'+id).val(window.val).focus().keypress(function(event){
            if (event.which == '13') {
                val = $('input#edit_'+id).val();
                par.html('<li id="'+id+'" class="pItem">'+val+'</li>');
                par.parent().find('td:eq(1),td:eq(2)').html('<img src="/public/circle.gif" />');

                $.post('/ajax/',{method: 'editCount',product_id: id,val: val},function(data) {
                    updateCounts();
                });

                bindItems();
            }
        });
    });

    $('.showcountask').unbind('click').click(function(){

        item_id = $(this).parent().parent().parent().attr('id').replace('trcat_','');

        $.post('/ajax/',{method: 'getCatAsk',object: item_id},function(data){
            result = $.parseJSON(data);

            result_cats = [];
            result_data = [];
            j=0;
            max_data = 0;
            for (i in result['data']) {
                j++;
                result_cats.push(result['data'][i]['date']);
                result_data.push(result['data'][i]['ask']*1);
                if (result['data'][i]['ask']*1 > max_data) max_data = result['data'][i]['ask']*1;
            }

            chart2 = new Highcharts.Chart({
                  chart: {
                     renderTo: 'chart_container',
                     defaultSeriesType: 'spline',
                     margin: [ 50, 50, 100, 80],
                     width:780,
                     height:400,
                  },
                  title: {
                     text: ''
                  },
                  xAxis: {
                     categories: result_cats,
                     labels: {
                        rotation: -90,
                        align: 'right',
                        style: {
                            font: 'normal 10px Verdana, sans-serif'
                        }
                     }
                  },
                  yAxis: {
                     min: 0,
                     max: max_data,
                     title: {
                        text: 'Годовой спрос'
                     }
                  },
                  legend: {
                     enabled: false
                  },
                  tooltip: {
                     formatter: function() {
                        return '<b>'+ this.x +'</b><br/>'+ this.y;
                     }
                  },
                  series: [{
                     name: 'Population',
                     data: result_data,
                     dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FF9900',
                        align: 'left',
                        x: 3,
                        y: -10,
                        formatter: function() {
                           return this.y;
                        },
                        style: {
                           font: 'normal 9px Verdana, sans-serif'
                        }
                     }
                  }]
               });

        $('#chart_container_box').css('top',$(window).scrollTop()+300+'px').fadeIn('fast');


    });
    return false;
});


    $('.showitemask').unbind('click').click(function(){

        item_id = $(this).parent().parent().attr('id').replace('tr_pitem_','');

        $.post('/ajax/',{method: 'getPItemAsk',object: item_id},function(data){
            result = $.parseJSON(data);

            result_cats = [];
            result_data = [];
            j=0;
            max_data = 0;
            for (i in result['data']) {
                j++;
                result_cats.push(result['data'][i]['date']);
                result_data.push(result['data'][i]['ask']*1);
                if (result['data'][i]['ask']*1 > max_data) max_data = result['data'][i]['ask']*1;
            }

            chart2 = new Highcharts.Chart({
                  chart: {
                     renderTo: 'chart_container',
                     defaultSeriesType: 'spline',
                     margin: [ 50, 50, 100, 80],
                     width:780,
                     height:400,
                  },
                  title: {
                     text: ''
                  },
                  xAxis: {
                     categories: result_cats,
                     labels: {
                        rotation: -90,
                        align: 'right',
                        style: {
                            font: 'normal 8px Verdana, sans-serif'
                        }
                     }
                  },
                  yAxis: {
                     min: 0,
                     max: max_data,
                     title: {
                        text: 'Годовой спрос'
                     }
                  },
                  legend: {
                     enabled: false
                  },
                  tooltip: {
                     formatter: function() {
                        return '<b>'+ this.x +'</b><br/>'+ this.y;
                     }
                  },
                  series: [{
                     name: 'Population',
                     data: result_data,
                     dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FF9900',
                        align: 'left',
                        x: 3,
                        y: -10,
                        formatter: function() {
                           return this.y;
                        },
                        style: {
                           font: 'normal 10px Verdana, sans-serif'
                        }
                     }
                  }]
               });

        $('#chart_container_box').css('top',$(window).scrollTop()+300+'px').fadeIn('fast');
    
    });
    return false;
});

}

function updateCounts()
{
    $.post('/ajax/',{method: 'updateCounts',project_id: <!--{L:project_id}-->},function(data) {

        result = $.parseJSON(data);
        
        for(catNum in result.items) {
            $('#trcat_'+catNum+' td:eq(1)').html(result.items[catNum].sum);
            $('#trcat_'+catNum+' td:eq(2)').html(result.items[catNum].perc);
            
            for(itemNum in result.items[catNum].items) {
                $('#tr_pitem_'+itemNum+' td:eq(1)').html(result.items[catNum].items[itemNum].ask);
                $('#tr_pitem_'+itemNum+' td:eq(2)').html(result.items[catNum].items[itemNum].perc);
            }
            
        }
    });
}


function removeCount(id)
{
    pid = id.replace('product_','tr_pitem_');
    $('#'+$('#'+pid).attr('class').replace('tr__','trcat_')).find('td:eq(1),td:eq(2)').html('<img src="/public/circle.gif" />');
    $.post('/ajax/',{method: 'removeCount',product_id: id},function(data) {
        
        
        result = $.parseJSON(data);
        if(result.drop) {
            $('#tr_pitem_'+result.drop).hide().remove();
        }
        updateCounts();
    });
}



$(document).ready(function(){
    bindItems();
});
</script>