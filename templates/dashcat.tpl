<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/app-content/app.css">
        <style>
            .catselect {
                margin-bottom: 10px;
                margin-right: 10px;
            }
        </style>
    </head>
    <body>
        
        <div class="container-fluid" style="margin-top:20px;">
            <div class="row">
                <div class="col-sm-5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            Тут искать вопросы
                        </div>
                        <div class="panel-body">
                            <div class="input-group input-group-lg">
                                <input type="text" class="form-control" id="tphrase" placeholder="Фраза из вопроса...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" onclick="searchQuestions();">Go!</button>
                                </span>
                              </div>
                            <div id="cats" style="margin-top: 20px;padding-top:20px;border-top:1px solid #ccc;">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Результаты поиска: <span id="resultscount">0</span>, без категории: <span id="nocat">0</span>
                        </div>
                        <div class="panel-body resultstable" id="resultstable">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="/node_modules/jquery/dist/jquery.min.js"></script>   
        <script>
            $.fn.enterKey = function (fnc) {
                return this.each(function () {
                    $(this).keypress(function (ev) {
                        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                        if (keycode == '13') {
                            fnc.call(this, ev);
                        }
                    })
                })
            }
            
            $('#tphrase').enterKey(function(){
                searchQuestions();
            });
            
            function searchQuestions() {
                
                $('#resultstable').html('<center>Ищем...</center>');
                
                $.get('/api/searchquestions/?text='+$('#tphrase').val(),function(result){
                    result = $.parseJSON(result);
                    var text = '<table class="table">';
                    for (k in result.questions) {
                        quest = result.questions[k];
                        text += '<tr><td>'+quest.text.replace($('#tphrase').val(),'<b>'+$('#tphrase').val()+'</b>')+'</td><td>'+quest.a1+'</td></tr>';
                    }
                    text+='</table>';
                    
                    $('#resultstable').html(text);
                    $('#resultscount').html(result.count);
                    $('#nocat').html(result.stats['none']);
                    
                    for (k in result.stats) {
                        $('.catselect[data-cat='+k+']').html($('.catselect[data-cat='+k+']').attr('data-name')+' ('+result.stats[k]+')')
                    }
                    
                });
            }
            $(document).ready(function(){
                var cats = $.parseJSON('<!--{L:cats}-->');
                var text = '';
                for (k in cats) {
                    text+='<a href="#" data-cat="'+k+'" data-name="'+cats[k]+'" class="btn btn-primary catselect">'+cats[k]+'</a> ';
                }
                $('#cats').html(text);
                
                
                $('.catselect').click(function(){
                    
                    
                    
                    //mark all questions as this cat
                    $.get('/api/markquests/?cat='+$(this).data('cat')+'&quest='+$('#tphrase').val(),function(data){
                        searchQuestions();
                        
                    });
                    return false;
                });
            });
        </script>
    </body>
</html>