<!--{P:2header}--><div role="main" class="main">
     <!--{P:breadcrumbs}-->

				<div class="container">

					<h2><strong>Подойдет</strong> вашей компании!</h2>

					<div class="row">
                                            <div class="col-md-12">
                                                <p class="lead">
                                                    Посмотрите на примеры, именно так можно использовать datarich. Проблема низкой лояльности из-за ошибок в коммуникации актуальна как для малого, так и для крупного бизнеса. Кому из вас приходилось получать смс-уведомления о новых акциях в 3 часа ночи? Вы продолжили сотрудничество с такой компанией? Не считайте количество ушедших клиентов &mdash; просто обогащайте клиентскую базу, выбирая наиболее эффективный способ коммуникации.
                                                </p>
                                            </div>
					</div>

					<hr>

					<div class="row">
						<div class="col-md-7">

                                                    <section class="toggle">
                                                        <label>СМС-агрегаторам</label>
                                                        <div class="toggle-content">
                                                                <p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra. </p>
                                                                <p><a class="btn btn-primary push-bottom" href="/try/">Попробуйте</a></p>
                                                        </div>
                                                    </section>
                                                    <section class="toggle">
                                                        <label>Интернет-магазинам</label>
                                                        <div class="toggle-content">
                                                                <p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra. </p>
                                                                <p><a class="btn btn-primary push-bottom" href="/try/">Попробуйте</a></p>
                                                        </div>
                                                    </section>
                                                    <section class="toggle">
                                                        <label>СМC и EMail коммуникация</label>
                                                        <div class="toggle-content">
                                                                <p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra. </p>
                                                                <p><a class="btn btn-primary push-bottom" href="/try/">Попробуйте</a></p>
                                                        </div>
                                                    </section>
                                                    <section class="toggle">
                                                        <label>Системам бронирования</label>
                                                        <div class="toggle-content">
                                                                <p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra. </p>
                                                                <p><a class="btn btn-primary push-bottom" href="/try/">Попробуйте</a></p>
                                                        </div>
                                                    </section>

						</div>
						<div class="col-md-5">
							<div class="featured-box featured-box-secundary">
								<div class="box-content clearfix">
									<h4>Преимущества</h4>
									<hr />

									<ul class="list icons pull-left list-unstyled">
                                                                            <li><i class="icon icon-check"></i>Исправление опечаток.</li>
                                                                            <li><i class="icon icon-check"></i>Насыщение дополнительными данными.</li>
                                                                            <li><i class="icon icon-check"></i>Высокая скорость работы.</li>
                                                                            <li><i class="icon icon-check"></i>Удаление дубликатов.</li>
                                                                            <li><i class="icon icon-check"></i>Приведение к единому формату.</li>
									</ul>

								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
<!--{P:3footer}-->