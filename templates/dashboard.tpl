<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-mobile-web-app-title" content="Викторина">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/app-content/app.css">
    </head>
    <body>
        
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">
                <div ng-view></div>
            </div>
        </div>

        <script src="/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/node_modules/angular/angular.min.js"></script>
        <script src="/node_modules/angular-route/angular-route.min.js"></script>
        <script src="/node_modules/angular-cookies/angular-cookies.min.js"></script>
        <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/assets/app.js"></script>
        
        <!--app dependent scripts -->
        
        <script src="/assets/app-services/authentication.service.js"></script>
        <script src="/assets/app-services/flash.service.js"></script>
        <script src="/assets/app-services/user.service.js"></script>
        <script src="/assets/app-services/question.service.js"></script>
        
        <script src="/assets/dashboard/dashboard.controller.js"></script>
        <script src="/assets/login/login.controller.js"></script>
        <script src="/assets/game/game.controller.js"></script>        
    </body>
</html>