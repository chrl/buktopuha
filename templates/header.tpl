<html>
<head>
    <title><!--{L:title}--></title>
    <script src="/public/js/jquery.js" type="text/javascript"></script>
    <script src="/public/js/jquery.json.js" type="text/javascript"></script>
    <script src="/public/js/catcher.js" type="text/javascript"></script>
<meta name='yandex-verification' content='7e37d630fb0a0338' />
    <link rel="stylesheet" href="/public/style.css" />
    <script>

        function addUserToProject(project,user)
        {
            $('#msg').fadeOut('fast');
            $.post('/ajax/',{method: 'addUserToProject', user: user, project: project},function(data){
                result = $.parseJSON(data);
                
                if (result['result']=='success')
                {
                    $('#usersList').fadeOut('fast',function(){
                        $.post('/ajax/',{method: 'getProjectUsers', project: project, encode: false},function(data){
                            $('#usersList').html(data).fadeIn('fast');
                        });
                    });
                }
                else
                {
                    $('#msg').html(result['message']).fadeIn('fast');
                }
            });
        }
        
        function findProjectUsers()
        {
            
            data = $.post('/ajax/',{method: 'getProjectUsersOption',project: $('#form_project option:selected').val()},
                function(data) {
                    $('#form_user').html(data);
                }
            );
            
            
            
        }
    
        function fixUrl(url)
        {
            if(url.indexOf('http://')<0) url = 'http://'+url;
            return url;
        }
        function loadUrl()
        {
            url = fixUrl($('#url').val());
            $('#mainFrame').attr('src',url);
        }
        

        function getSession(id,question)
        {
            $.post('/ajax/',
                { 
                    method: 'getSession',
                    id: id,
                    question: question,
                },function (data) {
                    
                    data = $.parseJSON(data);
                    if (data.result == 'error') {
                        alert(data.message)
                    } else {
                        window.step = data.step;
                        window.session_id = data.session_id;
                        makeQuestion(data);
                    }
                }
                );
        }
        
        function prevQuestion()
        {
            getSession(window.session_id,window.step-1);
        } 
        
        function nextQuestion()
        {
            getSession(window.session_id,new Number(window.step)+1);
        }
                 
        function finishEdit(type,id)
        {
            text = $('#'+type+'_'+id+' textarea').val();
            $('#'+type+'_'+id).html(getItem(type,id,text));
            
            $.post('/ajax/',{
                method: 'saveRItem',
                type: type,
                id: id,
                text: text,
            },function(data){
                
            });
            
        }
        
        function getItemLI(type,id,text,hide)
        {
            return '<li'+(hide?' style="display:none;"':'')+' id="'+type+'_'+id+'">'+getItem(type,id,text)+'</li>';
        }
        
        function getItem(type,id,text)
        {
            return '<span onclick="editItem(\''+type+'\','+id+');">'+text+'</span><br /><br />';
        }
                
        function addRItem(type,id)
        {
            $.post('/ajax/',{
                method: 'addNewRItem',
                id: id,
                type: type,
            },function(data){
                
                data = $.parseJSON(data);
                
                $('#question_'+id+'_info ul.'+type).append(getItemLI(type,data.id,'Новый элемент',true));
                $('#'+type+'_'+data.id).fadeIn('fast');
            });
            
        }

        function removeRItem(type,id)
        {
            $.post('/ajax/',{
                method: 'removeRItem',
                id: id,
                type: type,
            },function(data){

                $('#'+type+'_'+id).fadeOut('fast');
            });
            
        }
        
        function cancelEdit(type,id)
        {
            text = window.edits[type][id];
            $('#'+type+'_'+id).html(getItem(type,id,text));

        }
        
        function editItem(type,id)
        {
            text = $('#'+type+'_'+id).text();
            window.edits[type][id] = text;
            
            $('#'+type+'_'+id).html('<br /><textarea style="width:100%;height:150px;">'+text+'</textarea><br /><a href="#" onclick="finishEdit(\''+type+'\','+id+');return false;">Сохранить</a> / <a href="#" onclick="cancelEdit(\''+type+'\','+id+');return false;">Отмена</a> / <a style="color:red;" href="#" onclick="removeRItem(\''+type+'\','+id+');return false;">Удалить</a><br /><br />');
        }
        
        function removeQuestion(id)
        {
            $.post('/ajax/',{
                method: 'removeQuestion',
                id: id,
            },function(data){
                data = $.parseJSON(data);
                if (data.result == 'error') {
                    alert(data.message)
                } else {
                    $('#question_'+id).fadeOut('fast',function(){
                        if(data.removelast==1) {
                            $('#category_'+data.category_id).html('<div style="display:none;font-size:14px;color:gray;" class="coverable" id="no_questions_'+data.category_id+'">Вопросов нет.</div>');
                            $('#no_questions_'+data.category_id).fadeIn('fast');
                        }
                    });
                }
                
            });            
        }
        function editQuestion(id)
        {
            $('#qt_'+id).html('<input type="text" id="edit_question_'+id+'" value="'+$('#qt_'+id+' a').html()+'"/> <a href="#" style="font-size:14px;" onclick="saveEditQuestion('+id+');return false;">Сохранить</a>');
        }
        
        function saveEditQuestion(id)
        {
            $.post('/ajax/',{
                method: 'upQuestion',
                id: id,
                text: $('#edit_question_'+id).val(), 
            },function(data){
                data = $.parseJSON(data);
                if (data.result == 'error') {
                    alert(data.message);
                } else {
                    $('#qt_'+id).html('<a href="#" onclick="showQuestionInfo('+id+');return false;">'+$('#edit_question_'+id).val()+'</a>');
                }
            });
            
        }
                
        function saveQuestion(id)
        {
            text = $('#new_question_'+id).val();
            $.post('/ajax/',{
                method: 'saveQuestion',
                category: id,
                text: text, 
            },function(data){
                data = $.parseJSON(data);
                if (data.result == 'error') {
                    alert(data.message)
                } else {
                    $('#no_questions_'+id).fadeOut('fast');                    
                
                    $('#new_question_'+id+'_div').fadeOut('fast',function(){
                        $('#new_question_'+id+'_div').remove();
                        $('#category_'+id).append('<div id="question_'+data.id+'" class="coverable" style="display:none;">&laquo;<a href="#" onclick="showQuestionInfo('+data.id+');return false;">'+data.text+'</a>&raquo; <span style="color:#DDDDDD;">&mdash; <a class="grayred" href="#" onclick="removeQuestion('+data.id+');return false;">удалить</a></span><div id="question_'+data.id+'_info" style="font-size:14px;border:1px dotted black;margin:10px;padding:15px;display:none;"></div></div>');
                        $('#question_'+data.id).fadeIn('fast');
                    });
                }
                
            });
        }

        function addSourceParam()
        {
            i = $('tr.last').length;
            $('<tr class="last"><td><input type="text" class="middlefield" id="param_'+i+'"value="Название параметра" style="color:black;font-size:20px;"/></td><td><input type="text" class="middlefield" id="paramvalue_'+i+'"value="Значение" /></td><td><a href="#" onclick="$(this).parent().parent().remove();">&larr;&nbsp;Удалить</a></td></tr>').insertAfter('tr.last:last');
        }

        function createStrategy()
        {
            $.post('/ajax/',{
                method: 'createStrategy',
                name: $('#form_name').val(),
            },function(data){
                data = $.parseJSON(data);
                if (data.result == 'error') {
                    alert(data.message);
                } else {
                    $('#form_strategy').append('<option value="'+data.id+'">'+data.name+'</option>');
                    $('#form_strategy option[value='+data.id+']').attr('selected','selected');
                }
            });
        }
        
       
        function showInner(key)
        {
            $('#aboutPage').fadeOut('fast',function(){
                $.post('/ajax/',{method: 'showInner',key: key},function(data){
                    result = $.parseJSON(data);
                    $('#aboutPage').html(result.text).fadeIn('slow');
                });
            });
        }
        
        function closeMapTable()
        {
            $('#mapTable').stop().fadeOut('fast',function(){
                $('#mapTable').html('');
            });
        }
        
        function getNewActionBlock(action_id,exitPoint,startAction)
        {
            $.post('/ajax/',{method: 'newActionBlock',action_id: action_id,lastFWid: window.lastWFid,exitPoint:exitPoint,startAction:startAction},
            function(data){
                    data = $.parseJSON(data);
                    if (data.result == 'error') {
                        alert(data.message)
                    } else {
                        console.log(data);

                        action = data.action;

                        additional='<td colspan="4">&nbsp;</td>';
                        additionalBack = '';
                        if(action.exitpoints[0]!='')
                        {
                            additional='<td>&rarr;</td><td>'+action.exitpoints[0]+'</td><td>&rarr;</td><td><a href="#" class="popup">Выбрать действие</a></td>';
                            for(k in action.exitpoints) {
                                if (k!=0){
                                    additionalBack += '<tr><td>&rarr;</td><td>'+action.exitpoints[k]+'</td><td>&rarr;</td><td><a href="#" class="popup">Выбрать действие</a></td></tr>';
                                }
                            }
                        }

                        $('<tr><td wflid="0" actid="'+action.id+'" rowspan="'+action.exitpoints.length+'" valign="top">'+action.name+'</td>'+additional+'</tr>'+additionalBack+'<tr class="padder"><td colspan="5" style="border-bottom:1px solid gray;">&nbsp;</td></tr>').insertAfter('#workflow tr.padder:last');
                        defineActionPopups();
                    }
            });

        }

        window.edits = {};
        $(document).ready(function()
            {
                $('#uinfo_login').mouseover(function(){
                    $('#uinfo').fadeIn('fast');
                });
                
                $('#uinfo').mouseout(function(){
                    window.tm = setTimeout(function(){$('#uinfo').fadeOut('fast');},500);
                });

                $('#uinfo').mouseover(function(){
                    clearTimeout(window.tm);
                });                   
                
                                   
            }
        );
   </script>
</head>