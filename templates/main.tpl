<!--{P:2header}-->
<div role="main" class="main">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Импорт базы контактов</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Импорт контактов</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <section class="page-not-found">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                                      <form id="smpform" enctype="multipart/form-data" action="/validate/" method="POST">
                          <label class="control-label">Файл для загрузки</label>
                          <div class="controls">
                            <input type="file" name="cfile" data-filename-placement="inside" title="Выбрать файл с базой" class="form-control">
                          </div>
                                      </form>
                        </div>
                        <a class="btn btn-primary btn-lg btn-block" onclick="$('#smpform').submit();">Загрузить файл с базой</a>
                    </div>
                </div>
            </section>
        </div>
</div>
<!--{P:2footer}-->