<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Marck+Script&subset=latin,cyrillic" rel="stylesheet" type="text/css">

		<!-- Libs CSS -->
		<link rel="stylesheet" href="/css/bootstrap.css">
		<link rel="stylesheet" href="/css/fonts/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="/vendor/owl-carousel/owl.carousel.css" media="screen">
		<link rel="stylesheet" href="/vendor/owl-carousel/owl.theme.css" media="screen">
		<link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/css/theme.css">
		<link rel="stylesheet" href="/css/theme-elements.css">
		<link rel="stylesheet" href="/css/theme-animate.css">

		<!-- Current Page Styles -->
		<link rel="stylesheet" href="/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/css/skins/blue.css">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="/css/custom.css">

		<!-- Responsive CSS -->
		<link rel="stylesheet" href="/css/theme-responsive.css" />

		<!-- Head Libs -->
		<script src="/vendor/modernizr.js"></script>
		<script src="/vendor/jquery.js"></script>
                
                <link rel="stylesheet" href="/css/datatables.css">
                <link rel="stylesheet" href="/css/dataTables.fixedColumns.min.css">	
                <link rel="stylesheet" href="/css/dataTables.colReorder.min.css">
                <link rel="stylesheet" href="/css/dataTables.tableTools.min.css">

		<!--[if IE]>
			<link rel="stylesheet" href="/css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="/vendor/respond.js"></script>
		<![endif]-->

	</head>
	<body>
            <div class="body">
