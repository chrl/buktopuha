<!--{P:2header}-->
        <div role="main" class="main">
                <!--{P:breadcrumbs}-->
                                <div class="container">
					<h2>Ценовая политика</h2>

					<div class="row">
                                            <div class="col-md-12">
                                                <p class="lead">
                                                    Наш сервис только выходит на рынок, поэтому мы изучаем покупательскую реакцию на наши тарифы. Мы можем предложить вам более выгодные условия или индивидуальные решения &mdash; <a href="/contact/">просто свяжитесь с нами</a>.
                                                </p>
                                            </div>
					</div>

					<hr class="tall" />

					<div class="row">

						<div class="pricing-table">
							<div class="col-md-4">
                                                            <div class="plan">
                                                                <h3>Попробовать<br />
                                                                    <font style="color: black;text-decoration: none;text-transform: none;margin-top: 19px;display: block;">бесплатно</font>
                                                                </h3>
                                                                <a class="btn btn-lg btn-success" href="/try/?price=free">Бесплатно</a>
                                                                <ul>
                                                                    <li>до <b>1000</b> записей в сутки</li>
                                                                    <li>до <b>3 суток</b> хранения</li>
                                                                    <li>до <b>5 000</b> сгенерированных текстов</li>
                                                                </ul>
                                                            </div>
							</div>
							<div class="col-md-4">
                                                            <div class="plan most-popular">
                                                                <div class="plan-ribbon-wrapper"><div class="plan-ribbon">Выгодно!</div></div>
                                                                <h3>Малый бизнес<br />
                                                                    <font style="color: white;text-decoration: none;text-transform: none;margin-top: 19px;display: block;">10 копеек / контакт</font>
                                                                </h3>
                                                                <a class="btn btn-lg btn-primary" href="/try/?price=small">Подключиться</a>
                                                                <ul>
                                                                    <li>до <b>100000</b> записей в сутки</li>
                                                                    <li>до <b>30 суток</b> хранения</li>
                                                                    <li>до <b>500 000</b> сгенерированных текстов</li>
                                                                </ul>
                                                            </div>
							</div>
							<div class="col-md-4">
                                                            <div class="plan">
                                                                <h3>Агрегатор<br />
                                                                    <font style="color: black;text-decoration: none;text-transform: none;margin-top: 19px;display: block;">15 копеек / контакт</font>
                                                                </h3>
                                                                <a class="btn btn-lg btn-primary" href="/try/?price=large">Подключиться</a>
                                                                <ul>
                                                                    <li>до <b>1000000</b> записей в сутки</li>
                                                                    <li>Безлимитное хранение</li>
                                                                    <li>Неограниченное количество сгенерированных текстов</li>
                                                                </ul>
                                                            </div>
							</div>
						</div>

					</div>

				</div>
                                <section class="call-to-action featured footer">
					<div class="container">
						<div class="row">
							<div class="center">
                                                            <h3>DataRich.ru <span style="font-weight:300;">&mdash; сервис по обработке и обогащению клиентских данных.</font> <a href="/try/" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Попробуйте!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
							</div>
						</div>
					</div>
				</section>

			</div>
<!--{P:3footer}-->