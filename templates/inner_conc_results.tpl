<!--{P:header}-->
<!--{P:menu}-->
<table id="centerTable" width="100%" height="100%" cellpadding="10" cellspacing="1">
    <tr>
        <td align="left" valign="top">
        
        <h1><!--{T:search_market_share}--><span class="grayred"> | <a href="?exportCSV"><!--{T:export}--> CSV</a> | <a href="?export"><!--{T:export}--> DOC</a></span></h1>
        <div style="text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
            <!--{L:text}-->
        </div>
        </td>
        <td width="70%" valign="top" align="left">
        <h1><!--{T:pie_chart}--></h1>
        <div style="text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">        
            <div id="container" style="width: 100%; height: 400px"></div>
        </div>
        <br />
        <h1><!--{T:block_chart}--></h1>
        <div style="text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
            <div id="container2" style="width: 100%; height: 400px"></div>
        </div>
<script src="/highcharts.js"></script>
<script>
var chart;
$(document).ready(function() {
   chart = new Highcharts.Chart({
      chart: {
         renderTo: 'container',
         plotBackgroundColor: null,
         plotBorderWidth: null,
         plotShadow: true
      },
      title: {
         text: ''
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.point.y +' %';
         }
      },
      plotOptions: {
         pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
               enabled: true,
               color: '#000000',
               connectorColor: '#000000',
               formatter: function() {
                  return '<b>'+ this.point.name +'</b>: '+ this.point.y +' %';
               }
            }
         }
      },
       series: [{
         type: 'pie',
         name: 'Владение рынком',
         data: [
            <!--{L:charts}-->
         ]
      }]
   });
chart2 = new Highcharts.Chart({
      chart: {
         renderTo: 'container2',
         defaultSeriesType: 'column',
         margin: [ 50, 50, 100, 80]
      },
      title: {
         text: ''
      },
      xAxis: {
         categories: [
            <!--{L:cats}-->
         ],
         labels: {
            rotation: -45,
            align: 'right',
            style: {
                font: 'normal 13px Verdana, sans-serif'
            }
         }
      },
      yAxis: {
         min: 0,
         title: {
            text: '<!--{T:search_market_share}-->'
         }
      },
      legend: {
         enabled: false
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.x +'</b><br/>'+
                this.y;
         }
      },
      series: [{
         name: 'Population',
         data: [<!--{L:values}-->],
         dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            x: -3,
            y: 10,
            formatter: function() {
               return this.y;
            },
            style: {
               font: 'normal 13px Verdana, sans-serif'
            }
         }
      }]
   });

});


</script>
    </td>
    </tr>
</table>
<script>
function removeSegmentUri(segment,site) {
    $.post('/ajax/',{method: 'removeSegmentUri',task_id: segment, site: site},function(data){
        location.reload(true);
    });
}
</script>
<!--{P:bottom}-->