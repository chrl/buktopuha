<!--{P:header}-->
<!--{P:unreg_menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="10">
    <tr>
        <td align="center" valign="top">
        <div style="background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
        <table style="margin-top:20px;" cellspacing="10" id="regform">
            <tr>
                <td colspan="2" style="font-size:14px;color:#C5C5C5;padding-top:10px;padding-bottom:30px;">
                <!--{T:form_fields_desc}-->
					
                
                </td>
            </tr>        
            <tr><td><!--{T:_register_form_invite}--></td><td><input id="form_invite" class="middlefield" type="text" /></td></tr>
            <tr><td><!--{T:_register_form_login}--></td><td><input id="form_login" class="middlefield" type="text" /></td></tr>
            <tr><td><!--{T:_register_form_mail}--></td><td><input id="form_mail" class="middlefield" type="text" /></td></tr>
            <tr><td><!--{T:_register_form_phone}--></td><td><input id="form_phone" class="middlefield" type="text" /></td></tr>
            <tr><td><!--{T:_register_form_pass1}--></td><td><input id="form_pass1" class="middlefield" type="password" /></td></tr>
            <tr><td><!--{T:_register_form_pass2}--></td><td><input id="form_pass2" class="middlefield" type="password" /></td></tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="margin-right:15px;margin-bottom:10px;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2" align="right"><a class="handsomeButton" style="margin-right:15px;" href="#" id="regform_submit" onclick="catcher('regform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>
        </div>
        </td>
        <td width="40%" valign="top" align="center">
            <div style="margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
                Регистрация работает только по инвайтам.<br /><br />
                На данный момент Mushspider &mdash; очень молодая система, и в ней много багов. 
                Поэтому мы пускаем вовнутрь только людей, намеренных помочь нам с тестированием системы.<br /><br />
                Так, за каждые 3 найденных бага любой текущий пользователь Mushspider получит инвайт &mdash; короткий код, которым он сможет поделиться с другом или коллегой.
                Если у Вас нет друзей, которые могут поделиться инвайтами, но вы хотите помочь нам с тестированием, <a href="mailto:info@mushspider.ru">пишите</a> &mdash; мы что-нибудь обязательно придумаем.
            </div>
            <br /><br />
            <a href="#">Зачем регистрироваться?</a>&nbsp;&nbsp;&nbsp;<a href="#">О сервисе</a>&nbsp;&nbsp;&nbsp;<a href="#">Контакты</a>
        </td>
    </tr>
</table>
<!--{P:bottom}-->