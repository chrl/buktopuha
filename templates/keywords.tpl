<!--{P:header}-->
<!--{P:menu}-->
<div id="manualAddBox" style="display:none;position:absolute;width:600px;height:400px;left:50%;top:50px;margin-left:-300px;border:1px solid #E5E5E5;background-color:white;box-shadow: 0px 0px 15px 0px black;padding:10px;">
    <textarea id="manual" style="width:100%;height:90%"></textarea>
    <div style="float:left;margin-top:13px;" id="wordscount"></div>
    <div style="float:right;margin-top:13px;" align="right">
        <a class="handsomeButton" href="#" onclick="manualAddAll();return false;"><!--{T:add}--></a>
        <a class="handsomeButton" href="#" onclick="$('#manualAddBox').fadeOut('fast');return false;"><!--{T:close}--></a>
    </div>
</div>
<table id="centerTable" width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2" height="70">
            <table cellpadding="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <input id="searchKeys" class="middlefield" style="width:100%;" />
                </td>
                <td width="10">
                    <a id="goSearch" class="handsomeButton" href="#" onclick="getSimiliarKeys();return false;" style="margin-left:10px;"><!--{T:adjust_keywords}--></a>
                </td>
            </table>
        </td>
    </tr>
    <tr>
        <td width="50%" valign="top" style="border-top:1px solid #E5E5E5;padding-top:20px;">
            <div id="activelist">
                <ul><!--{L:keys}--></ul>
            </div>
            <div><a href="?export" target="_blank"><!--{T:export_list}--></a></div>
        </td>
        <td width="50%" valign="top" style="border-top:1px solid #E5E5E5;padding-top:20px;">
            <div id="keyslist">
            </div>
        </td>
    </tr>
</table>
<script>
    function addMassKeyword()
    {
        
        $.post('/ajax/',{method: 'addKeyword',segment_id: <!--{L:segment_id}-->,keyword: window.content[window.icount],quant: 0},function(data){
            result = $.parseJSON(data);
            $('#wordscount').html('<!--{T:added}--> '+(window.icount+1)+' из '+window.content.length);
            if (result['result']=='ok')
            {
                $('#activelist ul').append('<li id="keywrd_'+result['id']+'" class="fresh" style="display:none;">'+window.content[window.icount]+'('+result['quant']+') <a href="#" onclick="removeKeyword('+result['id']+');return false;">&larr; <!--{T:delete}--></a></li>');
                $('li.fresh').fadeIn('fast',function(){
                    $(this).removeClass('fresh');
                });
            }
            else {
                if (result['result']=='double') {

                }
                else
                {
                    alert(result['msg']);
                }
            }

            if (window.content.length>window.icount+1) {
                window.icount++;
                addMassKeyword();
            } else {
                $('#wordscount').html('');
                $('#manualAddBox').fadeOut('fast');
            }
        });
    }

    function addKeyword(value,quant)
    {




        $.post('/ajax/',{method: 'addKeyword',segment_id: <!--{L:segment_id}-->,keyword: value,quant: quant},function(data){
            result = $.parseJSON(data);
            if (result['result']=='ok')
            {
                window.nextKw = 'true';

                $('#activelist ul').append('<li id="keywrd_'+result['id']+'" class="fresh" style="display:none;">'+value+'('+result['quant']+') <a href="#" onclick="removeKeyword('+result['id']+');return false;">&larr; <!--{T:delete}--></a></li>');
                $('li.fresh').fadeIn('fast',function(){
                    $(this).removeClass('fresh');
                });
            }
            else {
                if (result['result']=='double') {
                    window.nextKw = 'true';
                }
                else
                {
                    alert(result['msg']);
                }
            }
        });
    }

    function removeKeyword(value)
    {

        $.post('/ajax/',{method: 'removeKeyword',keyword_id: value},function(data){
            result = $.parseJSON(data);
            if (result['result']=='ok')
            {
                $('li#keywrd_'+value).fadeOut('fast',function(){
                    $(this).remove();
                });
            }
            else
            {
                alert(result['msg']);
            }
        });
    }

    function massAdd()
    {
        $('#manualAddBox').fadeIn('fast');
    }

    function manualAddAll()
    {
        window.content = $('#manual').val().replace('\r','').split('\n');
        window.icount=0;

        addMassKeyword(0);

        $('#manual').val('');
        //$('#manualAddBox').fadeOut('fast');

    }


    function getSimiliarKeys()
    {

        $('#keyslist').fadeOut('fast',function(){
            $('#keyslist').html('<div align="center"><img src="/ajax-loader.gif" /></div>').fadeIn('fast',function(){
                $.post('/ajax/',{method: 'getSimiliarKeys',input: $('#searchKeys').val()},function(data){
                    result = $.parseJSON(data);

                    if (result['result']=='ok')
                    {
                        $('#keyslist').fadeOut('fast',function(){
                            $('#keyslist').html('<table width="100%" cellpadding="10"></table>');
                            for(i in result['keys'])
                            {
                                $('#keyslist table').append('<tr><td>'+i+'</td><td>'+result['keys'][i]+'</td></tr>');
                            }
                            $('#keyslist table tr:visible:nth-child(even)').css('background-color','#EEEEEE');
                            $('#keyslist table tr:visible:nth-child(odd)').css('background-color','#DDDDDD');
                            $('#keyslist table tr').click(function(){
                                value = $(this).find('td:eq(0)').text();
                                quant = $(this).find('td:eq(1)').text();
                                addKeyword(value,quant);
                                $(this).fadeOut('fast',function(){
                                    $(this).remove();
                                    $('#keyslist table tr:visible:nth-child(even)').css('background-color','#EEEEEE');
                                    $('#keyslist table tr:visible:nth-child(odd)').css('background-color','#DDDDDD');
                                });
                            });
                            $('#keyslist').fadeIn('fast');
                        });
                    }
                    else
                    {
                        alert(result['msg']);
                    }
                });
            });

        });
    }

$(document).ready(function(){
    $('#searchKeys').keypress(function(event){
        if (event.which == '13') {
            $('#goSearch').click();
        }
    });
    $('#manualadd').keypress(function(event){
        if (event.which == '13') {
            $('#goAdd').click();
        }
    });
    
});
    
</script>
<!--{P:bottom}-->