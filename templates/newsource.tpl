<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <table style="margin-top:20px;width:800px;" cellspacing="10" id="newsourceform">
            <input type="hidden" id="form_project" value="<!--{L:project_id}-->" />
            <tr><td>Название источника данных</td><td><input id="form_name" class="middlefield" type="text" /></td><td></td></tr>
            <tr><td>Стратегия сбора данных</td><td><select id="form_strategy" class="middlefield"><!--{L:select_strategy}--></select></td><td><a href="#" onclick="createStrategy();return false;">&larr; Создать новую</a></td></tr>
            <tr class="last"><td>Краткое описание источника</td><td><textarea id="form_comment" class="middlefield"></textarea></td><td></td></tr>
            <tr><td colspan="3"><a href="#" onclick="addSourceParam();return false;">&uarr; Добавить параметр</a></td></tr>
            <tr>
                <td colspan="3" style="font-size:15px;padding-top:20px;padding-bottom:20px;">Источник данных &mdash; источник, из которого будут браться данные для проекта. Сбор данных из каждого источника должен происходить по стратегии сбора. Добавить стратегию можно в разделе &laquo;<a href="/strategy/">Стратегии</a>&raquo;</td>
            </tr>
            <tr>
            <td colspan="3">
            <div id="msg" style="font-size:16px;color:red;padding:20px;border:1px solid red;display:none;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" id="testform_submit" onclick="catcher('newsourceform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>

        </td>
    </tr>
</table>
<!--{P:bottom}-->