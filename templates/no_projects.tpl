<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 800px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
            У Вас пока еще нет созданных проектов.<br /><br />
            Проект нужен для того, чтобы Вам было понятно, к чему относятся Ваши исследования. Если у Вас несколько сегментов рынка, не переживайте &mdash; внутри проекта можно будет создать сегменты. Также к проекту можно предоставить доступ кому-нибудь другому, например коллеге.<br /><br />

                <a class="handsomeButton" href="/projects/create/">Создать новый проект &rarr;</a>

        </div>

        </td>
    </tr>
</table>
<!--{P:bottom}-->