<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 800px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
        <table style="margin-top:20px;" cellspacing="10" id="newsegmentform">
            <input type="hidden" id="form_project" value="<!--{L:project_id}-->" />
            <tr><td>Название сегмента</td><td><input id="form_name" class="middlefield" type="text" /></td></tr>
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:20px;padding-bottom:20px;">
                    Проекты состоят из сегментов. Сегмент &mdash; это сегмент рынка, характеризующийся своим собственным семантическим полем, своей целевой аудиторией и мотивацией пользователей.<br /><br />
                    Если Вы затрудняетесь самостоятельно разбить рынок на сегменты &mdash; создайте один сегмент, позже Вы сможете кластеризовать его семантическое поле автоматически.
                
                </td>
            </tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="margin-bottom:20px;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" class="handsomeButton" id="testform_submit" onclick="catcher('newsegmentform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>
        </div>
        </td>
    </tr>
</table>
<!--{P:bottom}-->