<section class="page-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active"><!--{L:page_name}--></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2><!--{L:page_name}--></h2>
            </div>
        </div>
    </div>
</section>