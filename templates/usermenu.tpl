    <div class="navbar-collapse nav-main-collapse collapse">
            <div class="container">
                    <nav class="nav-main mega-menu">
                            <ul class="nav nav-pills nav-main" id="mainMenu">

                                    <li class="active">
                                            <a href="shortcodes.html">Главная</a>
                                    </li>

                                    <li class="dropdown mega-menu-item mega-menu-fullwidth">
                                            <a class="dropdown-toggle" href="#">
                                                    Документация API
                                                    <i class="icon icon-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                            <div class="mega-menu-content">
                                                                    <div class="row">
                                                                            <div class="col-md-3">
                                                                                    <ul class="sub-menu">
                                                                                            <li>
                                                                                                    <span class="mega-menu-sub-title">Main Features</span>
                                                                                                    <ul class="sub-menu">
                                                                                                            <li><a href="feature-pricing-tables.html">Pricing Tables</a></li>
                                                                                                            <li><a href="feature-icons.html">Icons</a></li>
                                                                                                            <li><a href="feature-animations.html">Animations</a></li>
                                                                                                            <li><a href="feature-typograpy.html">Typograpy</a></li>
                                                                                                            <li><a href="feature-grid-system.html">Grid System</a></li>
                                                                                                    </ul>
                                                                                            </li>
                                                                                    </ul>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                    <ul class="sub-menu">
                                                                                            <li>
                                                                                                    <span class="mega-menu-sub-title">Headers</span>
                                                                                                    <ul class="sub-menu">
                                                                                                            <li><a href="index.html">Header Version 1</a></li>
                                                                                                            <li><a href="index-header-2.html">Header Version 2</a></li>
                                                                                                            <li><a href="index-header-3.html">Header Version 3</a></li>
                                                                                                            <li><a href="index-header-4.html">Header Version 4</a></li>
                                                                                                            <li><a href="index-header-5.html">Header Version 5</a></li>
                                                                                                            <li><a href="index-header-6.html">Header Version 6</a></li>
                                                                                                            <li><a href="index-header-7.html">Header Version 7</a></li>
                                                                                                            <li><a href="index-header-signin.html">Header - Sign In / Sign Up</a></li>
                                                                                                            <li><a href="index-header-logged.html">Header - Logged</a></li>
                                                                                                    </ul>
                                                                                            </li>
                                                                                    </ul>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                    <ul class="sub-menu">
                                                                                            <li>
                                                                                                    <span class="mega-menu-sub-title">Footers</span>
                                                                                                    <ul class="sub-menu">
                                                                                                            <li><a href="index.html#footer">Footer Version 1</a></li>
                                                                                                            <li><a href="index-footer-2.html#footer">Footer Version 2</a></li>
                                                                                                            <li><a href="index-footer-3.html#footer">Footer Version 3</a></li>
                                                                                                            <li><a href="index-footer-4.html#footer">Footer Version 4</a></li>
                                                                                                    </ul>
                                                                                            </li>
                                                                                    </ul>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                    <ul class="sub-menu">
                                                                                            <li>
                                                                                                    <span class="mega-menu-sub-title">Blog</span>
                                                                                                    <ul class="sub-menu">
                                                                                                            <li><a href="blog-full-width.html">Blog Full Width</a></li>
                                                                                                            <li><a href="blog-large-image.html">Blog Large Image</a></li>
                                                                                                            <li><a href="blog-medium-image.html">Blog Medium Image</a></li>
                                                                                                            <li><a href="blog-timeline.html">Blog Timeline</a></li>
                                                                                                            <li><a href="blog-post.html">Single Post</a></li>
                                                                                                    </ul>
                                                                                            </li>
                                                                                    </ul>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </li>
                                            </ul>
                                    </li>
                                    <li>
                                            <a href="shortcodes.html">Примеры использования</a>
                                    </li>								
                                    <li class="dropdown mega-menu-item mega-menu-signin signin" id="headerAccount">
                                            <a class="dropdown-toggle" href="/enter/">
                                                    <i class="icon icon-user"></i> Войти
                                                    <i class="icon icon-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                            <div class="mega-menu-content">
                                                                    <div class="row">
                                                                            <div class="col-md-12">

                                                                                    <div class="signin-form">

                                                                                            <span class="mega-menu-sub-title">Войти</span>

                                                                                            <form action="" id="" type="post">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>Логин или E-Mail</label>
                                                                                                                            <input type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <a class="pull-right" id="headerRecover" href="#">(Потеряли пароль?)</a>
                                                                                                                            <label>Пароль</label>
                                                                                                                            <input type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-6">
                                                                                                                    <span class="remember-box checkbox">
                                                                                                                            <label for="rememberme">
                                                                                                                                    <input type="checkbox" id="rememberme" name="rememberme">Запомнить меня
                                                                                                                            </label>
                                                                                                                    </span>
                                                                                                            </div>
                                                                                                            <div class="col-md-6">
                                                                                                                    <input type="submit" value="Войти" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </form>

                                                                                            <p class="sign-up-info">Еще нет аккаунта? <a href="#" id="headerSignUp">Зарегистрируйтесь!</a></p>

                                                                                    </div>

                                                                                    <div class="signup-form">
                                                                                            <span class="mega-menu-sub-title">Создать аккаунт</span>

                                                                                            <form action="" id="" type="post">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>E-mail</label>
                                                                                                                            <input type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-6">
                                                                                                                            <label>Пароль</label>
                                                                                                                            <input type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                                    <div class="col-md-6">
                                                                                                                            <label>Повторите пароль</label>
                                                                                                                            <input type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                    <input type="submit" value="Зарегистрироваться" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </form>

                                                                                            <p class="log-in-info">Уже есть аккаунт? <a href="#" id="headerSignIn">Войдите на сайт!</a></p>
                                                                                    </div>

                                                                                    <div class="recover-form">
                                                                                            <span class="mega-menu-sub-title">Reset My Password</span>
                                                                                            <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                                                            <form action="" id="" type="post">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>E-mail Address</label>
                                                                                                                            <input type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                    <input type="submit" value="Submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </form>

                                                                                            <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel">Log In!</a></p>
                                                                                    </div>

                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </li>
                                            </ul>
                                    </li>


                            </ul>
                    </nav>
            </div>
    </div>