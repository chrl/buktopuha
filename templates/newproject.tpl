<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <div style="width: 800px; margin-left:20px;text-align:left;background-color:#F8F8F8;border:1px solid #E5E5E5;padding:20px;border-radius:10px;">
        <table style="margin-top:20px;" cellspacing="10" id="newprojectform">
            <tr><td>Название проекта</td><td><input id="form_name" class="middlefield" type="text" /></td></tr>
            <tr><td>Краткое описание проекта</td><td><textarea style="height:100px;" id="form_desc" class="middlefield"></textarea></td></tr>
            <tr>
                <td colspan="2" style="font-size:15px;padding-top:20px;padding-bottom:20px;">Проект нужен для того, чтобы было понятно, к чему относятся Ваши исследования. Если у Вас несколько сегментов рынка, не переживайте &mdash; внутри проекта можно будет создать сегменты. Также к проекту можно предоставить доступ кому-нибудь другому, например коллеге.</td>
            </tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="margin-bottom:20px;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" class="handsomeButton" id="testform_submit" onclick="catcher('newprojectform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>
        </div>

        </td>
    </tr>
</table>
<!--{P:bottom}-->