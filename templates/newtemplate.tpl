<!--{P:header}-->
<!--{P:menu}-->
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
        <table style="margin-top:20px;" cellspacing="10" id="newprojectform">
            <tr><td>Название шаблона</td><td><input id="form_name" class="middlefield" type="text" /></td></tr>
            <tr><td>Файл шаблона</td><td><input type="file" name="filedata" id="filedata"></td></tr>
            <tr>
                <td colspan="2" style="width:600px;font-size:15px;padding-top:20px;padding-bottom:20px;"><b>Правила</b><br /><br />Файлом шаблона может быть файл в формате .docx с заранее проставленными заменяемыми метками. Метки выглядат так: <b>%%placeholder%%</b>. Позже, в места где находятся эти метки в шаблоне, Вы сможете вывести сгенерированный текст.</td>
            </tr>
            <tr>
            <td colspan="2">
            <div id="msg" style="font-size:16px;color:red;padding:20px;border:1px solid red;display:none;">

            </div>
            </td>
            </tr>
            <tr>
                <td colspan="2"><a href="#" id="testform_submit" onclick="catcher('newprojectform');return false;"><!--{t:_register_go_on}--></a></td>
            </tr>
        </table>

        </td>
    </tr>
</table>
<link href="/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/uploadify/swfobject.js"></script>
<script type="text/javascript" src="/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#filedata').uploadify({
    'swf'       : '/uploadify/uploadify.swf',
    'script'    : '/recieve/',
    'uploader'  : '/recieve/',
    'checkExisting': false,
    'cancelImage' : '/uploadify/uploadify-cancel.png',
    'buttonText': 'Выбрать файлы',
    'folder'    : '/uploads',
    'auto'      : true,
    'debug'     : true,
    'fileTypeDesc'    : 'Документы',
    'fileTypeExts'    : '*.docx',
    'onUploadSuccess': function (file,data,response) {
                            console.log(data);
                       }
  });
});
</script>

<!--{P:bottom}-->