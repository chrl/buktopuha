<!--{P:header}-->
<!--{P:menu}-->
<style>
	.mGroup {
        line-height: 30px;
	}
    
    .gTitle {
        padding-left:20px;
    }
    
    .gTitle div {
         float:right;
         padding-right:20px;
         display:none;
    }
    
    .gTitle div img {
        margin-top: 10px;
        cursor:pointer;
    }
    
    .gTitle:hover div {

         display:block;
    }
    
    
    .gTitle:hover {
        background-color:#F5F5F5;
    } 
    
    .highlighter {
        border-left: 4px solid #DD4B39;
        left: 0;
        position: absolute;
    }
    .mItem {
        padding-left:30px;
	   display:none;
    }
	
    .mItem a {
		border-bottom: 1px dotted;
		text-decoration: none;
    }

    .mGroup a {
		border-bottom: 1px dotted;
		text-decoration: none;
    }	
	
    .mItem:hover {
        background-color:#F5F5F5;
    } 	
	
	.desc {
		background-color:#CCCCCC;
		padding:20px;
		border:1px dotted #222;
	}
	
</style>
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
		<td width="220" valign="top">
			<!--{L:menu}-->
		</td>
		<td valign="top" style="padding:6px 10px 10px;" id="container">
			<h1>API MushSpider.ru</h1>
			API MushSpider предназначена для автоматизации управления работой сервиса MushSpider.
			Это набор программных интерфейсов, с помощью которых стороннее приложение может обмениваться информацией с системой MushSpider.
			Все методы, доступные в API перечислены в левом меню.
			<h1>Аутентификация</h1>
			Аутентификация в API происходит с помощью передачи API KEY, который можно получить в <a href="/settings/">настройках аккаунта</a>.
			<h1>Адрес для запроса</h1>
			Все запросы к API необходимо направлять на адрес: 
			<div class="desc"><center>https://mushspider.ru/api/?apikey=APIKEY&method= ...</center></div>
			
		</td>
    </tr>
</table>
<script>
	$(document).ready(function(){
		$('div.gTitle a').click(function(){
			$(this).parent().parent().find('div.mItem').toggle();
			return false;
		});
		$('div.mItem a').click(function(){
			method = $(this).attr('method');
			
			$('.highlighter').remove();
			
			$(this).parent().prepend('<span class="highlighter" style="border-left: 4px solid #0099FF;">&nbsp;</span>');
			$(this).parent().parent().prepend('<span class="highlighter">&nbsp;</span>');

			$.post('/ajax/',{method:'getApiDescription',subject: method},function(data){
				data = $.parseJSON(data);
				if (data.result=='ok') {
					$('#container').html(data.text);
				}
				
			});
			
			return false;
		});		
		
	});
</script>
<!--{P:bottom}-->