<?php

?>
<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="app-content/app.css">
    </head>
    <body>
        
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">
                <div ng-view></div>
            </div>
        </div>

        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/angular/angular.min.js"></script>
        <script src="node_modules/angular-route/angular-route.min.js"></script>
        <script src="node_modules/angular-cookies/angular-cookies.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="app.js"></script>
        
        <!--app dependent scripts -->
        
        <script src="app-services/authentication.service.js"></script>
        <script src="app-services/flash.service.js"></script>

        <!-- Real user service that uses an api -->
        <!-- <script src="app-services/user.service.js"></script> -->

        <!-- Fake user service for demo that uses local storage -->
        <script src="app-services/user.service.local-storage.js"></script>

        <script src="dashboard/dashboard.controller.js"></script>
        <script src="login/login.controller.js"></script>
        <script src="register/register.controller.js"></script>
        
    </body>
</html>