#!/usr/bin/php
<?php

/**
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/
$dir = getcwd();
chdir(dirname(__FILE__));
define('CONFIG', dirname(__FILE__).'/config/current/cli.php');
$config = require_once(CONFIG);
require_once($config['constants']['BULLSHARKDIR'].'/core/core.php');                           // Base class load
BS::start();
chdir($dir);
