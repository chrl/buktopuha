<?php

/**
 * @package 02p
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
 * @TODO: some failproof scheme
*/

chdir('..');

error_reporting(E_ALL);
// check for clinics

$clinics = array(
    'aibolit.25med.ru'=>1,
);

//

$allowed = array('195.91.233.1','109.73.3.59','145.255.1.196');

if(false && !in_array($_SERVER['REMOTE_ADDR'],$allowed)) {

    $res = gethostbyaddr($_SERVER['REMOTE_ADDR']);

    if (false===strpos($res,'dynamic.ufanet.ru')) {

        echo 'Under construction';
        die;

    }
}

if (array_key_exists($_SERVER['HTTP_HOST'], $clinics)) {
    define('CONFIG','./config/current/config.'.$clinics[$_SERVER['HTTP_HOST']].'.php');
} else {
    define('CONFIG','./config/current/default.php');
}
$config = require_once(CONFIG);

require_once($config['constants']['BULLSHARKDIR'].'/core/core.php');                           // Base class load
BS::start();