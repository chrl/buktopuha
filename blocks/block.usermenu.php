<?php
/**
 * Projekt: BullShark
 * Author: Kirill S. Holodilin
 * Date: 2010, July
 * Block: text, simple
 */

class blockUsermenu
{
    var $version='usermenu v0.1';
    public function run($state,$arg)
    {
       if (isset($_SESSION['user']))
       {
            $menuParams = BS::getConfig('menuparams');
            $url = implode('.',BS::getUrl());
            
            foreach($menuParams as $key=>$value)
            {
                if(preg_match_all($key,$url,$matches))
                {
                    $menu = $value;
                    break;
                }
            }
            
            if(isset($menu))
            {
                $txt = '';
                if (isset($menu['back']))
                {
                  $txt = '<div style="display:inline-block;width:196px;text-align:center;"><a class="menu_red" href="'.$menu['back']['uri'].'">&larr; '.$menu['back']['name'].'</a></div> <span class="divider">&nbsp;</span> ';
                  unset($menu['back']);  
                }
                 
                if (isset($menu['current']))
                {
                  if ($arg != 'extend') {
                        $txt.= '<span class="menu">'.$menu['current'].'</span>';
                  }
                  
                  BS::Visual()->title = $menu['current'];
                    
                  if (isset($arg) && $arg == 'current') {
                        return '<span class="menu" style="padding-left:10px;">'.$menu['current'].'</span>';
                  }
                  
                  unset($menu['current']);
                  
                }     
                
                $cnt = count($menu);
                if ($cnt>0)
                {
                    $i=0;
                    foreach($menu as $key=>$item)
                    {
                        if($key[0]=='$') //admin access only
                        {
                            $i++;
                            if ($_SESSION['user']['role']==2)
                            {

                                $txt.='<a class="menu" href="'.$item.'">'.substr($key,1).'</a>'.(($i!=$cnt)?' <span class="divider">&nbsp;</span> ':'');
                            }
                        } else {
                            $i++;
                            $txt.='<a class="menu" href="'.$item.'">'.$key.'</a>'.(($i!=$cnt)?' <span class="divider">&nbsp;</span> ':'');
                        }
                    }
                }
                return $txt;
                
            } else throw new Exception('MenuParams not set for uri: '.$url);
            
            return '';
       }
       else return '';
    }
}
?>