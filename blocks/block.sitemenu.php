<?php
/**
 * Projekt: BullShark
 * Author: Kirill S. Holodilin
 * Date: 2010, July
 * Block: text, simple
 */

class blockSitemenu
{
    var $version='usermenu v0.1';
    public function run($state,$arg)
    {
        $menuParams = BS::getConfig('menuparams');
        $menuTree = BS::getConfig('menutree');
        $url = implode('.',BS::getUrl());
        $menu = '--false--';
        foreach($menuParams as $key=>$value)
        {
            if(preg_match_all($value['uri'],$url,$matches))
            {
                $menu = $key;
                BS::Visual()->page_name = $value['name'];
                break;
            }
        }
        
        $text = '';
		
		$apimenu = '';
		
		require_once('apps/app.api.php');
		$apiWorker = new apiworker();
		foreach ($apiWorker->groups as $key=>$group) {
			$apimenu.='<div class="col-md-3">
                        <ul class="sub-menu">
                            <li>
                                <span class="mega-menu-sub-title">'.$group.'</span>
                                <ul class="sub-menu">
                                    ';

								foreach ($apiWorker->methods as $kp=>$method) {
									if ($method['group']==$key) {
										$apimenu.='<li><a href="/apidoc/?method='.$kp.'" data-toggle="tooltip" rel="tooltip" data-placement="right" data-container="body" title="'.$method['name'].'">'.$kp.'</a></li>';
									}
								}
                                $apimenu.='</ul>
                            </li>
                        </ul>
                    </div>';
		}
        
        foreach ($menuTree as $key=>$value) {
            if (isset($menuParams[$key])) {
                $st = $menuParams[$key];
            } else {
                throw new Exception('Block '.$key.' not found in menuparams');
            }
            
            if ($value['template']=='none') {
                $text.='<li'.($menu==$key?' class="active"':'').'><a href="'.$value['uri'].'">'.$st['name'].'</a></li>';
            } else {
                $e = BS::Visual()->preloadTemplate($value['template']);
				
				if ($value['template']=='apidoc_menu') {
					BS::Visual()->apimenu = $apimenu;
					BS::Visual()->apimenuactive = ($menu==$key) ? ' active':'';
				}
				
                $text.=$e;
            }
            
            
        }

        return $text;
    }
    
}
?>