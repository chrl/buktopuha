<?php

class blockUserinfo
{
    var $version='usermenu v0.1';
    public function run($state,$arg)
    {
        
       $url = BS::getUrl();
       $url = implode('.', $url);
        
       if (isset($_SESSION['user']))
       {
           
           return '<li class="dropdown mega-menu-item mega-menu-signin signin logged" id="headerAccount">
									<a class="dropdown-toggle" href="#">
										<i class="icon icon-user"></i> '.$_SESSION['user']['mail'].'
										<i class="icon icon-angle-down"></i>
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-8">
														<div class="user-avatar">
															<div class="img-thumbnail">
																<img src="http://www.gravatar.com/avatar/'.md5($_SESSION['user']['mail']).'?s=60" alt="">
															</div>
															<p><strong>'.$_SESSION['user']['mail'].'</strong><span>Баланс: 1000 вызовов API</span></p>
														</div>
													</div>
													<div class="col-md-4">
														<ul class="list-account-options">
															<li>
																<a href="/profile/">Настройки</a>
															</li>
															<li>
																<a href="/logout/">Выход</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
';
       }
       else return '<li class="dropdown mega-menu-item mega-menu-signin signin '.($url=='enter'?'active':'').'" id="headerAccount">
                                            <a class="dropdown-toggle" href="/enter/">
                                                    <i class="icon icon-user"></i> Войти
                                                    <i class="icon icon-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                            <div class="mega-menu-content">
                                                                    <div class="row">
                                                                            <div class="col-md-12">

                                                                                    <div class="signin-form">

                                                                                            <span class="mega-menu-sub-title">Войти</span>

                                                                                            <div id="floginform">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>Логин или E-Mail</label>
                                                                                                                            <input id="flogin" type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <a class="pull-right" id="headerRecover" href="#">(Потеряли пароль?)</a>
                                                                                                                            <label>Пароль</label>
                                                                                                                            <input id="fpass" type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-6">
                                                                                                                    <span class="remember-box checkbox">
                                                                                                                            <label for="rememberme">
                                                                                                                                    <input type="checkbox" id="frememberme" name="rememberme">Запомнить меня
                                                                                                                            </label>
                                                                                                                    </span>
                                                                                                            </div>
                                                                                                            <div class="col-md-6">
                                                                                                                    <input type="submit" id="flogbutton" onclick="catcher(\'floginform\');return false;" value="Войти" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>

                                                                                            <p class="sign-up-info">Еще нет аккаунта? <a href="#" id="headerSignUp">Зарегистрируйтесь!</a></p>

                                                                                    </div>

                                                                                    <div class="signup-form">
                                                                                            <span class="mega-menu-sub-title">Создать аккаунт</span>

                                                                                            <div id="fsignupform">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>E-mail</label>
                                                                                                                            <input id="fmail" type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-6">
                                                                                                                            <label>Пароль</label>
                                                                                                                            <input id="fpass1" type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                                    <div class="col-md-6">
                                                                                                                            <label>Повторите пароль</label>
                                                                                                                            <input id="fpass2" type="password" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                    <input id="fsignupbutton" type="submit" value="Зарегистрироваться" class="btn btn-primary pull-right push-bottom" onclick="catcher(\'fsignupform\');return false;">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>

                                                                                            <p class="log-in-info">Уже есть аккаунт? <a href="#" id="headerSignIn">Войдите на сайт!</a></p>
                                                                                    </div>

                                                                                    <div class="recover-form">
                                                                                            <span class="mega-menu-sub-title">Reset My Password</span>
                                                                                            <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                                                            <form action="" id="" type="post">
                                                                                                    <div class="row">
                                                                                                            <div class="form-group">
                                                                                                                    <div class="col-md-12">
                                                                                                                            <label>E-mail Address</label>
                                                                                                                            <input type="text" value="" class="form-control">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                    <input type="submit" value="Submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </form>

                                                                                            <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel">Log In!</a></p>
                                                                                    </div>

                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </li>
                                            </ul>
                                    </li>

';
    }
}
?>