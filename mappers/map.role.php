<?php
class map_role extends map_everything {
	var $table  = 'roles';
	var $entity = array
	(
		'id'=>'id',
        'name'=>'name',

	);
    
    public function getRightsForRole($role_id)
    {
        $sql = 'select * from role_rights where role_id = ? order by alias asc';
        $res=BS::Database()->q($sql,array('role_id'=>$role_id));
		
        $result = array();
        if (!$res)return false;
        while (false !== $row = mysql_fetch_assoc($res))
        {
            $result[]=$row['alias'];
        }
        return ($result);
    }	
}