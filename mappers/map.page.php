<?php
class map_page extends map_everything {
	var $table  = 'pages';
	var $entity = array
	(
		'id'=>'id',
        'author'=>'author',
		'title'=>'title',
		'text'=>'text',
        'alias'=>'alias',
        'level'=>'level',
        'showmain'=>'showmain',
	);	
    
    
    public function dropPages()
    {
        $sql = 'truncate '.$this->table.';';
        $res=BS::Database()->q($sql,array());
    }
}