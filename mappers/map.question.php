<?php
class map_question extends map_everything {
	var $table  = 'questions';
	var $entity = array
	(
		'id'=>'id',
                'guid'=>'guid',
                'text'=>'text',
                'a1'=>'a1',
                'a2'=>'a2',
                'a3'=>'a3',
                'a4'=>'a4',
                'price'=>'price',
                'played'=>'played',
                'correct'=>'correct',
                'category'=>'category',
	);	

	public function markquests($text,$cat)
	{
                if (mb_strlen($text,'UTF-8')>=4) {
                    $res = BS::Database()->q('update questions set category = \''.$cat.'\' where text like \'%'.$text.'%\' and category = \'none\';',array());
                }
                return true;
	}          

	public function markAnswer($id,$correct)
	{
                if ($correct) {
                    $res = BS::Database()->q('update questions set played = played+1, correct=correct+1 where id = ?',array($id));
                } else {
                    $res = BS::Database()->q('update questions set played = played+1 where id = ?',array($id));
                }
                return true;
	}          
                
        
	public function search($text)
	{
		$res = BS::Database()->q('select * from questions where text like \'%'.$text.'%\' and category = \'none\' ;',array());

		$result = array();
                if (!$res)return false;
                while (false !== $row = mysql_fetch_assoc($res))
                {
                    $result[]=$row;
                }
                return $result;
	}   
        
        public function getNotNone()
	{
		$res = BS::Database()->q('select * from questions where category != \'none\' ;',array());

		$result = array();
                if (!$res)return false;
                while (false !== $row = mysql_fetch_assoc($res))
                {
                    $result[]=$row;
                }
                return $result;
	}   
        
        public function getRandom()
        {
            $res = BS::Database()->q('select * from questions where category!=\'none\' order by rand() limit 1');
            return mysql_fetch_assoc($res);
        }
        
        public function getRandomCat($cat)
        {
            $res = BS::Database()->q('select * from questions where category=\''.$cat.'\' order by rand() limit 1');
            
            $question = mysql_fetch_assoc($res);
            
            
            return $question;
        }
        
	public function getStats()
	{
		$res = BS::Database()->q('select category, count(*) as cnt from questions group by category order by category asc',array());

		$result = array();
                if (!$res)return false;
                while (false !== $row = mysql_fetch_assoc($res))
                {
                    $result[]=$row;
                }
                
                
                $rp2 = array();
                foreach ($result as $k=>$v) {
                    $rp2[$v['category']]=$v['cnt'];
                }
                
                return $rp2;
		
	}
        
}