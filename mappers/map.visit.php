<?php
class map_visit extends map_everything {
	var $table  = 'visits';
	var $entity = array
	(
		'id'=>'id',
        'date_clean'=>'date_clean',
		'date_unix'=>'date_unix',
		'ip'=>'ip',
		'url'=>'url',
	);
	
	public function getVisitHistory()
	{
		if (file_exists('data/visits.history')) {
			return json_decode(file_get_contents('data/visits.history'),true);
		} else {
			
			$params = array();
			
			for ($x=7;$x>0;$x--) {
				$params[$x] = $this->getDateParams($x);
			}
			
			file_put_contents('data/visits.history', json_encode($params));
			
			return $params;
		}
	}
	
	public function getTodayVisits()
	{
		$res = BS::Database()->q('select *,count(*) as cnt, min(date_unix) as first_v, max(date_unix) as last_v from visits where date_unix >= '.  mktime(0,0,0).' group by ip order by first_v asc',array());

		$result = array();
        if (!$res)return false;
        while (false !== $row = mysql_fetch_assoc($res))
        {
            $result[]=$row;
        }
        return $result;
	}
	
	public function getTodayParams()
	{
		$res = BS::Database()->q('select count(*) as views, count(distinct ip) as visits from visits where date_unix >= '.  mktime(0,0,0),array());
		return mysql_fetch_assoc($res);
	}	
	
	public function getYesterdayParams()
	{
		$res = BS::Database()->q('select count(*) as views, count(distinct ip) as visits from visits where date_unix >= '.(mktime(0,0,0)-24*3600). ' AND date_unix < '.mktime(0,0,0),array());
		return mysql_fetch_assoc($res);
	}	
	
	public function getDateParams($daysAgo)
	{
		$res = BS::Database()->q('select count(*) as views, count(distinct ip) as visits from visits where date_unix >= '.(mktime(0,0,0)-($daysAgo-1)*24*3600). ' AND date_unix < '.(mktime(0,0,0)-($daysAgo-2)*24*3600),array());
		return mysql_fetch_assoc($res);
	}	
}