<?php
class map_user extends map_everything {
	var $table  = 'users';
	var $entity = array
	(
		'id'=>'id',
                'guid'=>'guid',
                'login'=>'login',
                'role'=>'role',
                'translation'=>'translation',
                'apikey'=>'apikey',
                'balance'=>'balance',
                'avatar'=>'avatar',
                'lat'=>'lat',
            'lon'=>'lon',
            'level'=>'level',
            'textlevel'=>'textlevel',
            'fakeid'=>'fakeid',
            'apnstoken'=>'apnstoken',
            'secret'=>'secret',
	);	
        
        public function getLonely()
	{
		$res = BS::Database()->q('SELECT lat,lon,count(*) as cnt FROM `users` WHERE 1 group by lat,lon having cnt < 200',array());

		$result = array();
                if (!$res)return false;
                while (false !== $row = mysql_fetch_assoc($res))
                {
                    $result[]=$row;
                }
                return $result;
	}   

        public function getChallengers($userid)
	{
		$res = BS::Database()->q('select id,login,avatar,guid,level from users where abs( lat - (select lat from users where id = '.$userid.'))<5 and abs( lon - (select lon from users where id = '.$userid.'))<5 order by rand() limit 10',array());

		$result = array();
                if (!$res)return false;
                while (false !== $row = mysql_fetch_assoc($res))
                {
                    $result[]=$row;
                }
                return $result;
	}
}

