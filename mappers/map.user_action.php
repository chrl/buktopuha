<?php
class map_user_action extends map_everything {
	var $table  = 'user_actions';
	var $entity = array
	(
		'id'=>'id',
                                    'user_id'=>'user_id',
		'date'=>'date',
		'action'=>'action',
                                    'subject_id'=>'subject_id',
                                    'params'=>'params',
	);
}