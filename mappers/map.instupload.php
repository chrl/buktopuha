<?php

class map_instupload extends map_everything {
    var $table  = 'instupload';
    var $entity = array
    (
            'id'=>'id',
            'status'=>'status',
            'url'=>'url',
            'imagefile'=>'imagefile',
            'date_changed'=>'date_changed',
            'date_upload'=>'date_upload',
            'title'=>'title',
    );
    
    public function getMaxUpdate()
    {
        $res = BS::Database()->q('SELECT max(date_upload) as mdu from instupload where status = 2 or status = 4', array());
        $res = mysql_fetch_assoc($res);
        if ($res['mdu']== NULL) {
            return 0;
        } else return $res['mdu'];
        
    }
    
    public function getUploadPic()
    {
        $res = BS::Database()->q('SELECT * FROM `instupload` where status = 2 and date_upload <= unix_timestamp() order by date_upload asc limit 1',array());
        return mysql_fetch_assoc($res);
    }
        
}