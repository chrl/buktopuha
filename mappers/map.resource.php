<?php
class map_resource extends map_everything {
	var $table  = 'resources';
	var $entity = array
	(
		'id'=>'id',
        'author'=>'author',
        'alias'=>'alias',
		'name'=>'name',   
		'desc'=>'desc',
		'active'=>'active',
		'date'=>'date',
	);	
        
        public function getLastVersion($alias) {
            $res = BS::Database()->q('select max(version) as m from `'.$this->table.'` where alias=?',array($alias));
            return mysql_result($res, 0);
        }
        
        public function getPreLastVersion($alias,$version) {
            $res = BS::Database()->q('select id from `'.$this->table.'` where alias=? and version<? order by version desc limit 1',array($alias,$version));
            if (!mysql_num_rows($res)) return false;
            return mysql_result($res, 0);
        }
        
        public function getLastVersions() {
            $res = BS::Database()->q('select * from documents where version = (select max(version) from documents docm where docm.alias = documents.alias)',array());

            $result = array();
            if (!$res)return false;
            while (false !== $row = mysql_fetch_assoc($res))
            {
                $result[]=$row;
            }
            return $result;

        }
        
    
}