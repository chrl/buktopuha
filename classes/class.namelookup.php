<?php
class Namelookup
{
	
	protected $operator = null;
	
	public static function stem($text)
	{
		   $handle = proc_open(dirname(__FILE__)."/Stemmer/mystem" . ' -nie utf-8', array(
			   0 => array("pipe", "r"), 1 => array("pipe", "w"), 2 => array("pipe", "w")
		   ), $pipes);

		   if (!is_resource($handle)) {
			   return false;
		   }

		   fwrite($pipes[0], $text);
		   fclose($pipes[0]);
		   $raw = array_filter(explode("\n", stream_get_contents($pipes[1])));
		   fclose($pipes[1]);
		   if ( !feof( $pipes[2] ) ) {
			   $logLine = fgets( $pipes[2] );
			   if (!empty($logLine)) {
				   throw new \Exception($logLine);
			   }
		   }
		   proc_close($handle);
		   return $raw;
	}

	public function __call($name, $arguments) {
		if (method_exists($this->operator,$name)) {
			return call_user_func_array(array($this->operator,$name), $arguments);
		}
		return false;
	}

	function __construct()
        {
                    require_once dirname(__FILE__).'/NameCaseLib/Library/NCL.NameCase.ru.php';
                    $this->operator = new NCLNameCaseRu();
        }

}
?>