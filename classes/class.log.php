<?php
/**
 * @package BullShark
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
*/


/**
 * Acl
 * Class checks access to page and redirects if no access granted
 * 
 * @package Apxa
 * @author Charlie <charlie@chrl.ru>
 * @version 2010
 * @access public
 */
class Log
{
    
    public function Log($action=false)
    {
        if(is_array($action)) {
            BS::DAO('user_action')->insertItem(array(
                'user_id'=>isset($_SESSION['user']['id'])?$_SESSION['user']['id']:0,
                'date'=>time(),
                'action'=>$action['action'],
                'subject_id'=>isset($action['subject'])?$action['subject']:0,
                'params'=>isset($action['params'])?json_encode($action['params']):'',
            ));
        }
        return $this;
    }

}