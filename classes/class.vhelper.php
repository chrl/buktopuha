<?php
/**
 * @package BullShark
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
*/



class vhelper
{
    public function iterate($headers,$values,$actions = array()){
        
        
        
        $result = '<section class="panel"><table class="table table-striped text-lg"><thead>
				   <tr class="info">';
                 
        $x=0;  
        foreach ($headers as $key=>$value) {
            $result.='<th>'.$value.'</th>'; 
        }   
        
        if (!empty($actions)) {
            $result.='<th>Действия</th>';
        }
                        
        $result.='</tr>';
        $result.='</thead><tbody>';
        
        $url = BS::getUrl();
        
        $row=0;
        foreach($values as $value) {
            
            $result.='<tr>';
            $x=0;
            foreach ($headers as $hId=>$hValue) {
                 $result.='<td>'.$value[$hId].'</td>';
            }
            
            if(!empty($actions))
            {
                    $acts = array();
                    foreach($actions as $id=>$action)
                    {
                        if ($id == 'edit') {
                            $acts[]='<a title="'.$action.'" href="/admin/'.$url[1].'/'.$value['id'].'/'.$id.'/" class="btn btn-info btn-xs"><i class="icon-edit"></i></a>';
                        } elseif($id == 'remove') {
                            $acts[]='<a title="'.$action.'" href="/admin/'.$url[1].'/'.$value['id'].'/'.$id.'/" class="btn btn-danger btn-xs"><i class="icon-remove"></i></a>';
                        } else {
                            $acts[]='<a href="/admin/'.$url[1].'/'.$value['id'].'/'.$id.'/" class="btn btn-primary btn-xs">'.$action.'</a>';
                        }
                        
                        
                        
                    }
                
                $result.='<td>'.implode('&nbsp;',$acts).'</td>';
            }
            
            $result.='</tr>';
        }
        
        if (count($values)==0) {
            $result.='<tr><td colspan="'.(count($headers)+1).'" align="center">Нет элементов для отображения</td></tr>';
        }
        
        
        
        $result.='</tbody></table></section>';
        
        
        return $result;
    }
 
    function __construct()
    {
        return $this;
    }

}
?>