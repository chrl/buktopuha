<?php
/**
 * @package BullShark
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
*/


/**
 * Acl
 * Class checks access to page and redirects if no access granted
 * 
 * @package Apxa
 * @author Charlie <charlie@chrl.ru>
 * @version 2010
 * @access public
 */
class Sms
{
    public $text = '';
    
    public function Sms($text = '')
    {
        $this->text = $text;
        
        return $this;
    }
    
    public function send($numbers)
    {
        if (!is_array($numbers)) $numbers = array($numbers);
        
        foreach ($numbers as $number) {
            $smsid = file_get_contents('http://bytehand.com:3800/send?id='.BS::getConfig('sms.id').'&key='.BS::getConfig('sms.key').'&to='.$number.'&from='.BS::getConfig('sms.sender').'&text='.  urlencode($this->text));
        }
        
        return $this;
    }
    

}