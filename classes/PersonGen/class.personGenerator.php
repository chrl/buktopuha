<?php

require_once 'class.gperson.php';
require_once 'class.deductorFactory.php';

class PersonGenerator {
    
    public $deductors = array('Fio');
    
    public function setDeductors(array $deductors)
    {
        $this->deductors = $deductors;
        return $this;
    }
    
    public function generatePerson($query) {
        $person = new gperson();
        $person->setQuery($query);
        $ded = new DeductorFactory();
        
        $ded->getFormat()->deduct($person);
        
        foreach ($this->deductors as $item) {
            $ded->getDeductor($item)->deduct($person);
        }
        
        return $person;
    }
    
}
