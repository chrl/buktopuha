<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractDeductor
 *
 * @author kh2
 */
abstract class AbstractDeductor {
	
	protected $item = '';
        
        public function __construct($item = '') {
            $this->item = $item;
        }

	public function setItem($item)
	{
		$this->item = $item;
		return $this;
	}
	
	public function predict()
	{
		return $this;
	}
	
	public function clear()
	{
		return $this;
	}
	
	public function format()
	{
		return $this;
	}
	
	public function translitIt($str)
	{
		$tr = array(
			"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
			"Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
			"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
			"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
			"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
			"Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
			"Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"yi","ь"=>"'","э"=>"e","ю"=>"yu","я"=>"ya"
		  );
		  return strtr($str,$tr);
	}
	
	public function deduct($item)
	{
                $this->item = $item;
		$this->predict()->clear()->format();
	
		return $this->item;
	}
	
}
