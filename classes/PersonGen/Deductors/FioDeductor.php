<?php

require_once('AbstractDeductor.php');

class FioDeductor extends AbstractDeductor {
	
	public function searchExact($item,$res) {
		
		foreach ($res as $i) {
			if ($i['translit'] == $item) return $i;
		}
		
		return false;
	}

	public function searchSoundex($item,$res) {
		
		$possibleWords = array();
		
		$divider = (mb_strlen(metaphone($item))<=2) ? 1:2;

		
		
		foreach ($res as $i) {
			if(levenshtein(metaphone($item), metaphone($i['translit'])) < mb_strlen(metaphone($item))/$divider)
			{
			  if(levenshtein($item, $i['translit']) < mb_strlen($item)/$divider)
			  {
				$possibleWords[] = $i;
			  }
			}			
		}
		
		// now parse possiblewords
		
		if (count($possibleWords)==1) {
			return $possibleWords[0];
		}
		
		$similarity = 0;
	    $meta_similarity = 0;
	    $min_levenshtein = 1000;
	    $meta_min_levenshtein = 1000;		
		
		foreach($possibleWords as $n)
		{
		  $min_levenshtein = min($min_levenshtein, levenshtein($n['translit'], $item));
		}
		
		foreach($possibleWords as $n)
		{
		  if(levenshtein($n['translit'], $item) == $min_levenshtein)
		  {
		   $similarity = max($similarity, similar_text($n['translit'], $item));
		  }
		}
		
		$result = array();
		
		foreach($possibleWords as $n=>$k)
		{
		  if(levenshtein($k['translit'], $item) <= $min_levenshtein)
		  {
				if(similar_text($k['translit'], $item) >= $similarity)
				{
				  $result[] = $k;
				}
		  }
		}		
		
		if (count($result)==1) {
			return $result[0];
		}		
		
		foreach($result as $n)
		{
		  $meta_min_levenshtein = min($meta_min_levenshtein, levenshtein(metaphone($n['translit']), metaphone($item)));
		}

		foreach($result as $n)
		{
		  if(levenshtein($n['translit'], $item) == $meta_min_levenshtein)
		  {
		   $meta_similarity = max($meta_similarity, similar_text(metaphone($n['translit']), metaphone($item)));
		  }
		}
		
		$meta_result = array();
		
		foreach($result as $n=>$k)
		{
		  if(levenshtein(metaphone($k['translit']), metaphone($item)) <= $meta_min_levenshtein)
		  {
		   if(similar_text(metaphone($k['translit']), metaphone($item)) >= $meta_similarity)
		   {
			 $meta_result[] = $k;
		   }
		  }
		}		
		
		if (count($meta_result)==1) {
			return $meta_result[0];
		}		

		return false;
	}	
	
	public function predict() {
		
		$result = array();
		
		$query = explode(' ',$this->item->query[0]);
		
		foreach ($query as $key=>$item) {
			// clean now
			$query[$key] = trim($query[$key]);
			$query[$key] = trim($query[$key],'"\'');

			$query[$key] = mb_strtolower($query[$key],'UTF-8');
			$query[$key] = $this->translitIt($query[$key]);
		}

		foreach ($query as $key=>$item) {
			
			$res= BS::DAO('dictname')->getLooklike($item);
			
			$r = $this->searchExact($item,$res);
			
			if ($r && is_array($r)) {
				
				if ($r['link']>0) {
					$r = BS::DAO('dictname')->getById($r['link']);
				}
				
				$result[$r['type']]=  mb_strtoupper(mb_substr($r['name'],0,1,'UTF-8'),'UTF-8')
						.mb_strtolower(mb_substr($r['name'],1,mb_strlen($r['name'],'UTF-8')-1,'UTF-8'),'UTF-8');
				unset($query[$key]);
				
			} else {
				
				$r = $this->searchSoundex($item,$res);
				
				
				if ($r && is_array($r)) {
					
					if ($r['link']>0) {
						$r = BS::DAO('dictname')->getById($r['link']);
					}
				
					$result[$r['type']]=  mb_strtoupper(mb_substr($r['name'],0,1,'UTF-8'),'UTF-8')
							.mb_strtolower(mb_substr($r['name'],1,mb_strlen($r['name'],'UTF-8')-1,'UTF-8'),'UTF-8');
					unset($query[$key]);
				}
			}
			
		}
		
		var_dump($result);
		
		return $this;
	}
}
