<?php

require_once('AbstractDeductor.php');

class FormatDeductor extends AbstractDeductor {
	
    protected $encoding = array();
    
    public function detect_encoding($string) {  
            static $list = array('utf-8', 'windows-1251');
            foreach ($list as $item) {
              $sample = @iconv($item, $item, $string);
              if (md5($sample) == md5($string))
                return $item;
            }
            return null;
    }
    
    public function predictDelimiter($query)
    {
        $delimiters = array('tab'       => "\t",
                    'comma'     => ",",
                    'semicolon' => ";"
        );
        
        $cnt = array();
        foreach ($delimiters as $k=>$v) {
            $cnt[$k] = count(explode($v,$query));
        }
        arsort($cnt);
        list($del) = array_keys(array_slice($cnt, 0,1,true));
        return $delimiters[$del];
    }
    
    public function findDelimiterForFile($handle) {
            $delimiters = array('tab'       => "\t",
                                'comma'     => ",",
                                'semicolon' => ";"
                            );
            $line = array();            # Stores the count of delimiters in each row
            $valid_delimiter = array(); # Stores Valid Delimiters

            for ( $i = 1; $i < (count($handle)>6?6:count($handle)); $i++ ){
                foreach ($delimiters as $key => $value ){
                    $line[$key][$i] = count( explode( $value, $handle[$i] ) ) - 1;
                }
            }

            foreach ( $line as $delimiter => $count ){
                if ( $count[1] > 0 and $count[2] > 0 ){
                    $match = true;
                    $prev_value = '';
                    foreach ( $count as $value ){
                        if ( $prev_value != '' )
                            $match = ( $prev_value == $value and $match == true ) ? true : false;
                        $prev_value = $value;
                    }
                } else { 
                    $match = false;
                }
                if ( $match == true )    $valid_delimiter[] = $delimiter;
            }//foreach
            $delimiter = ( $valid_delimiter[0] != '' ) ? $valid_delimiter[0] : "comma";

            return $delimiters[$delimiter];             
    }
    
    public function predict() {
        $query = $this->item->query;
        $enc = $this->detect_encoding($query);
        if($enc!='UTF-8') {
            $query = iconv($enc,'UTF-8',$query);
            $this->item->setQuery($query);
        }
        
        
        $query = explode($this->predictDelimiter($query),$query);
        foreach ($query as $k=>$v) {
            $query[$k] = trim($query[$k]);
            $query[$k] = trim($query[$k],'"');
        }
        
        $this->item->setQuery($query);

        return $this;
        
    }
    
    
}
