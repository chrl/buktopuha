<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class
 *
 * @author kh2
 */
class DeductorFactory {
    public function __call($name, $arguments) {
        if (substr($name,0,3)=='get') {
            $name = substr($name,3);
            
            if (file_exists(dirname(__FILE__).'/Deductors/'.$name.'Deductor.php')) {
                require_once ('Deductors/'.$name.'Deductor.php');
                $class = $name.'Deductor';
                $deductor = new $class();
                return $deductor;
            } else {
                throw new Exception('Deductor not found: '.$name.'Deductor');
            }
                
        }
    }
    
    public function getDeductor($name) {
        return $this->{'get'.$name}();
    }
}
