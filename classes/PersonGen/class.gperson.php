<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class
 *
 * @author charlie
 */
class gperson {
    
    public $query = '';
    
    public $fields = array();
    protected $translations = array(
        'name'=>'Имя',
        'lastname'=>'Фамилия',
        'patronim'=>'Отчество',
    );

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }
    
    public function getPrintableFields()
    {
        $a = array();
        foreach ($this->fields as $key=>$value)
        {
            $a[] = array(
                'name'=>isset($this->translations[$key])?$this->translations[$key]:$key,
                'key'=>$key,
                'value'=>$value,
            );
        }
        return $a;
    }
    
}
