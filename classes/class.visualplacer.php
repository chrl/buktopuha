<?php
/**
 * @package BullShark
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
*/

class visualplacer
{

    function __construct()
    {
        return $this;
    }
	
	public function postExecute()
	{
		$modPlaces = BS::DAO('module_place')->getByFields(array('active'=>1),null,false,array('priority','ASC'));
		foreach ($modPlaces as $place) {
			
			$module = BS::DAO('module')->getById($place['module']);
			
			if (file_exists(APPDIR . '/../modules/' . $module['alias'] . '/module.php'))
			{
				require_once (APPDIR . '/../modules/' . $module['alias'] . '/module.php');    
			} 
			else throw new Exception('Module '.$module['alias'].' not found!');	
			$mname = 'module_'.$module['alias'];
			$mname = new $mname();
			

			
			if (method_exists($mname, $place['method'])) {
				
				if ($place['place'] =='execute') {
					$mname->$place['method']();
				} else {
				
					$url = implode('/',BS::getUrl());
					
					
					
					if (preg_match('/'.$place['pages'].'/',$url)) {
					
						if (!(BS::Visual()->$place['place'])) {
							BS::Visual()->$place['place'] = '';
						}

						BS::Visual()->$place['place'].=$mname->$place['method']();
					}
				}
			}

		}
	}

}
?>