<?php

require_once 'PersonGen/class.personGenerator.php';

class PersGen
{
    public $factory = null;
    
    public function __construct()
    {
        $this->factory = new PersonGenerator();
        return $this->factory;
    }
    
    public function __call($name, $arguments) {
            if (method_exists($this->factory,$name)) {
                    return call_user_func_array(array($this->factory,$name), $arguments);
            }
            return false;
    }
}