<?php
/**
 * @package BullShark
 * @author Charlie <charlie@chrl.ru>
 * @since 0.1
*/


/**
 * Translate
 * Class translates languages via google translate engine
 * 
 * @package Apxa
 * @author Charlie <charlie@chrl.ru>
 * @version 2010
 * @access public
 */
class Ytranslate
{
    public function ytranslate($s_text=false, $s_lang = 'en', $d_lang = 'ru'){
        
        if (empty($s_text)) return $this;
        
        $url = "https://translate.yandex.net/api/v1.5/tr.json/translate?format=plain&key=trnsl.1.1.20131117T010859Z.8339578a93645e37.85709b575c9087dde7b5864915c43e27b3183e1e&lang=en&text=".urlencode($s_text);
        $b = file_get_contents($url);
        $json = json_decode($b, true);

        return $json['text'][0];
    }
 
}
?>